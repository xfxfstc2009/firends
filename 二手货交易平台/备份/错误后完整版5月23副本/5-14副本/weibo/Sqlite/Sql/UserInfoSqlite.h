// SQL执行语句
#import <Foundation/Foundation.h>
#import "UserModel.h"
@interface UserInfoSqlite : NSObject

// 新增用户
+(BOOL)addItem:(UserModel *)item;

// 查找用户
+(BOOL)selectItem:(UserModel *)item;

// 修改登录状态
+(BOOL)updateItemisLog0;

// 修改登录状态=1
+(BOOL)updateItemisLog1:(UserModel *)item;


// 查找当前登录的用户
+(BOOL)selectItemWithUserIsLogin:(UserModel *)item;

@end
