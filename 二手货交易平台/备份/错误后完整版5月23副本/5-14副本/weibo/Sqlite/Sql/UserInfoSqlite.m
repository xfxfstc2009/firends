// SQL执行语句
#import "UserInfoSqlite.h"
#import <sqlite3.h>
// 本地数据库的表名
#define TableName @"Userinfo"
@implementation UserInfoSqlite



// 新增
+(BOOL)addItem:(UserModel *)item
{
    if(item==nil)
        return false;

    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];

    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
          
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"insert into %@",TableName];
             [sql appendFormat:@"(djlsh,username,password,mobilephone,email,cityname,address,shop,shopno,createtime,islog) values ('%i','%@','%@','%@','%@','%@','%@','%i','%i','%@','%i')",item.DjLsh,item.userName,item.pwdWord,item.MobilePhone,item.Email,item.cityName,item.Address,item.Shop,item.ShopNo,item.CreateTime,item.isLog];
            
            
            

            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;

}

// 查找
+(BOOL)selectItem:(UserModel *)item
{
    if(item==nil)
        return false;
    
    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select * from %@ ",TableName];
            [sql appendFormat:@"where userName=%@",item.userName];
            
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;
    
}


// 修改登录状态=0
+(BOOL)updateItemisLog0
{
    
    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"update %@ set islog=0 where username is(select username from %@ where islog=1)",TableName,TableName];
            
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;
    
}

// 修改登录状态=1
+(BOOL)updateItemisLog1:(UserModel *)item
{

    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];

    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {

            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"update userinfo set islog=1,passWord='%@',cityname='%@',address='%@' where username = '%@'",item.pwdWord,item.cityName,item.Address,item.userName];

            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;

}


// 查找当前登录的用户
+(BOOL)selectItemWithUserIsLogin:(UserModel *)item
{
    if(item==nil)
        return false;
    
    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select userName from %@ ",TableName];
            [sql appendFormat:@"where isLog=1"];
            
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;
    
}



@end
