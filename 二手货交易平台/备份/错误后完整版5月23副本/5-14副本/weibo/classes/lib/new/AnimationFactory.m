#import "AnimationFactory.h"

@implementation AnimationFactory

// 点到点移动
+(CABasicAnimation *)moveTime:(float)time fromPoint:(CGPoint)point1 toPoint:(CGPoint)point2
{
    CABasicAnimation *animation  = [CABasicAnimation animationWithKeyPath:@"position"]; // 设置需要改变的属性
    animation.fromValue = [NSValue valueWithCGPoint:point1];
    animation.toValue = [NSValue valueWithCGPoint:point2];
    animation.duration = time;
    return animation;
}

// 点移动(bug:会损坏按钮无法响应事件)
+(CABasicAnimation *)moveTime:(float)time toPoint:(CGPoint)point
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"transform.translation"];
    animation.toValue = [NSValue valueWithCGPoint:point];
    animation.duration = time;
    animation.removedOnCompletion = NO; // 动画结束后是否移除效果
    animation.fillMode = kCAFillModeForwards;
    animation.repeatCount = 1;
    return animation;
}
// 横向移动(bug:会损坏按钮无法响应事件)
+(CABasicAnimation *)moveTime:(float)time toX:(float)x
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    animation.toValue = [NSNumber numberWithFloat:x];
    animation.duration = time;
    animation.removedOnCompletion = NO; // 动画结束后是否移除效果
    animation.fillMode = kCAFillModeForwards;
    animation.repeatCount = 1;
    return animation;
}
// 纵向移动(bug:会损坏按钮无法响应事件)
+(CABasicAnimation *)moveTime:(float)time toY:(float)y
{
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    animation.toValue = [NSNumber numberWithFloat:y];
    animation.duration = time;
    animation.removedOnCompletion = NO; // 动画结束后是否移除效果
    animation.fillMode = kCAFillModeForwards;
    animation.repeatCount = 1;
    return animation;
}

// 淡化动画
+(CATransition *)fadeWithTime:(float)time
{
    // 淡出效果
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = time;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = kCATransitionFade;
    animation.subtype = kCATransitionFromLeft; // 对于淡出无意义
    return animation;
}

// 水波纹动画
+(CATransition *)waterWithTime:(float)time
{
    // 淡出效果
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = time;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"rippleEffect"; // 波纹
    animation.subtype = kCATransitionFromLeft; // 对于波纹无意义
    return animation;
}

@end
