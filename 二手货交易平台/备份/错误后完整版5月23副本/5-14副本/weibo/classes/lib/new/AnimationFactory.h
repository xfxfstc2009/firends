#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

// 动画 梦工厂
@interface AnimationFactory : NSObject

// 点到点移动
+(CABasicAnimation *)moveTime:(float)time fromPoint:(CGPoint)point1 toPoint:(CGPoint)point2;

// 点移动(传递差值)(bug:会损坏按钮无法响应事件)
+(CABasicAnimation *)moveTime:(float)time toPoint:(CGPoint)point;
// 横向移动(传递差值)(bug:会损坏按钮无法响应事件)
+(CABasicAnimation *)moveTime:(float)time toX:(float)x;
// 纵向移动(传递差值)(bug:会损坏按钮无法响应事件)
+(CABasicAnimation *)moveTime:(float)time toY:(float)y;

// 淡出动画
+(CATransition *)fadeWithTime:(float)time;
// 水波纹动画
+(CATransition *)waterWithTime:(float)time;

@end
