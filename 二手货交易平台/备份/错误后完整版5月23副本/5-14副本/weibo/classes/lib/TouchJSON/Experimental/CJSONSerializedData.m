#import "CJSONSerializedData.h"

@interface CJSONSerializedData ()
@end

#pragma mark -

@implementation CJSONSerializedData

@synthesize data;

- (id)initWithData:(NSData *)inData
{
    if ((self = [super init]) != NULL)
    {
        data = inData;
    }
    return(self);
}



- (NSData *)serializedJSONData
{
    return(self.data);
}

@end
