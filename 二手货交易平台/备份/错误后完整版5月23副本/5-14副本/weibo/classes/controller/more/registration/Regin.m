
#import "Regin.h"
#import "CustomInfoModel.h"
#import "Md5Coder.h"

#import "UserInfoSqlite.h"

// 获取地址
#import "MMLocationManager.h"
@interface Regin ()

@end

@implementation Regin
// 获取到的城市名称
@synthesize cityname;
// 获取到的地址
@synthesize address;

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // 设置标题
    self.title=@"用户注册";
    
    // 设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];

    // 3.设置右边的注册item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"注册" size:CGSizeMake(50, 30) target:self action:@selector(regin)];

    // 4.设置获取地址方法
    [self getAddress];
    [self getCity];
    
    // 4.设置按钮
    [self addButtonofSet];
    
    // 6. 数据交互代理方法
    dal = [[CustomInfoDAL alloc] initWithDelegate:self];
}


#pragma mark -获取 地址
-(void)getAddress
{
    __block __weak Regin *wself = self;
    [[MMLocationManager shareLocation] getAddress:^(NSString *addressString) {
        [wself setLabelText:addressString];
    }];
}

#pragma mark 获取地址显示在上面
-(void)setLabelText:(NSString *)text
{
    address=text;
}
#pragma mark 获取城市
-(void)getCity
{
    __block __weak Regin *wself = self;
    [[MMLocationManager shareLocation] getCity:^(NSString *cityString) {
        [wself setLabelText1:cityString];
    }];
}

#pragma mark 获取地址显示在上面
-(void)setLabelText1:(NSString *)text
{
    cityname=text;
}

#pragma mark 左边的取消按钮的监听事件
-(void)cancel
{
    // 返回上层
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 右边的注册按钮的监听事件
-(void)regin
{
    /**********将数据存入服务器**********/
    NSString * uid = userField.text;
    NSString * pwd = pwdField.text;
    pwd = [[Md5Coder md5Encode:pwd] lowercaseString];
    CustomInfoModel *cus=[[CustomInfoModel alloc]init];
    cus.passWord=pwd;
    cus.userName=uid;
    cus.cityName=cityname;
    cus.address=address;
    
    if([dal addItem:cus]== false)
    {
        [self setAlert:@"请检查网络"];

    }
    else
    {
  
    }
}

#pragma mark 服务器回调方法
-(void)addCustomCallBack:(BOOL)item
{
    [self setAlert:@"服务器注册成功"];
}

#pragma mark 弹出Alert
-(void)setAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];

}

#pragma mark 添加基础控件
-(void)addButtonofSet
{
    /**********请输入账号***********/
    // 设置文字的位置
    userLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    // 设置文本内容
    userLabel.text=@"用户名";
    // 设置标题文字大小
    userLabel.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:userLabel];
    
    
    /**********请输入密码***********/
    // 设置文字的位置
    pwdLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 160, 100, 30)];
    // 设置文本内容
    pwdLabel.text=@"密  码";
    
    // 设置标题文字大小
    pwdLabel.font=[UIFont systemFontOfSize:18];
    
    [self.view addSubview:pwdLabel];
    
    
    
    
    /**********输入账号***********/
    // 初始化坐标位置
    userField=[[UITextField alloc] initWithFrame:CGRectMake(90, 100, 190, 30)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    userField.placeholder = @"请输入您的用户名";
    // 设置点一下清除内容
    userField.clearButtonMode=userField;
    // 默认就是左对齐，这个是UITextField扩展属性
    userField.textAlignment = UITextAlignmentLeft;
    // 设置textField的形状
    userField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置为YES当用点触文本字段时，字段内容会被清除,这个属性一般用于密码设置，当输入有误时情况textField中的内容
    userField.clearsOnBeginEditing = YES;
    // 设置键盘完成按钮
    userField.returnKeyType=UIReturnKeyDone;
    // 委托类需要遵守UITextFieldDelegate协议
    userField.delegate=self;
    //设置TextFiel输入框字体大小
    userField.font = [UIFont systemFontOfSize:18];
    
    
    
    
    
    /**********输入密码***********/
    
    //初始化坐标位置
    pwdField=[[UITextField alloc] initWithFrame:CGRectMake(90, 160, 190, 30)];
    // 设置文本文档的输入为密码
    pwdField.secureTextEntry=YES;
    // 设置点一下清除内容
    pwdField.clearButtonMode=pwdField;
    //为空白文本字段绘制一个灰色字符串作为占位符
    pwdField.placeholder = @"请输入您的密码";
    //设置textField的形状
    pwdField.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    pwdField.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    pwdField.delegate=self;
    //设置TextFiel输入框字体大小
    pwdField.font = [UIFont systemFontOfSize:18];
    
    
    
    //    把TextField添加到视图上
    [self.view addSubview:userField];
    [self.view addSubview:pwdField];



}


@end
