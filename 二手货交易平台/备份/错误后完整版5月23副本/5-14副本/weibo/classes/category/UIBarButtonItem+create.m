//调用这个方法，快速创建一个barbuttonitem出来

#import "UIBarButtonItem+create.h"

@implementation UIBarButtonItem (create)
+(UIBarButtonItem *)barButtonItemWithIcon:(NSString *)icon target:(id)target action:(SEL)action
{
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *btnNormal=[UIImage imageNamed:icon];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [btn setAllstateBg:icon];
    btn.bounds=(CGRect){CGPointZero,btnNormal.size};
    return [[UIBarButtonItem alloc] initWithCustomView:btn];

}


// 传入一个图片，传出按钮//写微博的按钮使用
+ (UIBarButtonItem *)barButtonItemWithBg:(NSString *)bg title:(NSString *)title size:(CGSize)size target:(id)target action:(SEL)action
{
    // 创建按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    // 按钮文字
    [btn setTitle:title forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    // 按钮背景
    [btn setAllstateBg:bg];
    // 按钮边框
    btn.bounds= (CGRect){CGPointZero, size};
    // 监听器
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}
@end
