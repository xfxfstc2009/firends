﻿#import "CustomInfoModel.h"
#import "JsonDataExchange.h"

@implementation CustomInfoModel

@synthesize djLsh;
@synthesize userName;
@synthesize passWord;
@synthesize mobilePhone;
@synthesize email;
@synthesize cityName;
@synthesize address;
@synthesize shop;
@synthesize shopNo;
@synthesize createTime;


+(CustomInfoModel *)itemWithDict:(NSDictionary *)dict
{
    CustomInfoModel *item = [[CustomInfoModel alloc] init];
    item.djLsh = [[dict valueForKey:@"DjLsh"] intValue];
    item.userName = [dict valueForKey:@"UserName"];
    item.passWord = [dict valueForKey:@"PassWord"];
    item.mobilePhone = [dict valueForKey:@"MobilePhone"];
    item.email = [dict valueForKey:@"Email"];
    item.cityName = [dict valueForKey:@"CityName"];
    item.address = [dict valueForKey:@"Address"];
    item.shop = [[dict valueForKey:@"Shop"] intValue];
    item.shopNo = [[dict valueForKey:@"ShopNo"] intValue];
    item.createTime = [JsonDataExchange JsonTimeToNSDate:[dict valueForKey:@"CreateTime"]];
    return item;
}

-(void)exchangeNil
{
    if(userName == nil) userName = @"";
    if(passWord == nil) passWord = @"";
    if(mobilePhone == nil) mobilePhone = @"";
    if(email == nil) email = @"";
    if(cityName == nil) cityName = @"";
    if(address == nil) address = @"";
    if(createTime == nil)
        createTime = [JsonDataExchange NSDateMin];
}
-(NSMutableString *)getJsonValue
{
    [self exchangeNil];

    NSMutableString * jsonItem = [NSMutableString string];
    [jsonItem appendFormat:@"{"];
    [jsonItem appendFormat:@"\"DjLsh\":%i,", djLsh];
    [jsonItem appendFormat:@"\"UserName\":\"%@\",", userName];
    [jsonItem appendFormat:@"\"PassWord\":\"%@\",", passWord];
    [jsonItem appendFormat:@"\"MobilePhone\":\"%@\",", mobilePhone];
    [jsonItem appendFormat:@"\"Email\":\"%@\",", email];
    [jsonItem appendFormat:@"\"CityName\":\"%@\",", cityName];
    [jsonItem appendFormat:@"\"Address\":\"%@\",", address];
    [jsonItem appendFormat:@"\"Shop\":%i,", shop];
    [jsonItem appendFormat:@"\"ShopNo\":%i,", shopNo];
    [jsonItem appendFormat:@"\"CreateTime\":\"\\/%@\\/\"", [JsonDataExchange JsonTimeFromNSDate:createTime]];
    [jsonItem appendFormat:@"}"];
    return jsonItem;
}
-(NSMutableString *)getXmlValue
{
    [self exchangeNil];

    NSMutableString *xmlItem = [NSMutableString string];
    [xmlItem appendFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"];
    [xmlItem appendFormat:@"<item>"];
    [xmlItem appendFormat:@"<DjLsh>%i</DjLsh>",djLsh];
    [xmlItem appendFormat:@"<UserName>%@</UserName>",userName];
    [xmlItem appendFormat:@"<PassWord>%@</PassWord>",passWord];
    [xmlItem appendFormat:@"<MobilePhone>%@</MobilePhone>",mobilePhone];
    [xmlItem appendFormat:@"<Email>%@</Email>",email];
    [xmlItem appendFormat:@"<CityName>%@</CityName>",cityName];
    [xmlItem appendFormat:@"<Address>%@</Address>",address];
    [xmlItem appendFormat:@"<Shop>%i</Shop>",shop];
    [xmlItem appendFormat:@"<ShopNo>%i</ShopNo>",shopNo];
    [xmlItem appendFormat:@"<CreateTime>%@</CreateTime>",[JsonDataExchange JsonTimeFromNSDate:createTime]];
    [xmlItem appendFormat:@"</item>"];
    return xmlItem;
}

#pragma mark - NSCoding
-(void) encodeWithCoder: (NSCoder *) encoder
{
    [encoder encodeInt:djLsh forKey: @"djLsh"];
    [encoder encodeObject:userName forKey: @"userName"];
    [encoder encodeObject:passWord forKey: @"passWord"];
    [encoder encodeObject:mobilePhone forKey: @"mobilePhone"];
    [encoder encodeObject:email forKey: @"email"];
    [encoder encodeObject:cityName forKey: @"cityName"];
    [encoder encodeObject:address forKey: @"address"];
    [encoder encodeInt:shop forKey: @"shop"];
    [encoder encodeInt:shopNo forKey: @"shopNo"];
    [encoder encodeObject:createTime forKey: @"createTime"];
}
-(id) initWithCoder: (NSCoder *) decoder
{
    djLsh = [decoder decodeIntForKey:@"djLsh"];
    userName = [decoder decodeObjectForKey:@"userName"] ;
    passWord = [decoder decodeObjectForKey:@"passWord"];
    mobilePhone = [decoder decodeObjectForKey:@"mobilePhone"] ;
    email = [decoder decodeObjectForKey:@"email"];
    cityName = [decoder decodeObjectForKey:@"cityName"];
    address = [decoder decodeObjectForKey:@"address"];
    shop = [decoder decodeIntForKey:@"shop"];
    shopNo = [decoder decodeIntForKey:@"shopNo"];
    createTime = [decoder decodeObjectForKey:@"createTime"];

    return self;
}

#pragma mark - NSCopying
// 复制
-(id)copyWithZone:(NSZone *)zone
{
    CustomInfoModel *newItem = [[CustomInfoModel allocWithZone: zone] init];

    newItem.djLsh = self.djLsh;
    newItem.userName = self.userName;
    newItem.passWord = self.passWord;
    newItem.mobilePhone = self.mobilePhone;
    newItem.email = self.email;
    newItem.cityName = self.cityName;
    newItem.address = self.address;
    newItem.shop = self.shop;
    newItem.shopNo = self.shopNo;
    newItem.createTime = self.createTime;

	return newItem;
}

@end

