#import "ImgUploader.h"

@implementation ImgUploader

@synthesize delegate = _delegate;

// 构造函数
-(id)init
{
    self = [super init];
    if(self)
    {
    }
    return self;
}
-(id)initWithDelegate:(id<ImgUploaderDelegate>)delegate
{
    self = [super init];
    if(self)
    {
        _delegate = delegate;
    }
    return self;
}

// 取消数据交互
-(void)cancel
{
    [theConnection cancel];

}

/*** 发送的消息格式 ***
 
 Content-type: multipart/form-data, boundary=AaB03x
 
 --AaB03x
 content-disposition: form-data; name="field1"
 
 Hello Boris!
 
 --AaB03x
 content-disposition: form-data; name="pic"; filename="boris.png"
 Content-Type: image/png
 
 ... contents of boris.png ...
 
 --AaB03x--
 
 ***/

// 发送PNG图片(Key=pic)到page页面
-(BOOL)sendImgWithPage:(NSString *)page
                params:(NSDictionary *)params
{
    // 用户设置
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *wcfUrlStr = [defaults objectForKey:@"wcfurl"];
    if(wcfUrlStr == nil || wcfUrlStr.length == 0)
    {
        wcfUrlStr = WcfUrl;
        [defaults setObject:WcfUrl forKey:@"wcfurl"];
    }
    
    // 请求的地址
    NSString *urlPath = [[NSString alloc] initWithFormat:@"%@/Wcf/%@", wcfUrlStr, page];
    
    // 分界线的标识符
    NSString *TWITTERFON_FORM_BOUNDARY = @"AaB03x";
    // 根据url初始化request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlPath]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:10];
    // 分界线 --AaB03x（不是起始线）
    NSString *MPboundary = [[NSString alloc] initWithFormat:@"--%@",TWITTERFON_FORM_BOUNDARY];
    // 结束符 AaB03x--（只有1个）
    NSString *endMPboundary = [[NSString alloc] initWithFormat:@"%@--",MPboundary];
    // 要上传的图片
    UIImage *image = [params objectForKey:@"img"];
    // 得到图片的data
    NSData *data = UIImagePNGRepresentation(image);
    // http body的字符串
    NSMutableString *body = [[NSMutableString alloc] init];
    // 参数的集合的所有key的集合
    NSArray *keys = [params allKeys];
    
    // 遍历keys
    for(int i=0; i<[keys count]; i++)
    {
        // 得到当前key
        NSString *key = [keys objectAtIndex:i];
        // 如果key不是img，说明value是字符类型，比如name:Boris
        if(![key isEqualToString:@"img"])
        {
            // 添加分界线，换行
            [body appendFormat:@"%@\r\n",MPboundary];
            // 添加字段名称，换2行
            [body appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
            // 添加字段的值
            [body appendFormat:@"%@\r\n",[params objectForKey:key]];
        }
    }
    
    // 添加分界线，换行
    [body appendFormat:@"%@\r\n",MPboundary];
    // 声明img字段，文件名为boris.png
    [body appendFormat:@"Content-Disposition: form-data; name=\"img\"; filename=\"boris.png\"\r\n"];
    // 声明上传文件的格式
    [body appendFormat:@"Content-Type: image/png\r\n\r\n"];
    
    // 声明结束符：--AaB03x--
    NSString *end = [[NSString alloc] initWithFormat:@"\r\n%@", endMPboundary];
    // 声明myRequestData，用来放入http body
    NSMutableData *myRequestData=[NSMutableData data];
    // 将body字符串转化为UTF8格式的二进制
    [myRequestData appendData:[body dataUsingEncoding:NSUTF8StringEncoding]];
    // 将image的data加入
    [myRequestData appendData:data];
    // 加入结束符--AaB03x--
    [myRequestData appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    
    // 设置HTTPHeader中Content-Type的值
    NSString *content=[[NSString alloc] initWithFormat:@"multipart/form-data; boundary=%@",TWITTERFON_FORM_BOUNDARY];
    // 设置HTTPHeader
    [request setValue:content forHTTPHeaderField:@"Content-Type"];
    // 设置Content-Length
    [request setValue:[NSString stringWithFormat:@"%i", (int)myRequestData.length] forHTTPHeaderField:@"Content-Length"];
    // 设置http body
    [request setHTTPBody:myRequestData];
    // http method
    [request setHTTPMethod:@"POST"];

    // 创建网络链接

    theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if(theConnection)
    {

        webData = [[NSMutableData alloc] init]; // 重新创建对象
        
        return TRUE;
    }
    else
    {
        NSLog(@"theConnection is NULL");
        return FALSE;
    }
}

// 根据参数，创建一个 URLRequest（还没有执行该请求）
//-(BOOL)sendImgWithPage:(NSString *)page
//                 image:(UIImage *)image
//             extension:(NSString *)extension
//                  fold:(NSString *)fold
//{
//    // 用户设置
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *wcfUrlStr = [defaults objectForKey:@"wcfurl"];
//    if(wcfUrlStr == nil || wcfUrlStr.length == 0)
//    {
//        wcfUrlStr = WcfUrl;
//        [defaults setObject:WcfUrl forKey:@"wcfurl"];
//    }
//    
//    // 请求的地址
//    NSString *urlPath = [[[NSString alloc] initWithFormat:@"%@/Wcf/%@?Fold=%@",wcfUrlStr,page,fold] autorelease];
//    NSURL *url = [NSURL URLWithString:urlPath];
//    
//    if(IfDebugDAL)
//        NSLog(@"WcfRequest urlPath = %@", urlPath);
//    
//    // 处理后缀名 ==>
//    NSString *contentType = nil;
//    extension = [extension lowercaseString];
//    if([extension isEqualToString:@"png"] || [extension isEqualToString:@".png"])
//    {
//        contentType = @"image/png";
//    }
//    else if([extension isEqualToString:@"jpg"] || [extension isEqualToString:@".jpg"]
//            || [extension isEqualToString:@"jpeg"] || [extension isEqualToString:@".jpeg"])
//    {
//        contentType = @"image/jpeg";
//    }
//    else if([extension isEqualToString:@"gif"] || [extension isEqualToString:@".gif"])
//    {
//        contentType = @"image/gif";
//    }
//    else if([extension isEqualToString:@"bmp"] || [extension isEqualToString:@".bmp"])
//    {
//        contentType = @"image/bmp";
//    }
//    else
//    {
//        return false;
//    }
//    
//    // UIImage ==> NSData
//    NSData *imageData;
//    if (UIImagePNGRepresentation(image) == nil)
//        imageData = UIImageJPEGRepresentation(image, 1); // 1是压缩参数
//    else
//        imageData = UIImagePNGRepresentation(image); // 无损压缩
//    
//    // 请求
//    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
//    [theRequest setHTTPMethod:@"POST"];
//    [theRequest setValue:[NSString stringWithFormat:@"%i",imageData.length] forHTTPHeaderField:@"Content-Length"];
//    [theRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];
//    [theRequest setHTTPBody:imageData];
//    
//    [theRequest setHTTPBody:UIImagePNGRepresentation(image)];
//    
//    // 创建网络链接
//    [theConnection release];
//    theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
//    if(theConnection)
//    {
//        [webData release],webData = nil;        // 清空数据
//        webData = [[NSMutableData alloc] init]; // 重新创建对象
//        
//        return TRUE;
//    }
//    else
//    {
//        NSLog(@"theConnection is NULL");
//        return FALSE;
//    }
//}

#pragma mark - NSURLConnectionDelegate
// 接收到回应，准备接收数据
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if(webData == nil)
        webData = [[NSMutableData alloc] init];
    [webData setLength:0]; // 清空容器
}
// 开始传递(续传)数据
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data]; // 分段接收数据
}
// 发生错误
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Connection failed: %@",[error description]);
    [_delegate uploadErrorCallBack]; // 回调错误信息
}
// 连接交互结束
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [_delegate uploadCallBack:webData];// 回调

}

@end
