#import <Foundation/Foundation.h>

@protocol ImgUploaderDelegate

@optional
// 成功返回
-(void)uploadCallBack:(NSMutableData *)webData;
// 失败返回
-(void)uploadErrorCallBack;

@end
