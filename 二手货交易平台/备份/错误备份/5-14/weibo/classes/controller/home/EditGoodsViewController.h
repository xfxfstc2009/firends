// 修改商品信息
#import <UIKit/UIKit.h>
#import "GoodsInfoDAL.h"
@interface EditGoodsViewController : UIViewController<UITextFieldDelegate,GoodsInfoDelegate>
{
    GoodsInfoDAL *dal; // 修改商品的代理方法
    
    
    /**************必填*******************/
    UILabel *typeIDLabel;       // 类型编号
    UILabel *customIDLabel;     // 客户编号
    UILabel *goodsNameLabel;    // 商品名称
    UILabel *buyPriceLabel;     // 采购价格
    UILabel *salePriceLabel;    // 销售报价
    UILabel *saleSignLabel;     // 销售标记
    UILabel *createTimeLabel;   // 创建时间
    
    
    UITextField *typeIDField;       // 类型编号
    UITextField *customIDField;     // 客户编号
    UITextField *goodsNameField;    // 商品名称
    UITextField *buyPriceField;     // 采购价格
    UITextField *salePriceField;    // 销售报价
    UITextField *saleSignField;     // 销售标记
    UITextField *createTimeField;   // 创建时间
    
    
    /**************选填*******************/
    UILabel *goodsSizeLabel;        // 商品规格
    UILabel *imgPathLabel;          // imgpath
    UILabel *imgNameLabel;          // imgname
    UILabel *goodsDescLabel;        // goodsdesc
    
    
    UITextField *goodsSizeField;    // 商品规格
    UITextField *imgPathField;      // imgpath
    UITextField *imgNameField;      // imgname
    UITextField *goodsDescField;    // goodsdesc
}
// 类型编号
@property(nonatomic,strong)NSString *typeid1;
// 商品名称
@property(nonatomic,strong)NSString *goodsname;
// 采购价格
@property(nonatomic,strong)NSString *buyprice;
// 出售价格
@property(nonatomic,strong)NSString *salePrice;
// 出售状态
@property(nonatomic,strong)NSString *salesign;



@end
