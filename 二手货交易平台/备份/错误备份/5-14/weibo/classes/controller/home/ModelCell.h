
#import <UIKit/UIKit.h>
@class GoodsInfoModel;

@interface ModelCell : UITableViewCell

@property (strong, nonatomic) GoodsInfoModel *goodsModel;
// 图片
@property (strong, nonatomic) IBOutlet UIImageView *goodsimage1;
// 物品名字
@property (strong, nonatomic) IBOutlet UILabel *goodsName1;
// 物品价格
@property (strong, nonatomic) IBOutlet UILabel *goodsPrice1;
// 物品简介
@property (strong, nonatomic) IBOutlet UILabel *goodsdesc1;
// 发布时间
@property (strong, nonatomic) IBOutlet UILabel *createTime;

-(void)setValues:(GoodsInfoModel *)item;

@end
