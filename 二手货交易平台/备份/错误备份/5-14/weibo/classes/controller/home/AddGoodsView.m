// 添加商品页面
#import "AddGoodsView.h"
#import "GoodsInfoModel.h"
#import <sqlite3.h>
#import "GoodsInfoSqlite.h"
#import "UserModel.h"
@implementation AddGoodsView

// 查本地数据库，然后得到用户名
@synthesize userDjlsh;
- (void)viewDidLoad
{
    // 1.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];

    // 4. 数据交互代理方法
    dal = [[GoodsInfoDAL alloc] initWithDelegate:self];
    // 5.设置按钮
    [self addButtonSet];

    // 读取当前登陆的islog=1然后去设置发布的内容
    [self SearchFromDataBase];
    // 接上面的已知登录账户密码得到单据流水号
//    [self searchFromseverWithDjLsh];
    
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    // 3.设置右边的添加item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"添加" size:CGSizeMake(50, 30) target:self action:@selector(sendadd)];
}

#pragma mark 左边的取消按钮的监听事件
-(void)cancel
{
    // 返回上层
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 右边的传输内容事件
-(void)sendadd
{
    // 创建存放到服务器数据库的对象
    GoodsInfoModel *addgoods=[[GoodsInfoModel alloc]init];
    addgoods.customId=[userDjlsh intValue];
    addgoods.goodsName=GoodsNameField.text;
    addgoods.oldlevel=OldlevelField.text;
    addgoods.buyPrice=[BuyPriceField.text floatValue];
    addgoods.salePrice=[SalePriceField.text floatValue];
    addgoods.goodsDesc=GoodsDescField.text;
    addgoods.saleSign=0;
    addgoods.createTime=[NSDate date];
    // 进行存储
    if([dal addItem:addgoods]==false)
    {
        [self setAlert:@"添加失败"];
    }
    else
    {
        
    }
}

-(void)addGoodsInfoCallBack:(BOOL)result
{
    if(result==true)
    {
        [self setAlert:@"添加成功"];
    }
    else
    {
        [self setAlert:@"添加失败"];
    }
}


#pragma mark -得到当前用户
#pragma mark 读取本地数据库获得当前登陆帐号
-(void)SearchFromDataBase
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select Djlsh from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    userDjlsh = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                }
                
                
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
}


#pragma mark 设置Alert弹出消息
-(void)setAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];
    
}

#pragma mark 添加按钮等控件
-(void)addButtonSet
{
    
    /**********物品名字***********/
    GoodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    GoodsNameLabel.text=@"商品名称";
    GoodsNameLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:GoodsNameLabel];
    
    /**********新旧程度***********/
    OldlevelLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 140, 100, 30)];
    OldlevelLabel.text=@"新旧程度";
    OldlevelLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:OldlevelLabel];
    
    /**********购买时的价格***********/
    BuyPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 180, 100, 30)];
    BuyPriceLabel.text=@"购买时价格";
    BuyPriceLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:BuyPriceLabel];
    
    /**********出售的价格***********/
    SalePriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 220, 100, 30)];
    SalePriceLabel.text=@"商品出售价格";
    SalePriceLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:SalePriceLabel];
    
    /**********商品简介***********/
    GoodsDescLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 260, 100, 30)];
    GoodsDescLabel.text=@"商品简介";
    GoodsDescLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:GoodsDescLabel];
    

    
    /**********商品名称***********/
    GoodsNameField=[[UITextField alloc] initWithFrame:CGRectMake(110, 100, 150, 30)];
    GoodsNameField.placeholder = @"请输入商品名称";
    GoodsNameField.borderStyle=UITextBorderStyleRoundedRect;
    GoodsNameField.clearsOnBeginEditing = YES;
    GoodsNameField.returnKeyType=UIReturnKeyDone;
    GoodsNameField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:GoodsNameField];
    
    
    /**********采购价格***********/
    OldlevelField=[[UITextField alloc] initWithFrame:CGRectMake(110, 140, 150, 30)];
    OldlevelField.placeholder = @"请输入新旧程度";
    OldlevelField.borderStyle=UITextBorderStyleRoundedRect;
    OldlevelField.clearsOnBeginEditing = YES;
    OldlevelField.returnKeyType=UIReturnKeyDone;
    OldlevelField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:OldlevelField];
    
    /**********销售报价***********/
    BuyPriceField=[[UITextField alloc] initWithFrame:CGRectMake(110, 180, 150, 30)];
    BuyPriceField.placeholder = @"请输入您的出售价格";
    BuyPriceField.borderStyle=UITextBorderStyleRoundedRect;
    BuyPriceField.clearsOnBeginEditing = YES;
    BuyPriceField.returnKeyType=UIReturnKeyDone;
    BuyPriceField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:BuyPriceField];
    
    /**********销售报价***********/
    SalePriceField=[[UITextField alloc] initWithFrame:CGRectMake(110, 220, 150, 30)];
    SalePriceField.placeholder = @"请输入您的出售价格";
    SalePriceField.borderStyle=UITextBorderStyleRoundedRect;
    SalePriceField.clearsOnBeginEditing = YES;
    SalePriceField.returnKeyType=UIReturnKeyDone;
    SalePriceField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:SalePriceField];
    
    /**********销售报价***********/
    GoodsDescField=[[UITextField alloc] initWithFrame:CGRectMake(110, 260, 150, 30)];
    GoodsDescField.placeholder = @"请输入您的物品简介";
    GoodsDescField.borderStyle=UITextBorderStyleRoundedRect;
    GoodsDescField.clearsOnBeginEditing = YES;
    GoodsDescField.returnKeyType=UIReturnKeyDone;
    GoodsDescField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:GoodsDescField];
    
    
    
}

@end
