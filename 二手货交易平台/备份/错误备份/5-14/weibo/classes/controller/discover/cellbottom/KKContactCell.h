//
//  KKContactViewController.h
//  outing
//
//  Created by tripbe on 13-1-11.
//  Copyright (c) 2013年 seavision. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShopInfoModel.h"
@interface KKContactCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property(nonatomic,strong) ShopInfoModel *shopModel;


-(void)setValues;
@end
