// 我的收藏
#import "MyCollection.h"
#import "CollectionInfoSqlite.h"
#import <sqlite3.h>
#import "CollectionIntoViewController.h"

#define CurrTableName @"Collection"
@interface MyCollection ()

@end

@implementation MyCollection
// 获得当前登录的customid
@synthesize customid;
// 获得的列表
@synthesize collectionList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"我的收藏";
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    }
    return self;
}
 // 返回方法

-(void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewDidLoad
{
    //1.根据customid取列表
    [self getCollectionList];
}









#pragma mark - UITableView 数据源方法

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return collectionList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *Identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:Identifier];
    }
    CollectionModel *c1 = [collectionList objectAtIndex:indexPath.row];
    cell.textLabel.text = c1.goodsName;
    return cell;
}

#pragma mark tableview delegate 选中某一行 进行跳转
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.collectionIntoViewController == nil) {
        self.collectionIntoViewController = [[CollectionIntoViewController alloc] init];
    }
    [self.collectionIntoViewController setValue: self.collectionList[indexPath.row]];
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.collectionIntoViewController];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    self.collectionIntoViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:nav animated:YES completion:^{
        
        [self.collectionIntoViewController setValue:self.collectionList[indexPath.row]];
    }];
    
}





#pragma mark - 本地数据库查询方法

// 根据当前的customid取收藏列表
-(void)getCollectionList
{
    // 得到当前登录的customid
    [self SearchFromDataBaseGetCustomID];
    collectionList=[[NSMutableArray alloc]init];
    collectionList = [self getListDesc:customid];
    NSLog(@"%@",collectionList);
}

//获取当前登录的CustomID
-(void)SearchFromDataBaseGetCustomID
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    customid = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]integerValue];
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}

// 根据我读取我的收藏列表
-(NSMutableArray *)getListDesc:(int)lastID
{
    // 返回值
    NSMutableArray *list = [NSMutableArray array];
    
    // 1.获取iPhone上sqlite3的数据库文件的地址(应用程序沙盒里面的Documents文件夹)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:SqliteName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL find = [fileManager fileExistsAtPath:path];
    if (find)
    {
        // 2.打开iPhone上的sqlite3的数据库文件
        sqlite3 *database;
        // 判断数据库是否打开
        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK)
        {
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败");
        }
        else
        {
            // 3.准备sql语句
            sqlite3_stmt *statement;
            
            // 用“limit 0,5”代替“top 5”
            NSMutableString *query = [NSMutableString stringWithString:@"select "];
            [query appendString:@"DjLsh,CustomID,GoodsName,Oldlevel,BuyPrice,SalePrice,SaleSign,ImageTitle,ImgPath,ImgName,GoodsDesc,CreateTime,MobilePhone"];
            [query appendFormat:@" from %@ where customdjlsh = %i",CurrTableName,lastID];

           
            
            // NSLog(@"query = %@",query);
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    // 返回值
                    CollectionModel *item = [[CollectionModel alloc] init];
                    
                    item.djLsh = (float)sqlite3_column_int(statement, 0);
                    
                    item.customID = (float)sqlite3_column_int(statement, 1);
                    
                    char *goodsName = (char *) sqlite3_column_text(statement,  2);
                    
                    
                    if(goodsName != NULL)
                        item.goodsName = [NSString stringWithUTF8String:goodsName];
                    else
                        item.goodsName = @"";
                    
                    
                    char *newLevel = (char *) sqlite3_column_text(statement,  3);
                    if(newLevel != NULL)
                        item.oldLevell = [NSString stringWithUTF8String:newLevel];
                    else
                        item.oldLevell = @"";
                    
                    
                    
                    item.buyPrice = (float)sqlite3_column_int(statement, 4);
                    
                    
                    item.salePrice = (float)sqlite3_column_int(statement, 5);
                    
                    item.saleSign = (float)sqlite3_column_int(statement, 6);
                    
                    char *imageTitle = (char *) sqlite3_column_text(statement,  7);
                    if(imageTitle != NULL)
                        item.imgTitle = [NSString stringWithUTF8String:imageTitle];
                    else
                        item.imgTitle = @"";
                    
                    char *imgPath = (char *) sqlite3_column_text(statement,  8);
                    if(imgPath != NULL)
                        item.imgPath = [NSString stringWithUTF8String:imgPath];
                    else
                        item.imgPath = @"";
                    
                    char *imgName = (char *) sqlite3_column_text(statement,  9);
                    if(imgName != NULL)
                        item.imgName = [NSString stringWithUTF8String:imgName];
                    else
                        item.imgName = @"";
                    
                    char *goodsDesc = (char *) sqlite3_column_text(statement,  10);
                    if(goodsDesc != NULL)
                        item.goodsDesc = [NSString stringWithUTF8String:goodsDesc];
                    else
                        item.goodsDesc = @"";
                    
                    
                    char *createTime = (char *) sqlite3_column_text(statement,  11);
                    if(createTime != NULL)
                        item.createTime = [NSString stringWithUTF8String:createTime];
                    else
                        item.createTime = @"";
                    
                    char *mobilePhone = (char *) sqlite3_column_text(statement,  12);
                    if(mobilePhone != NULL)
                        item.mobilephone = [NSString stringWithUTF8String:mobilePhone];
                    else
                        item.createTime = @"";
                    
                    
                    // 时间没处理
                    
                    [list addObject:item];
                }
            }
            
            // 7.释放sql文资源
            sqlite3_finalize(statement);
            
            // 8.关闭iPhone上的sqlite3的数据库
            sqlite3_close(database);
        }
    }
    return list;
}

@end
