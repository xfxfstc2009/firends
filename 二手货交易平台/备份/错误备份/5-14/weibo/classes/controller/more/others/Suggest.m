// 意见反馈
#import "Suggest.h"

@interface Suggest ()

@end

@implementation Suggest

@synthesize sText;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // 设置标题
        self.title = @"意见反馈";
        // 设置左侧的返回按钮
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
        // 设置右侧的发送按钮
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleBordered target:self action:@selector(send)];
        // 添加意见反馈的输入框
        UITextView *input = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 320, 500)];
        [input setText:@""];
        input.font = [UIFont systemFontOfSize:17];
        [self.view addSubview:input];
    }
    return self;
}






#pragma mark 左侧返回按钮点击事件
-(void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark 右侧的发送按钮点击事件
-(void)send
{
    NSLog(@"发送信息");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    // 1.1.清除标签的背景
    cell.textLabel.backgroundColor = [UIColor clearColor];
    // 1.2.设置标签高亮时的文字颜色为默认的文字颜色
    cell.textLabel.highlightedTextColor = cell.textLabel.textColor;
    
    // 1.3.设置cell的背景view
    UIImageView *bg = [[UIImageView alloc] init];
    cell.backgroundView = bg;
    
    UIImageView *selectedBg = [[UIImageView alloc] init];
    cell.selectedBackgroundView = selectedBg;
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_icon_arrow.png"]];
    return  cell;
}

@end
