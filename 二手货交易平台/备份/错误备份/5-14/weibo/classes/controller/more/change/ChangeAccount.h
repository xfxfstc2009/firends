// 修改账户
#import <UIKit/UIKit.h>
#import "CustomInfoDAL.h"
@interface ChangeAccount : UIViewController<UITextFieldDelegate,CustomInfoDelegate>
{
    CustomInfoDAL *dal; // 用户登录的代理方法
    
    UILabel *UserField; // 用户名
    UILabel *PwdField;  // 密码
    UILabel *PwdFieldAgan; // 再次输入密码
    
    UITextField *UserTextField;//用户名输入框
    UITextField *PwdTextFieldold;//密码输入框
    UITextField *PwdTextFieldnew;//密码输入框
    
    UIButton *changeButton; // 修改按钮
    
    
}

@property(nonatomic,assign)int customDjLsh;
@end
