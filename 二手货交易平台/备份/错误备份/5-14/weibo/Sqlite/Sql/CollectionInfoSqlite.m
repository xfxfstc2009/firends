// 添加到收藏
#import "CollectionInfoSqlite.h"
#import "CollectionModel.h"
#import <sqlite3.h>
#define CurrTableName @"Collection"
@implementation CollectionInfoSqlite


// 读取列表
-(NSMutableArray *)getListDesc:(int)lastID
{
    // 返回值
    NSMutableArray *list = [NSMutableArray array];
    
    // 1.获取iPhone上sqlite3的数据库文件的地址(应用程序沙盒里面的Documents文件夹)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:SqliteName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL find = [fileManager fileExistsAtPath:path];
    if (find)
    {
        // 2.打开iPhone上的sqlite3的数据库文件
        sqlite3 *database;
        // 判断数据库是否打开
        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK)
        {
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败");
        }
        else
        {
            // 3.准备sql语句
            sqlite3_stmt *statement;
            
            // 用“limit 0,5”代替“top 5”
            NSMutableString *query = [NSMutableString stringWithString:@"select "];
            [query appendString:@"DjLsh,CustomID,GoodsName,Oldlevel,BuyPrice,SalePrice,SaleSign,ImageTitle,ImgPath,ImgName,GoodsDesc,CreateTime,MobilePhone"];
            [query appendFormat:@" from %@ where djLsh = %i",CurrTableName,lastID];
            
            
            
            // NSLog(@"query = %@",query);
            if(sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
            {
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    // 返回值
                    CollectionModel *item = [[CollectionModel alloc] init];
                    
                    item.djLsh = (float)sqlite3_column_int(statement, 0);
                    
                    item.customID = (float)sqlite3_column_int(statement, 1);
                    
                    char *goodsName = (char *) sqlite3_column_text(statement,  2);
                    
                    
                    if(goodsName != NULL)
                        item.goodsName = [NSString stringWithUTF8String:goodsName];
                    else
                        item.goodsName = @"";
                    
                    
                    char *newLevel = (char *) sqlite3_column_text(statement,  3);
                    if(newLevel != NULL)
                        item.oldLevell = [NSString stringWithUTF8String:newLevel];
                    else
                        item.oldLevell = @"";
                    
                    
                    
                    item.buyPrice = (float)sqlite3_column_int(statement, 4);
                    
                    
                    item.salePrice = (float)sqlite3_column_int(statement, 5);
                    
                    item.saleSign = (float)sqlite3_column_int(statement, 6);
                    
                    char *imageTitle = (char *) sqlite3_column_text(statement,  7);
                    if(imageTitle != NULL)
                        item.imgTitle = [NSString stringWithUTF8String:imageTitle];
                    else
                        item.imgTitle = @"";
                    
                    char *imgPath = (char *) sqlite3_column_text(statement,  8);
                    if(imgPath != NULL)
                        item.imgPath = [NSString stringWithUTF8String:imgPath];
                    else
                        item.imgPath = @"";
                    
                    char *imgName = (char *) sqlite3_column_text(statement,  9);
                    if(imgName != NULL)
                        item.imgName = [NSString stringWithUTF8String:imgName];
                    else
                        item.imgName = @"";
                    
                    char *goodsDesc = (char *) sqlite3_column_text(statement,  10);
                    if(goodsDesc != NULL)
                        item.goodsDesc = [NSString stringWithUTF8String:goodsDesc];
                    else
                        item.goodsDesc = @"";
                    
                    
                    char *createTime = (char *) sqlite3_column_text(statement,  11);
                    if(createTime != NULL)
                        item.createTime = [NSString stringWithUTF8String:createTime];
                    else
                        item.createTime = @"";
                    
                    char *mobilePhone = (char *) sqlite3_column_text(statement,  12);
                    if(mobilePhone != NULL)
                        item.mobilephone = [NSString stringWithUTF8String:mobilePhone];
                    else
                        item.createTime = @"";
                    
                    
                    // 时间没处理
                    
                    [list addObject:item];
                }
            }
            
            // 7.释放sql文资源
            sqlite3_finalize(statement);
            
            // 8.关闭iPhone上的sqlite3的数据库
            sqlite3_close(database);
        }
    }
    return list;
}



// 新增到本地数据库
+(BOOL)addInToCollection:(CollectionModel *)item
{
    if(item==nil)
        return false;
    BOOL result=false;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"insert into %@",CurrTableName];
            
            [sql appendFormat:@"(djlsh,customid,goodsname,oldlevel,buyprice,saleprice,salesign,imagetitle,imgpath,imgname,goodsdesc,createtime,mobilephone,customDjlsh)values('%i','%i','%@','%@','%.2f','%.2f','%i','%@','%@','%@','%@','%@','%@','%i')",item.djLsh,item.customID,item.goodsName,item.oldLevell,item.buyPrice,item.salePrice,item.saleSign,item.imgTitle,item.imgPath,item.imgName,item.goodsDesc,item.createTime,item.mobilephone,item.customDjlsh];
            
            
            
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;
    
}




@end
