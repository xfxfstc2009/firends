﻿#import <Foundation/Foundation.h>
#import "CustomInfoModel.h"

// 客户信息 委托协议
@protocol CustomInfoDelegate<NSObject>

@optional

-(void)loginCallBack:(CustomInfoModel *)item;
-(void)addCustomCallBack:(BOOL)result;
-(void)updateCustomCallBack:(BOOL)result;
-(void)deleteCustomCallBack:(BOOL)result;
-(void)getCustomFullItemCallBack:(CustomInfoModel *)item;

@end

