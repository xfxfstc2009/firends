﻿#import "CustomInfoDAL.h"
#import "CustomInfoModel.h"

#import "CJSONDeserializer.h"
#import "CJSONSerializer.h"

@implementation CustomInfoDAL

@synthesize delegate;

#pragma mark - lifeCycle
-(id)init
{
    self = [super init];
    if(self)
    {
        request = [[WcfRequest alloc] initWithUrlPara:@"CustomService.svc"
                                              wcfName:@"ICustomService"];
        request.delegate = self;
    }
    return self;
}

-(CustomInfoDAL *)initWithDelegate:(id)de
{
    self = [super init];
    if(self)
    {
        request = [[WcfRequest alloc] initWithUrlPara:@"CustomService.svc"
                                              wcfName:@"ICustomService"];
        request.delegate = self;
        delegate = de;
    }
    return self;
}


// 根据djlsh取单条
-(BOOL)login:(NSString *)userName and:(NSString *)passWord
{
    // 方法名称
    NSString *funcName = @"Login";
    // 准备参数
    NSMutableString *funcPara = [NSMutableString stringWithString:@""];
    [funcPara appendString:[NSString stringWithFormat:@"<%@>%@</%@>",@"userName",userName,@"userName"]];
    [funcPara appendString:[NSString stringWithFormat:@"<%@>%@</%@>",@"passWord",passWord,@"passWord"]];
    // 远程交互
    [request cancel];
    return [request requestWithPara:funcName :funcPara];
}

// 新增
-(BOOL)addItem:(CustomInfoModel *)item
{
    // 方法名称
    NSString *funcName = @"AddItem";
    // 准备参数
    NSMutableString *funcPara = [NSMutableString stringWithString:@""];
    [funcPara appendString:[NSString stringWithFormat:@"<%@>",@"jsonItem"]];
    [funcPara appendString:[item getJsonValue]];
    [funcPara appendString:[NSString stringWithFormat:@"</%@>",@"jsonItem"]];
    // 远程交互
    [request cancel];
    return [request requestWithPara:funcName :funcPara];
}

// 更新
-(BOOL)updateItem:(CustomInfoModel *)item
{
    // 方法名称
    NSString *funcName = @"UpdateItem";
    // 准备参数
    NSMutableString *funcPara = [NSMutableString stringWithString:@""];
    [funcPara appendString:[NSString stringWithFormat:@"<%@>",@"jsonItem"]];
    [funcPara appendString:[item getJsonValue]];
    [funcPara appendString:[NSString stringWithFormat:@"</%@>",@"jsonItem"]];
    // 远程交互
    [request cancel];
    NSLog(@"%@",funcPara);
    return [request requestWithPara:funcName :funcPara];
}

// 删除
-(BOOL)deleteItem:(int)djLsh
{
    // 方法名称
    NSString *funcName = @"DeleteItem";
    // 准备参数
    NSMutableString *funcPara = [NSMutableString stringWithString:@""];
    [funcPara appendString:[NSString stringWithFormat:@"<%@>%i</%@>",@"djLsh",djLsh,@"djLsh"]];
    // 远程交互
    [request cancel];
    return [request requestWithPara:funcName :funcPara];
}

// 根据djlsh取单条
-(BOOL)getFullItem:(int)djLsh
{
    // 方法名称
    NSString *funcName = @"GetFullItem";
    // 准备参数
    NSMutableString *funcPara = [NSMutableString stringWithString:@""];
    [funcPara appendString:[NSString stringWithFormat:@"<%@>%i</%@>",@"djLsh",djLsh,@"djLsh"]];
    // 远程交互
    [request cancel];
    return [request requestWithPara:funcName :funcPara];
}


#pragma mark - WcfRequestDelegate
// 成功返回
-(void)requestCallBack:(NSMutableData *)webData
{
    if(IfDebugDAL)
        NSLog(@"%@",[[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding] );

   
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:webData];
    [parser setDelegate:self];
    [parser parse];

}
// 失败返回
-(void)requestErrorCallBack:(NSString *)funcName
{
    if([funcName isEqualToString:@"Login"])
    {
        [delegate loginCallBack:false];
    }
    else if([funcName isEqualToString:@"AddItem"])
    {
        [delegate addCustomCallBack:false];
    }
    else if([funcName isEqualToString:@"UpdateItem"])
    {
        [delegate updateCustomCallBack:false];
    }
    else if([funcName isEqualToString:@"DeleteItem"])
    {
        [delegate deleteCustomCallBack:false];
    }
    else if([funcName isEqualToString:@"GetFullItem"])
    {
        [delegate getCustomFullItemCallBack:nil];
    }
}

#pragma mark - NSXMLParserDelegate
// 开始遍例xml的节点
- (void)parserDidStartDocument:(NSXMLParser *)parser
{

    xmlData = [[NSMutableDictionary alloc] init];
}
// 纪录元素开始标签，并出始化临时数组，
// 遍例xml的节点,发现元素开始符的处理函数（即报告元素的开始以及元素的属性）
// 获得节点头的值
-(void)parser:(NSXMLParser *)parser
didStartElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qualifiedName
   attributes:(NSDictionary *)attributeDict
{
    currentElement = elementName;
   
    currentData = [[NSMutableString alloc] init];
}
// 提取XML标签中的元素，反复取多次，
// 当xml节点有值时,则进入此句,处理标签包含内容字符 （报告元素的所有或部分内容）
// 节点之间的内容
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [currentData appendString:string]; // 续读数据
}
// 纪录元素结尾标签，并把临时数组中的元素存到字符串数组中，
// 当遇到结束标记时，进入此句, 发现元素结束符的处理函数，保存元素各项目数据（即报告元素的结束标记）
-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    // 对象添加到字典里，它的引用次数并没有发生变化
    [xmlData setObject:currentData forKey:currentElement];
    //[currentElement release];
    //[currentData release];
}

// 结束分析，调用显示函数,报告解析的结束
-(void)parserDidEndDocument:(NSXMLParser *)parser
{
    // 登陆
    NSString *loginResult = [xmlData objectForKey:@"LoginResult"];
    if (loginResult == nil)
        loginResult = [xmlData objectForKey:@"LoginBack"];
    if (loginResult != nil)
    {
        if(loginResult.length > 0)
        {
            CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
            NSError *error = nil;
            NSData* data = [loginResult dataUsingEncoding: NSUTF8StringEncoding];
            NSDictionary *jsonDict = [jsonDeserializer deserializeAsDictionary:data error:&error];
            if (error)
            {
                NSLog(@"%@",error);
            
            }
            // 得到数据
            CustomInfoModel *item = [CustomInfoModel itemWithDict:jsonDict]; // 正常返回
            [delegate loginCallBack:item]; // 正常返回
        }
        else
        {
            [delegate loginCallBack:nil]; // 失败回调
        }
        return;
    }

    
    // 添加数据
    NSString *addResult = [xmlData objectForKey:@"AddItemResult"];
    if (addResult == nil)
        addResult = [xmlData objectForKey:@"AddItemBack"];
    if (addResult != nil)
    {
        if(addResult.length > 0)
        {
            [delegate addCustomCallBack:[[addResult lowercaseString] boolValue]]; // 正常返回
        }
        else
        {
            [delegate addCustomCallBack:FALSE]; // 失败回调
        }
        return;
    }

    // 更新数据
    NSString *updateResult = [xmlData objectForKey:@"UpdateItemResult"];
    if (updateResult == nil)
        updateResult = [xmlData objectForKey:@"UpdateItemBack"];
    if (updateResult != nil)
    {
        if(updateResult.length > 0)
        {
            [delegate updateCustomCallBack:[[updateResult lowercaseString] boolValue]]; // 正常返回
        }
        else
        {
            [delegate updateCustomCallBack:FALSE]; // 失败回调
        }
        return;
    }

    // 删除数据
    NSString *deleteResult = [xmlData objectForKey:@"DeleteResult"];
    if (deleteResult == nil)
        deleteResult = [xmlData objectForKey:@"DeleteBack"];
    if (deleteResult != nil)
    {
        if(deleteResult.length > 0)
        {
            [delegate deleteCustomCallBack:[[deleteResult lowercaseString] boolValue]]; // 正常返回
        }
        else
        {
            [delegate deleteCustomCallBack:FALSE]; // 失败回调
        }
        return;
    }

    // 取单条
    NSString *itemResult = [xmlData objectForKey:@"GetFullItemResult"];
    if (itemResult == nil)
        itemResult = [xmlData objectForKey:@"GetFullItemBack"];
    if (itemResult!= nil)
    {
        if(itemResult.length > 0)
        {
            //  json反序列化
            CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
            NSError *error = nil;
            NSData* data = [itemResult dataUsingEncoding: NSUTF8StringEncoding];
            NSDictionary *jsonDict = [jsonDeserializer deserializeAsDictionary:data error:&error];
            if (error)
            {
                NSLog(@"%@",error);
             
            }
            // 得到数据
            CustomInfoModel *item = [CustomInfoModel itemWithDict:jsonDict]; // 正常返回
            [delegate getCustomFullItemCallBack:item];
        }
        else
        {
            [delegate getCustomFullItemCallBack:nil]; // 失败回调
        }
        return;
    }
}
// 报告不可恢复的解析错误
-(void)paser:parserErrorOccured{}

@end

