
#import <Foundation/Foundation.h>

@interface ShopModel : NSObject
@property (nonatomic,assign) int djLsh; // 单据流水号
@property (nonatomic,copy) NSString *shopName; // shopname
@property (nonatomic,copy) NSString *shopDesc; // shopdesc
@property (nonatomic,assign) int customID; // 客户编号
@end
