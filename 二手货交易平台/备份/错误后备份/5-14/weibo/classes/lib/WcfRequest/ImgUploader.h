#import <Foundation/Foundation.h>
#import "ImgUploaderDelegate.h"


@interface ImgUploader : NSObject <NSURLConnectionDelegate>
{
    NSURLConnection *theConnection; // 当前网络链接
    NSMutableData *webData;         // 接收到的数据
    
    // 最后调用的方法(用户错误处理)
    NSString *lastFuncName;
}

@property (nonatomic,assign) id<ImgUploaderDelegate> delegate;

// 构造函数
-(id)initWithDelegate:(id<ImgUploaderDelegate>)delegate;

// 发送PNG图片(Key=pic)到page页面
-(BOOL)sendImgWithPage:(NSString *)page
                params:(NSDictionary *)params;

//// 根据参数，创建一个 URLRequest（还没有执行该请求）
//-(BOOL)sendImgWithPage:(NSString *)page
//                 image:(UIImage *)image
//             extension:(NSString *)extension
//                  fold:(NSString *)fold;

// 取消数据交互
-(void)cancel;

@end
