// 设置首次加载
#import "AppDelegate.h"

#import "MainViewController.h"
#import "NewFeatureViewController.h"
#import "MenuViewController.h"
#import "REFrostedViewController.h"
#import "HomeViewController.h"
#import "MenuViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // 添加沙盒
  [self addEditableDatabeIfNeed];
    
    
    // 1.创建窗口
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    /*
     2.判断是否第一次使用这个版本
     */
    NSString *key = (NSString *)kCFBundleVersionKey;
    
    // 2.1.先去沙盒中取出上次使用的版本号
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    
    // 2.2.加载程序中info.plist文件(获得当前软件的版本号)
    NSString *currentVersionCode = [NSBundle mainBundle].infoDictionary[key];
    
    if ([lastVersionCode isEqualToString:currentVersionCode]) {
        // 非第一次使用软件
        
        // 显示状态栏
        [UIApplication sharedApplication].statusBarHidden=NO;

        
        // 2.3.开始微博(主界面)
        [self startWeibo:NO];
    } else {
        // 第一次使用软件
        // 2.3.保存当前软件版本号
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        // 2.4.新特性控制器
        NewFeatureViewController *new = [[NewFeatureViewController alloc] init];
        new.startBlock = ^(BOOL shared){
            [self startWeibo:shared];
        };
        self.window.rootViewController = new;
    }
    
    // 3.显示窗口
 
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)startWeibo:(BOOL)shared
{
    // 1.显示状态栏
    [UIApplication sharedApplication].statusBarHidden = YES;


    // 2.主界面
   
    
    
    
    MainViewController *navigationController = [[MainViewController alloc] initWithRootViewController:[[HomeViewController alloc] init]];
    MenuViewController *menuController = [[MenuViewController alloc] initWithStyle:UITableViewStylePlain];
    
    // Create frosted view controller
    //
    REFrostedViewController *frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:navigationController menuViewController:menuController];
    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    
    // Make it a root controller
    //
    self.window.rootViewController = frostedViewController;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];


    
    
    
    
    
    
    
}



-(void)addEditableDatabeIfNeed
{
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(!ifFind)
    {
        NSLog(@"沙盒数据库文件不存在，需要复制!");
        // 把包里面的数据库文件 复制到沙盒的Documents 文件夹
        //
        NSString * srcPath= [[NSBundle mainBundle]pathForResource:@"mydatabase.sqlite" ofType:nil];
        
        // 复制操作
        [fm copyItemAtPath:srcPath toPath:path error:nil];
    }
}



@end
