//
//  MoreViewController.h
//  weibo
//
//  Created by 裴烨烽 on 14-4-29.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  Account;
@class Suggest;
@class AboutUs;

@interface MoreViewController : UITableViewController<UIAlertViewDelegate,UIActionSheetDelegate>

@property (nonatomic,retain) NSArray *data;
@property (nonatomic,retain) UIImage *bgImage;
@property (nonatomic,retain) UIButton *btnB;
@property (nonatomic,retain) AboutUs *aboutUs;
@property (nonatomic,retain) UIViewController *selectedController;
@property (nonatomic,retain) Suggest *suggest;


@end