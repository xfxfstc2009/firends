//
//  CollectionIntoViewController.m
//  weibo
//
//  Created by 裴烨烽 on 14-5-21.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import "CollectionIntoViewController.h"

@interface CollectionIntoViewController ()

@end

@implementation CollectionIntoViewController
@synthesize tablename;
@synthesize collModel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // 0.设置标题
    self.title=@"登录";
    
    // 1.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    // 3.设置右边的取消item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"登录" size:CGSizeMake(50, 30) target:self action:@selector(send)];

}

-(void)setValue:(CollectionModel *)item
{
   tablename = item.goodsName;
}


-(void)send
{
    NSLog(@"%@",tablename);
}
-(void)cancel
{
 [self dismissViewControllerAnimated:YES completion:nil];
}
@end
