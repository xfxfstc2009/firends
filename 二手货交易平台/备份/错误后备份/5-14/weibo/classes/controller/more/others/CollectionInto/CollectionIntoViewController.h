// 收藏内部
#import <UIKit/UIKit.h>
#import "CollectionModel.h"

@interface CollectionIntoViewController : UIViewController
@property (nonatomic,strong)NSString * tablename;
@property (nonatomic,strong)CollectionModel *collModel;
-(void)setValue:(CollectionModel *)item;
@end
