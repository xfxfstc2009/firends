//
//  NewFeatureViewController.m
//  weibo
//
//  Created by apple on 13-8-28.
//  Copyright (c) 2013年 itcast. All rights reserved.
//

#import "NewFeatureViewController.h"
#import "MainViewController.h"

// 设置图片数量（首次加载然后添加的图片）
#define kCount 4


@interface NewFeatureViewController ()

{
    UIPageControl *_pageControl;
     UIButton *_share;
}
@end
@implementation NewFeatureViewController

#pragma mark 自定义控制器的view
-(void)loadView
{
    UIImageView *imageView =[[UIImageView alloc] init];
    imageView.frame=[UIScreen mainScreen].bounds;
    imageView.image=[UIImage fullscreenImageWithName:@"new_feature_background.png"];
    imageView.userInteractionEnabled=YES; // 检测自己能不能用交互
    self.view=imageView;
}

#pragma mark view加载完毕
-(void)viewDidLoad
{
    [super viewDidLoad];
 
    CGSize viewSize=self.view.bounds.size; // 设置屏幕的size
    // 加载UIScrollView
   UIScrollView *scrollView=[[UIScrollView alloc] init];
    scrollView.frame=self.view.bounds;
    scrollView.contentSize=CGSizeMake(kCount *viewSize.width, 0);
    scrollView.showsHorizontalScrollIndicator=NO; // 设置是不是要下面的条
    scrollView.pagingEnabled=YES; // 是否分页
    scrollView.delegate=self;
    
    [self.view addSubview:scrollView];
    // 添加UIImageView
    for(int i=0;i<kCount;i++)
    {
       [self addImageViewAtIndex:i inView:scrollView];
    }
    
    
    // 加载UIPageControl
    UIPageControl *pageControl=[[UIPageControl alloc] init]; // 创建一个UIPageControl
    pageControl.center=CGPointMake(viewSize.width*0.5, viewSize.height*0.9); // 设置pagecontrol的位置
    
    pageControl.bounds=CGRectMake(0, 0, 100, 0);
    pageControl.numberOfPages=kCount; // 设置多少页
    pageControl.pageIndicatorTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"new_feature_pagecontrol_point.png"]]; // 设置点的颜色
    pageControl.currentPageIndicatorTintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"new_feature_pagecontrol_checked_point.png"]]; // 设置当前选中的点的颜色
    
    pageControl.userInteractionEnabled=NO; // 不能被点击切换
    [self.view addSubview:pageControl]; // 加载进入view
    _pageControl=pageControl;
}

-(void)addImageViewAtIndex:(int)index inView:(UIView *)superView
{
    CGSize viewSize=self.view.frame.size;
    
    // 1.创建imageview
    UIImageView *imageView=[[UIImageView alloc] init];
    imageView.frame=CGRectMake(index*viewSize.width, 0, viewSize.width, viewSize.height);
    // 2.设置图片
    NSString *name=[NSString stringWithFormat:@"new_feature_%d.png",index+1];
    imageView.image = [UIImage fullscreenImageWithName:name];
    // 3.添加
    [superView addSubview:imageView];
    
    // 如果是最后一张图片，添加2个按钮（分享按钮）
    if(index==kCount-1)
    {
        [self addBtnInView:imageView];
    }

}
#pragma mark 添加最后一张图片的按钮 (分享 开始)
-(void)addBtnInView:(UIView *)view
{
    CGSize viewSize=self.view.bounds.size; // 设置屏幕的size
    
    view.userInteractionEnabled=YES; // 用户可以进行点击
    // 开始按钮
    // 1.1 创建
    UIButton *start = [UIButton buttonWithType:UIButtonTypeCustom];
    [view addSubview:start];
    // 1.2.设置背景图片
    UIImage *startNormal = [UIImage imageNamed:@"new_feature_finish_button.png"];
    UIImage *startHighlighted = [UIImage imageNamed:@"new_feature_finish_button_highlighted.png"];
    [start setBackgroundImage:startNormal forState:UIControlStateNormal];
    [start setBackgroundImage:startHighlighted forState:UIControlStateHighlighted];
    // 1.3.边框
    start.center = CGPointMake(viewSize.width * 0.5,viewSize.height * 0.85);
    start.bounds = (CGRect){CGPointZero, startNormal.size};
    // start.bounds = CGRectMake(0, 0, startNormal.size.width, startNormal.size.height);
    // 1.4.监听
    [start addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
    
   
    
    /* 分享按钮 */
    // 2.1.创建
    UIButton *share = [UIButton buttonWithType:UIButtonTypeCustom];
    [view addSubview:share];
    // 2.2.设置背景图片
    UIImage *shareNormal = [UIImage imageNamed:@"new_feature_share_false.png"];
    [share setBackgroundImage:shareNormal forState:UIControlStateNormal];
    [share setBackgroundImage:[UIImage imageNamed:@"new_feature_share_true.png"] forState:UIControlStateSelected];
    // 2.3.边框
    share.center = CGPointMake(viewSize.width * 0.5, viewSize.height * 0.75);
    share.bounds = (CGRect){CGPointZero, shareNormal.size};
    // 2.4.监听
    [share addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    // 2.5.高亮状态下不要改变图片颜色
    share.adjustsImageWhenHighlighted = NO;
    // 2.6.默认选中
    share.selected = YES;
    _share = share;
}


#pragma mark 开始按钮的事件

-(void)start
{
    if (_startBlock) {
        _startBlock(_share.isSelected);
    }
    
    
    
    
 
//    // 销毁新特性控制器，然后转向新的界面
//    // 跳到主界面-MainViewControl
//    MainViewController *main=[[MainViewController alloc] init];
//    
//    // 跳转,切换窗口的根控制器（自动销毁新特性控制器）
//    self.view.window.rootViewController=main;
//   // [self presentViewController:main animated:YES completion:nil];
}

#pragma mark 分享按钮
-(void)share:(UIButton *)btn
{
    // 让按钮的选中状态取反
    btn.selected = ! btn.isSelected;
}


#pragma mark 滚动代理
#pragma mark 减速完毕就会调用（）
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControl.currentPage=scrollView.contentOffset.x/scrollView.frame.size.width;
}

-(void)dealloc
{
    NSLog(@"销毁");
}

@end