// 修改对应的商品信息
#import "ShopDetailed.h"
#import "GoodsInfoSqlite.h"
#import <sqlite3.h>
#import "CollectionModel.h"
#import "CollectionInfoSqlite.h"
#import "ColorButton.h"
@implementation ShopDetailed


@synthesize goodsDjLsh; // 商品的单据流水号
@synthesize titlename;  // 商品名称
@synthesize customID;   // 客户编号
@synthesize goodsName;  // 商品名称
@synthesize oldLevel;  // newlevel
@synthesize buyPrice;   // 采购价格
@synthesize salePrice;  // 销售报价
@synthesize goodsSign;  // 商品规格
@synthesize ImageTitle; // 图片的名字
@synthesize imgPath;    // imgpath
@synthesize imgName;    // imgname
@synthesize goodsDesc;  // goodsdesc
@synthesize createTime; // 创建时间
@synthesize mobilephone; // 电话

@synthesize addToCollectiom;
@synthesize callPhone;

@synthesize customDjlsh;// 当前登录账户的djlsh

-(void)setValues:(GoodsInfoModel*)supply
{
    
    goodsDjLsh=supply.djLsh; // 单据流水号
    customID=supply.customId;
    customIDLabel.text=[NSString stringWithFormat:@"客户编号为:%i",supply.customId];// 客户编号
    goodsNameLabel.text=[NSString stringWithFormat:@"商品名字为:%@",supply.goodsName]; // 商品名字
    oldLevellLabel.text=[NSString stringWithFormat:@"新旧程度为:%@",supply.oldlevel]; // 新旧程度
    buyLabel.text=[NSString stringWithFormat:@"购买价格为%.2f",supply.buyPrice];// 购买的价格
    saleLabel.text=[NSString stringWithFormat:@"出售价格为%.2f",supply.salePrice];// 销售价格
    goodsSignLabel.text=[NSString stringWithFormat:@"当前的销售状态%i",supply.saleSign];// 是否出售
    ImageTitle=supply.imageTitle;
    imgName=supply.imgName;
    imgPath=supply.imgPath;
    goodsDescLabel.text=[NSString stringWithFormat:@"销售简介%@",supply.goodsDesc];// 商品简介
    createTime=supply.createTime;
}

- (void)stringBasedFolding {
    
}



// 通过Customid得到用户的电话号码



- (void)viewDidLoad {
	[super viewDidLoad];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    
    // 4. 添加按钮
    [self addButtonSet];
    
    // 5.根据单据流水号去查询对应的账户的信息;
    // .文件代理方法
    cusDal = [[CustomInfoDAL alloc] initWithDelegate:self];
    [self searchCustomItem];
    [self SearchSqlGetCustomDjlsh];
}





#pragma mark 返回上层
-(void)cancel
{
    // 返回上层
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark 发送




#pragma mark 添加到数据库
-(void)addToCollection
{
    CollectionModel *collModel=[[CollectionModel alloc]init];
    collModel.customDjlsh=customDjlsh;
    collModel.djLsh=goodsDjLsh;
    collModel.goodsName=goodsNameLabel.text;     // 物品名字
    collModel.customID=[customIDLabel.text intValue];      // 持有者id
    collModel.oldLevell=oldLevellLabel.text;     // 新旧程度
    collModel.buyPrice=[buyLabel.text floatValue];       // 买来价格
    collModel.salePrice=[saleLabel.text floatValue];     // 出售价格
    collModel.goodsDesc=goodsDescLabel.text;     // 物品简介
    collModel.createTime=[NSString stringWithFormat:@"%@",createTime];   // 创建时间
    collModel.mobilephone=mobilephone; // 电话号码
    [CollectionInfoSqlite addInToCollection :collModel];
}

-(void)call
{
    NSMutableString *phoneNumber=[NSMutableString string];
    [phoneNumber appendFormat:@"tel://"];
    [phoneNumber appendFormat:@"%@",mobilephone];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    
}





-(void)SearchSqlGetCustomDjlsh
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    customDjlsh = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];
                    
                    
                }
                
                
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
}








#pragma mark 根据单据流水号，去取单条
-(void)searchCustomItem
{
    [cusDal getFullItem:customID];
}
-(void)getCustomFullItemCallBack:(CustomInfoModel *)item
{
    // 获得取得单条的信息(电话)
    mobilephone = item.mobilePhone;
}

#pragma mark 添加按钮
-(void)addButtonSet
{
    /**********商品名称***********/
    customIDLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    customIDLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:customIDLabel];
    
    /**********采购价格***********/
    goodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 140, 100, 30)];
    goodsNameLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsNameLabel];
    
    /**********销售报价***********/
    oldLevellLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 180, 100, 30)];
    oldLevellLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:oldLevellLabel];
    
    /**********销售报价***********/
    buyLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 220, 100, 30)];
    buyLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:buyLabel];
    
    /**********销售报价***********/
    saleLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 260, 100, 30)];
    saleLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:saleLabel];
    
    /**********销售报价***********/
    goodsSignLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 300, 100, 30)];
    goodsSignLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsSignLabel];
    
    /**********销售报价***********/
    goodsDescLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 340, 180, 30)];
    goodsDescLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsDescLabel];
    
    addToCollectiom=[[UIButton alloc]initWithFrame:CGRectMake(90, 370, 100, 30)];
    [addToCollectiom setTitle:@"添加到收藏" forState:UIControlStateNormal];
    [addToCollectiom setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addToCollectiom addTarget:self action:@selector(addToCollection) forControlEvents:UIControlEventTouchUpInside];
    [addToCollectiom setBackgroundImage:[UIImage imageNamed:@"chatfrom_bg_normal.png"] forState:UIControlStateNormal];
    [self.view addSubview:addToCollectiom];
    
    
    
    //加入电话//
    NSMutableArray *colorArray = [@[[UIColor colorWithRed:0.3 green:0.278 blue:0.957 alpha:1],[UIColor colorWithRed:0.133 green:0.90 blue:0.843 alpha:1]] mutableCopy];
	callPhone = [[ColorButton alloc]initWithFrame:CGRectMake(60, 420, 200, 40) FromColorArray:colorArray ByGradientType:topToBottom];
    [callPhone setTitle:@"拨打电话" forState:UIControlStateNormal];
    [callPhone addTarget:self action:@selector(call) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:callPhone];
}
@end
