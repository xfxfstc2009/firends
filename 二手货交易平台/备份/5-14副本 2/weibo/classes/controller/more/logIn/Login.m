#import "Login.h"
#import "Md5Coder.h"
#import "UserInfoSqlite.h"
@interface Login ()

@end

@implementation Login



- (void)viewDidLoad
{
    [super viewDidLoad];
    // 0.设置标题
    self.title=@"登录";
    
    // 1.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    // 3.设置右边的取消item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"登录" size:CGSizeMake(50, 30) target:self action:@selector(send)];
    
    // 4.添加按钮
    [self addButtonofSet];
    
    // 设置登录代理
    dal1 = [[CustomInfoDAL alloc] initWithDelegate:self];
}

#pragma mark 键盘收入
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

#pragma mark 右侧的登录按钮
-(void)send
{
    NSString *uid = UserTextField.text;
    NSString *pwd = PwdTextField.text;
    if(uid.length <= 0)
    {
     [self lockAnimationForView:usertest];
        usertest.hidden=false;
    }
    else if(pwd.length <= 0)
    {
        
        [self lockAnimationForView:pwdtest];
        pwdtest.hidden=false;
    }
    else
    {
        pwd = [[Md5Coder md5Encode:pwd] lowercaseString];
        
        
        if([dal1 login:uid and:pwd] == false)
        {
           [self setAlert:@"登录失败,请检查网络"];
        }
        else
        {
            NSLog(@"success!");
        }
        
    }
    
}

#pragma mark - CustomInfoDelegate
#pragma mark 登录的回调方法
-(void)loginCallBack:(CustomInfoModel *)item
{
    if(item.passWord.length==32)
    {
        UserModel *user = [[UserModel alloc] init];
        user.DjLsh=item.djLsh;
        user.userName=item.userName;
        user.pwdWord=item.passWord;
        user.MobilePhone=item.mobilePhone;
        user.Email=item.email;
        user.cityName=item.cityName;
        user.Address=item.address;
        user.CreateTime=item.createTime;
        
        //1. 判断数据库中是否有这条数据
        // 1)如果有就把所有的登录状态修改成0,把这行的状态修改成1
        
        // 2.如果没有的话
        // 1)先添加这个数据
        // 2)清空所有的登录状态
        // 3)吧这行数据修改为1
        
        
        // 1
        if([UserInfoSqlite addItem:user]==false) // 有这行数据
        {
            // 1)清空所有账号的isLog
            [UserInfoSqlite updateItemisLog0];
            
            // 2)设置这个账号的isLog
            [UserInfoSqlite updateItemisLog1:user];
            
            // 3)成功消息
            [self setAlert1:@"登录成功"];
            
        }
        else // 没有这行数据
        {
            // 1)清空所有账号的isLog
            [UserInfoSqlite updateItemisLog0];
            user.isLog=YES;
            // 2)先添加这个数据
            [UserInfoSqlite updateItemisLog1:user];
            // 3)弹出成功消息
          [self setAlert1:@"登录成功"];
            
        }
    }
    else
    {
        [self setAlert:@"登录失败"];
    }
}

#pragma mark 设置Alert弹出消息
-(void)setAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];

}

-(void)setAlert1:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];
    
}



#pragma mark 键盘抖动
-(void)lockAnimationForView:(UIView*)view
{
    CALayer *lbl = [view layer];
    CGPoint posLbl = [lbl position];
    CGPoint y = CGPointMake(posLbl.x-10, posLbl.y);
    CGPoint x = CGPointMake(posLbl.x+10, posLbl.y);
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:x]];
    [animation setToValue:[NSValue valueWithCGPoint:y]];
    [animation setAutoreverses:YES];
    [animation setDuration:0.08];
    [animation setRepeatCount:3];
    [lbl addAnimation:animation forKey:nil];
}





- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}
#pragma mark 左侧的取消按钮
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark 设置按钮控件
-(void)addButtonofSet
{
    /**********请输入账号***********/
    // 设置文字的位置
    UserField=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    // 设置文本内容
    UserField.text=@"用户名";
    // 设置标题文字大小
    UserField.font=[UIFont systemFontOfSize:15];
    [self.view addSubview:UserField];
    
    
    /**********请输入账号test***********/
    // 设置文字的位置
    usertest=[[UILabel alloc] initWithFrame:CGRectMake(100, 130, 180, 30)];
    // 设置文本内容
    usertest.text=@"请输入有效长度的用户名";
    usertest.hidden=true;
    usertest.highlighted=true;
    usertest.textColor=[UIColor redColor];
    // 设置标题文字大小
    usertest.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:usertest];
    
    
    
    /**********请输入密码***********/
    // 设置文字的位置
    UserField=[[UILabel alloc] initWithFrame:CGRectMake(30, 160, 100, 30)];
    // 设置文本内容
    UserField.text=@"密  码";
    
    // 设置标题文字大小
    UserField.font=[UIFont systemFontOfSize:18];
    
    [self.view addSubview:UserField];
    
    /**********请输入密码test***********/
    // 设置文字的位置
    pwdtest=[[UILabel alloc] initWithFrame:CGRectMake(100, 190, 180, 30)];
    // 设置文本内容
    pwdtest.text=@"请输入有效长度的密码";
    pwdtest.hidden=true;
    pwdtest.highlighted=true;
    pwdtest.textColor=[UIColor redColor];
    // 设置标题文字大小
    pwdtest.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:pwdtest];
    
    
    /**********输入账号***********/
    // 初始化坐标位置
    UserTextField=[[UITextField alloc] initWithFrame:CGRectMake(90, 100, 190, 30)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    UserTextField.placeholder = @"请输入您的用户名";
    // 设置点一下清除内容

    // 设置textField的形状
    UserTextField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置为YES当用点触文本字段时，字段内容会被清除,这个属性一般用于密码设置，当输入有误时情况textField中的内容
    UserTextField.clearsOnBeginEditing = YES;
    // 设置键盘完成按钮
    UserTextField.returnKeyType=UIReturnKeyDone;
    // 委托类需要遵守UITextFieldDelegate协议
    UserTextField.delegate=self;
    //设置TextFiel输入框字体大小
    UserTextField.font = [UIFont systemFontOfSize:18];
    
    
    
    
    
    /**********输入密码***********/
    
    //初始化坐标位置
    PwdTextField=[[UITextField alloc] initWithFrame:CGRectMake(90, 160, 190, 30)];
    // 设置文本文档的输入为密码
    PwdTextField.secureTextEntry=YES;
    // 设置点一下清除内容

    //为空白文本字段绘制一个灰色字符串作为占位符
    PwdTextField.placeholder = @"请输入您的密码";
    //设置textField的形状
    PwdTextField.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    PwdTextField.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    PwdTextField.delegate=self;
    //设置TextFiel输入框字体大小
    PwdTextField.font = [UIFont systemFontOfSize:18];
    
    
    
    //    把TextField添加到视图上
    [self.view addSubview:PwdTextField];
    [self.view addSubview:UserTextField];
    
}



@end
