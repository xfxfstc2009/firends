// 收藏内部
#import <UIKit/UIKit.h>
#import "CollectionModel.h"

@interface CollectionIntoViewController : UIViewController
{

    
    UILabel * goodsNameLabel;       // 商品名字
    UILabel * oldlevellLabel;       // 新旧程度
    UILabel * buyPriceLabel;        // 买来的价格
    UILabel * salePriceLabel;       // 出售的价格
    UILabel * saleSignLabel;        // 是否卖出标识
    UILabel * goodsDescLabel;       // 商品简介绍
    UILabel * createTimeLabel;      // 创建时间
    UILabel * mobilephoneLable;     // 电话号码
    
    

}
// 传参必须
@property (nonatomic,strong)NSString * tablename;
@property (nonatomic,strong)CollectionModel *collModel;
-(void)setValue:(CollectionModel *)item;




// 定义属性

@property (nonatomic,copy) NSString *goodsName; // 商品名称
@property (nonatomic,copy) NSString *oldLevell; // 新旧程度
@property (nonatomic,assign) float buyPrice;    // 采购价格
@property (nonatomic,assign) float salePrice;   // 销售报价
@property (nonatomic,assign) int saleSign;      // 出售标记
@property (nonatomic,copy) NSString *imgTitle;   // 图片标题
@property (nonatomic,copy) NSString *imgPath;   // 图片路径
@property (nonatomic,copy) NSString *imgName;   // 图片名字
@property (nonatomic,copy) NSString *goodsDesc; // 物品简介
@property (nonatomic,copy) NSString *createTime;  // 创建时间
@property(nonatomic,copy)NSString *mobilephone;  // 电话号码

//  电话按钮
@property (nonatomic,copy) UIButton *callPhone;

@end
