// 添加商品页面
#import <UIKit/UIKit.h>
#import "GoodsInfoDAL.h"
#import "CustomInfoDAL.h"
@interface AddGoodsView : UIViewController
<GoodsInfoDelegate,CustomInfoDelegate>
{
    // 设置代理
    GoodsInfoDAL *dal;

    // 用户代理
    CustomInfoDAL *dal1;
    UILabel *GoodsNameLabel;  // 物品名字
    UILabel *OldlevelLabel;   // 新旧程度
    UILabel *BuyPriceLabel;   // 买来的价格
    UILabel *SalePriceLabel;  // 出售的价格
    UILabel *GoodsDescLabel;  // 商品简介绍
    
    
    UITextField *GoodsNameField;    // 输入物品名字
    UITextField *OldlevelField;     // 输入新旧程度
    UITextField *BuyPriceField;     // 购买时的价格
    UITextField *SalePriceField;    // 出售的价格
    UITextField *GoodsDescField;    // 物品简介

    
}

// 得到当前登录状态的账户和密码
@property(nonatomic,strong)NSString * userDjlsh;
@end
