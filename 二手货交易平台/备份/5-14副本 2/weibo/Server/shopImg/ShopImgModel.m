﻿#import "ShopImgModel.h"
#import "JsonDataExchange.h"

@implementation ShopImgModel

@synthesize djLsh;
@synthesize title;
@synthesize imgPath;
@synthesize imgName;
@synthesize customID;



+(ShopImgModel *)itemWithDict:(NSDictionary *)dict
{
    ShopImgModel *item = [[ShopImgModel alloc] init];
    item.djLsh = [[dict valueForKey:@"DjLsh"] intValue];
    item.title = [dict valueForKey:@"Title"];
    item.imgPath = [dict valueForKey:@"ImgPath"];
    item.imgName = [dict valueForKey:@"ImgName"];
    item.customID = [[dict valueForKey:@"CustomID"] intValue];
    return item;
}

-(void)exchangeNil
{
    if(title == nil) title = @"";
    if(imgPath == nil) imgPath = @"";
    if(imgName == nil) imgName = @"";
}
-(NSMutableString *)getJsonValue
{
    [self exchangeNil];

    NSMutableString * jsonItem = [NSMutableString string];
    [jsonItem appendFormat:@"{"];
    [jsonItem appendFormat:@"\"DjLsh\":%i,", djLsh];
    [jsonItem appendFormat:@"\"Title\":\"%@\",", title];
    [jsonItem appendFormat:@"\"ImgPath\":\"%@\",", imgPath];
    [jsonItem appendFormat:@"\"ImgName\":\"%@\",", imgName];
    [jsonItem appendFormat:@"\"CustomID\":%i", customID];
    [jsonItem appendFormat:@"}"];
    return jsonItem;
}
-(NSMutableString *)getXmlValue
{
    [self exchangeNil];

    NSMutableString *xmlItem = [NSMutableString string];
    [xmlItem appendFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"];
    [xmlItem appendFormat:@"<item>"];
    [xmlItem appendFormat:@"<DjLsh>%i</DjLsh>",djLsh];
    [xmlItem appendFormat:@"<Title>%@</Title>",title];
    [xmlItem appendFormat:@"<ImgPath>%@</ImgPath>",imgPath];
    [xmlItem appendFormat:@"<ImgName>%@</ImgName>",imgName];
    [xmlItem appendFormat:@"<CustomID>%i</CustomID>",customID];
    [xmlItem appendFormat:@"</item>"];
    return xmlItem;
}

#pragma mark - NSCoding
-(void) encodeWithCoder: (NSCoder *) encoder
{
    [encoder encodeInt:djLsh forKey: @"djLsh"];
    [encoder encodeObject:title forKey: @"title"];
    [encoder encodeObject:imgPath forKey: @"imgPath"];
    [encoder encodeObject:imgName forKey: @"imgName"];
    [encoder encodeInt:customID forKey: @"customID"];
}
-(id) initWithCoder: (NSCoder *) decoder
{
    djLsh = [decoder decodeIntForKey:@"djLsh"];
    title = [decoder decodeObjectForKey:@"title"] ;
    imgPath = [decoder decodeObjectForKey:@"imgPath"] ;
    imgName = [decoder decodeObjectForKey:@"imgName"] ;
    customID = [decoder decodeIntForKey:@"customID"];

    return self;
}

#pragma mark - NSCopying
// 复制
-(id)copyWithZone:(NSZone *)zone
{
    ShopImgModel *newItem = [[ShopImgModel allocWithZone: zone] init];

    newItem.djLsh = self.djLsh;
    newItem.title = self.title;
    newItem.imgPath = self.imgPath;
    newItem.imgName = self.imgName;
    newItem.customID = self.customID;

	return newItem;
}

@end

