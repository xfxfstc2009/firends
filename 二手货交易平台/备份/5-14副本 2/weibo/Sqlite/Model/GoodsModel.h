// 商品模型
#import <Foundation/Foundation.h>

@interface GoodsModel : NSObject
@property (nonatomic,assign) int djLsh;         // 单据流水号
@property (nonatomic,assign) int customID;      // 客户编号
@property (nonatomic,copy) NSString *goodsName; // 商品名称
@property (nonatomic,copy) NSString *oldLevell; // 新旧程度
@property (nonatomic,assign) float buyPrice;    // 采购价格
@property (nonatomic,assign) float salePrice;   // 销售报价
@property (nonatomic,assign) int saleSign;      // 出售标记
@property (nonatomic,copy) NSString *imgTitle;   // 图片标题
@property (nonatomic,copy) NSString *imgPath;   // 图片路径
@property (nonatomic,copy) NSString *imgName;   // 图片名字
@property (nonatomic,copy) NSString *goodsDesc; // 物品简介
@property (nonatomic,copy) NSDate *createTime;  // 创建时间
@property(nonatomic,copy)NSString *mobilephone;  // 电话号码


@end
