﻿#import <Foundation/Foundation.h>

// 客户信息
@interface CustomInfoModel : NSObject
<NSCoding,NSCopying>

@property (nonatomic,assign) int djLsh; // 单据流水号
@property (nonatomic,copy) NSString *userName; // 用户名
@property (nonatomic,copy) NSString *passWord; // 密码
@property (nonatomic,copy) NSString *mobilePhone; // 手机号码
@property (nonatomic,copy) NSString *email; // 电子邮箱
@property (nonatomic,copy) NSString *cityName; // 城市名称
@property (nonatomic,copy) NSString *address; // 地址
@property (nonatomic,assign) int shop; // shop
@property (nonatomic,assign) int shopNo; // shopno
@property (nonatomic,copy) NSDate *createTime; // 创建时间

+(CustomInfoModel *)itemWithDict:(NSDictionary *)dict;

-(void)exchangeNil;               // 替换所有的nil为空字符串
-(NSMutableString *)getJsonValue; // 转换成Json字符串
-(NSMutableString *)getXmlValue;  // 转换成Xml字符串

@end

