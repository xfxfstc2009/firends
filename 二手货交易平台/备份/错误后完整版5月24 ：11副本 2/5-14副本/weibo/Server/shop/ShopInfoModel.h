﻿#import <Foundation/Foundation.h>

// shopinfo
@interface ShopInfoModel : NSObject
<NSCoding,NSCopying>

@property (nonatomic,assign) int djLsh; // 单据流水号
@property (nonatomic,copy) NSString *shopName; // shopname
@property (nonatomic,copy) NSString *shopDesc; // shopdesc
@property (nonatomic,assign) int customID; // 客户编号

+(ShopInfoModel *)itemWithDict:(NSDictionary *)dict;

-(void)exchangeNil;               // 替换所有的nil为空字符串
-(NSMutableString *)getJsonValue; // 转换成Json字符串
-(NSMutableString *)getXmlValue;  // 转换成Xml字符串

@end

