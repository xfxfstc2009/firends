//
//  RegisterViewController.h
//  Login_Register
//
//  Created by Mac on 14-3-26.
//  Copyright (c) 2014年 NanJingXianLang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomInfoDAL.h"
@class TopNavBar;

@interface RegisterViewController : UIViewController<CustomInfoDelegate>
{
    CustomInfoDAL *dal;
    

}

@property (nonatomic,strong) UITableView *registerTableView;

@property (nonatomic,strong) TopNavBar    *topNavBar;

// 获取到的城市名称 - 杭州
@property (nonatomic,strong)NSString *cityname;

// 获取到的地址
@property(nonatomic,strong)NSString *address;

@end
