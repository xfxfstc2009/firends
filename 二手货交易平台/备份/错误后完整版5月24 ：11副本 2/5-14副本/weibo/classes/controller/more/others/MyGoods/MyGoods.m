// 我发布的宝贝
#import "MyGoods.h"

@interface MyGoods ()

@end

@implementation MyGoods

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"我的宝贝";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
    // 设置右侧的发送按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleBordered target:self action:@selector(send)];
}
#pragma mark 左侧返回按钮点击事件
-(void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark 右侧的发送按钮点击事件
-(void)send
{
    NSLog(@"发送信息");
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    
    return 0;
}



@end
