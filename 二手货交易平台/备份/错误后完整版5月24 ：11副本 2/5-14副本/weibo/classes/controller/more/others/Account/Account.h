
#import <UIKit/UIKit.h>
#import "ImgUploader.h"

@interface Account : UIViewController
<UIActionSheetDelegate,UIImagePickerControllerDelegate,ImgUploaderDelegate>
{
    // 选择的照片
    UIImage *currImage;                // 当前正在传送的图片
    NSMutableArray *imageList;         // 图片数组
    NSMutableArray *imageListNeedSend; // 图片数组,需要上传的
    UIImagePickerController *imagePicker;
    
    
    
    
    // 图片上传控件
    ImgUploader *imgUploader;

    // 最后上传的图片在服务器上存放的文件夹和名称
    NSString *filePath;
    NSString *fileName;

}

@property(nonatomic,retain)IBOutlet UIScrollView *scrollview;
@property(nonatomic,strong)UIImage *imageView;




@end
