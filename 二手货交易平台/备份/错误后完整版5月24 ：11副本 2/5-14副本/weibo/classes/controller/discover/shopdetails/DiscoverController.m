
#import "DiscoverController.h"

@interface DiscoverController ()

@end

@implementation DiscoverController
@synthesize titlename;
@synthesize customID;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    // 3.设置右边的注册item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"添加" size:CGSizeMake(50, 30) target:self action:@selector(sendadd)];

    // 设置商品代理
    dalgoods = [[GoodsInfoDAL alloc] initWithDelegate:self];
}

-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
// 根据用户的djlsh进行查询发布的商品列表
-(void)sendadd
{
    [dalgoods GetGoodsList:14];
}
// 返回一个列表
-(void)getgoodsListWithDjLshCallBack:(NSMutableArray *)list
{
    NSLog(@"%@",list);
}



-(void)setValues:(ShopInfoModel*)item
{
    titlename =[NSString stringWithFormat:@"%@",item.shopName];
    self.title=titlename;
    customID=item.customID;
}



@end
