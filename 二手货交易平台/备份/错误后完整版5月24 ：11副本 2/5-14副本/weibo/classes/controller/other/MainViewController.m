
#import "MainViewController.h"
#import "HomeViewController.h"
#import "DiscoverViewController.h"
#import "MessageViewController.h"

#import "MoreViewController.h"
#import "Dock.h"


#import "UIViewController+REFrostedViewController.h"
#import "MenuViewController.h"
// 读取数据库
#import <sqlite3.h>

@interface MainViewController ()
{
    // 选中的控制器
    UIViewController *_selectedViewController;
}

@property (strong, readwrite, nonatomic) MenuViewController *menuViewController;
@end

@implementation MainViewController
// 判断是否登录来响应是否能啦出menu
@synthesize islogname;
- (void)viewDidLoad
{ 
    [super viewDidLoad];

    // 1.添加dock
    [self addDock];
    
    // 2.创建所有自控制器
    [self createChildViewControllers];

    // 3.在加载的时候默认显示为首页
    [self selecteControllerAtIndex:0];
    
    // 4.设置导航栏主题
    [self setNavigationTheme];
  // 读取数据库来读取出islogname.length==0?
  [self SearchFromDataBase];
    if(islogname.length==0)
    {
    
    }
    else
    {
        // 5. 设置右移弹出menu-(访问本地数据库，查看是否有用户登录，登录就可以右移，否则不能)
        [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
    }
   
    
}
// 右移弹出view
- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    [self.frostedViewController panGestureRecognized:sender];
}
- (void)showMenu
{
    [self.frostedViewController presentMenuViewController];
}


#pragma mark 设置导航栏主题
-(void)setNavigationTheme
{
    
    /*******ios 7 中下列方法已经不适用了********/
//    // 1.导航栏
//        // 操作navBar相当于操作整个应用中的所有导航栏
//    // 1.导航栏
//    // 1.1.操作navBar相当操作整个应用中的所有导航栏
//    UINavigationBar *navBar = [UINavigationBar appearance];
//    // 1.2.设置导航栏背景
//    [navBar setBackgroundImage:[UIImage imageNamed:@"navigationbar_background.png"] forBarMetrics:UIBarMetricsDefault];
//    // 1.3.设置状态栏背景
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;
//    // 1.4.设置导航栏的文字
//    [navBar setTitleTextAttributes:@{
//                                     UITextAttributeTextColor : [UIColor darkGrayColor],
//                                     UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetZero]
//                                     }];
    
    
    // 2.导航栏上面的item
    UIBarButtonItem *barItem =[UIBarButtonItem appearance];
    // 2.1.设置背景
    [barItem setBackgroundImage:[UIImage imageNamed:@"navigationbar_button_background.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [barItem setBackgroundImage:[UIImage imageNamed:@"navigationbar_button_background_pushed.png"] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    [barItem setBackgroundImage:[UIImage imageNamed:@"navigationbar_button_background_disable.png"] forState:UIControlStateDisabled barMetrics:UIBarMetricsDefault];
    // 2.2.设置item的文字属性
    NSDictionary *barItemTextAttr = @{
                                      UITextAttributeTextColor : [UIColor darkGrayColor],
                                      UITextAttributeTextShadowOffset : [NSValue valueWithUIOffset:UIOffsetZero],
                                      UITextAttributeFont : [UIFont systemFontOfSize:13]
                                      };
    [barItem setTitleTextAttributes:barItemTextAttr forState:UIControlStateNormal];
    [barItem setTitleTextAttributes:barItemTextAttr forState:UIControlStateHighlighted];
    
    
}


#pragma mark 重写父类的方法：添加一个子控制器
- (void)addChildViewController:(UIViewController *)childController withNav:(BOOL)booln
{
    if (booln == YES)
    {
        // 1.创建导航控制器的目的：需要一个导航条
        // 这句代码是创建导航控制器
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:childController];
        // 2.添加子控制器（包装过后的导航控制器）
        // childViewControllers里面都是导航控制器
        [super addChildViewController:nav];
    }
    else
        [super addChildViewController:childController];
    
}



#pragma mark 创建所有的子控制器
-(void)createChildViewControllers
{
    // 创建5个子控制器
    // 创建【首页】控制器
    HomeViewController *home=[[HomeViewController alloc] init];
  //  home.title=@"首页";


    [self addChildViewController:home withNav:YES];//会将自控制器添加到createChildViewControllers中去,因为createChildViewControllers方法是只读的
   // self.childViewControllers;
    
    
    
    
    
    
    // 创建【消息】控制器
    MessageViewController *message=[[MessageViewController alloc] init];
   // message.title=@"消息";

    [self addChildViewController:message withNav:YES];//会将自控制器添加到createChildViewControllers中去,因为createChildViewControllers方法是只读的

    
    
    // 创建【广场】控制器
    DiscoverViewController *discover=[[DiscoverViewController alloc] init];
    // discover.title=@"广场";
    
    [self addChildViewController:discover withNav:YES];//会将自控制器添加到createChildViewControllers中去,因为createChildViewControllers方法是只读的
    
    

    
    // 创建【更多】控制器
    MoreViewController *more=[[MoreViewController alloc] initWithStyle:UITableViewStyleGrouped];
    //  more.title=@"更多";
    
    [self addChildViewController:more withNav:NO];//会将自控制器添加到createChildViewControllers中去,因为createChildViewControllers方法是只读的
    
    
  



    
    
}


#pragma mark 添加监听事件dock
-(void)addDock
{
    // 添加dock
    // 1.显示背景
    self.view.backgroundColor=[UIColor whiteColor];
    // 2.添加Dock
    Dock *dock=[[Dock alloc] init];
    dock.frame=CGRectMake(0, self.view.frame.size.height-kDockHeight, self.view.frame.size.width, kDockHeight);
    [self.view addSubview:dock];
    
    // 2.添加dock里面的item
    [dock addDockItemWithIcon:@"tabbar_home.png" title:@"首页"];
    [dock addDockItemWithIcon:@"tabbar_message_center.png" title:@"消息"];
    [dock addDockItemWithIcon:@"tabbar_discover.png" title:@"广场"];
    [dock addDockItemWithIcon:@"tabbar_profile.png" title:@"个人设置"];

    
    // 3.监听Dock内部item的点击
    dock.itemClickBlock = ^(int index){
        /*切换上面子控制器内容,这样做的好处就是可以切换一个视图就移除掉一个视图，否则的话，是在视图上面层叠视图*/
        
//        // 第一步,移除当前的控制器
//        [_selectedViewController.view removeFromSuperview];
//        
//        // 第二步，添加新的控制器的view
//        UIViewController *new = self.childViewControllers[index];
//        // 这句话可以将dock进行切换的时候显示出来
//        new.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-kDockHeight);
//        [self.view addSubview:new.view];
//        
//        // 第三步：让新的控制器称为当前选中的控制器
//        _selectedViewController=new;
        
        [self selecteControllerAtIndex:index];
        
    };

}

#pragma mark 选中index位置对应的自控制器
-(void)selecteControllerAtIndex:(int)index
{
    /******** 更好的方法********/
    // 第0步，取出新的控制器
    UIViewController *new = self.childViewControllers[index];
    
    
    if(new==_selectedViewController)return ; // 判断点击的是不是当前已经选中的，如果是，那么就直接return；
    // 第一步,移除当前的控制器
    [_selectedViewController.view removeFromSuperview];
    
    // 第二步，添加新的控制器的view
    // 这句话可以将dock进行切换的时候显示出来
    new.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-kDockHeight);
    [self.view addSubview:new.view];
    
    // 第三步：让新的控制器称为当前选中的控制器
    _selectedViewController=new;
    
}











// 读取数据库内容，响应是否能右啦拉出menu
-(void)SearchFromDataBase
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select username from userinfo where islog = 1"];
            
            // 执行sql语句

            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    islogname = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                }
                
                
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
}




@end