#import "SandBoxImage.h"

@implementation SandBoxImage

// 去除 fileName 所有 "/"
-(NSString *)prepareName:(NSMutableString *)fileName
{
    NSString *search = @"/";
    NSString *replace = @"";
    NSRange subRange = [fileName rangeOfString:search];
    while (subRange.location != NSNotFound)
    {
        [fileName replaceCharactersInRange:subRange withString:replace];
        subRange = [fileName rangeOfString:search];
    }
    return fileName;
}

// 判断沙盒的document文件及里面有没有fileName文件
-(UIImage*)getSandBoxImage:(NSString *)fileName
{
    NSFileManager *fileManager1 = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    fileName = [self prepareName:[fileName mutableCopy]];
    
    NSString *imgPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    BOOL ifFind = [fileManager1 fileExistsAtPath:imgPath];
    if (ifFind)
    {
        NSData *data=[NSData dataWithContentsOfFile:imgPath];
        //直接把该图片读出来
        return [UIImage imageWithData:data];
    }
    else
        return nil;
}

// 保存图片到沙盒
-(BOOL)saveSandBoxImage:(UIImage*)image andName:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    fileName = [self prepareName:[fileName mutableCopy]];
    
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:fileName];// @"sms.gif"
    return [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
}

// 清空缓存
-(bool)clear
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *librarysDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtPath:librarysDirectory error:NULL];
    NSEnumerator *e = [contents objectEnumerator];
    NSString *filename;
    while ((filename = [e nextObject]))
    {
        // 判断是否未所需文件类型
        NSString *pathExtension = [[filename pathExtension] lowercaseString];
        if ([pathExtension isEqualToString:@"jpg"]
            || [pathExtension isEqualToString:@"gif"]
            || [pathExtension isEqualToString:@"png"]
            || [pathExtension isEqualToString:@"bmp"])
        {
            // 删除
            [fileManager removeItemAtPath:[librarysDirectory stringByAppendingPathComponent:filename] error:NULL];
        }
    }
    
    return true;
}

@end
