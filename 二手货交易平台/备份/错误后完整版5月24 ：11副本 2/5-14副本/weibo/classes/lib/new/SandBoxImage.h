#import <Foundation/Foundation.h>

// 沙盒图片处理类
@interface SandBoxImage : NSObject

// 去除 fileName 所有 "/"
-(NSString *)prepareName:(NSMutableString *)fileName;

// 读取沙盒图片
-(UIImage*)getSandBoxImage:(NSString *)fileName;

// 保存图片到沙盒
-(BOOL)saveSandBoxImage:(UIImage*)image andName:(NSString *)fileName;

// 清空缓存
-(bool)clear;

@end
