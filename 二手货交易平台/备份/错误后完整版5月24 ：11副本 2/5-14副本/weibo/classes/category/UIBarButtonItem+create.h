//调用这个方法，快速创建一个barbuttonitem出来

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (create)
+(UIBarButtonItem *)barButtonItemWithIcon:(NSString *)icon target:(id)target action:(SEL)action;

+ (UIBarButtonItem *)barButtonItemWithBg:(NSString *)bg title:(NSString *)title size:(CGSize)size target:(id)target action:(SEL)action;
@end
