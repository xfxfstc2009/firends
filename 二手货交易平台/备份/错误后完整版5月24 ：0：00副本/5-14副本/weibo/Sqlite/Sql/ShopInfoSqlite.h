
#import <Foundation/Foundation.h>
#import "ShopModel.h"
@interface ShopInfoSqlite : NSObject


// 新增商店
+(BOOL)addShop:(ShopModel *)item;
// 查询表中是否有数据
+(BOOL)SearchShopCountIsHave;
@end
