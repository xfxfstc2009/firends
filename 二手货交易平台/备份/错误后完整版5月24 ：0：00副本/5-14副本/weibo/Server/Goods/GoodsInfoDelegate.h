﻿#import <Foundation/Foundation.h>
#import "GoodsInfoModel.h"

// 商品信息 委托协议
@protocol GoodsInfoDelegate<NSObject>

@optional
-(void)addGoodsInfoCallBack:(BOOL)result;

-(void)updateGoodsInfoCallBack:(BOOL)result;
-(void)deleteGoodsInfoCallBack:(BOOL)result;
-(void)getGoodsInfoItemCallBack:(GoodsInfoModel *)item;
-(void)getGoodsInfoListCallBack:(NSMutableArray *)list;
-(void)getGoodsInfoPageListCallBack:(NSMutableArray *)list
                             andRecordCount:(int)recordCount
                               andPageCount:(int)pageCount
                                andPageSize:(int)pageSize
                               andPageIndex:(int)pageIndex;

-(void)getgoodsListWithDjLshCallBack:(NSMutableArray *)list;


@end

