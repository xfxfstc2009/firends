// 修改账户
#import "ChangePwd.h"
#import "CustomInfoModel.h"
#import "Md5Coder.h"
@interface ChangePwd ()

@end

@implementation ChangePwd



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 1.修改账户信息
    [self changeButtonofSet];
    
    // 2.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    // 3.设置右边的取消item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"修改" size:CGSizeMake(50, 30) target:self action:@selector(change)];
    
    // 4.设置代理
    dal = [[CustomInfoDAL alloc] initWithDelegate:self];
    
}

#pragma mark 取消
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark 修改
-(void)change
{
    
    
    NSString *uid=UserTextField.text;
    NSString *pwdnew=PwdTextFieldnew.text;
    pwdnew = [[Md5Coder md5Encode:pwdnew] lowercaseString];
    CustomInfoModel *cus=[[CustomInfoModel alloc]init];
    cus.userName=uid;
    cus.passWord=pwdnew;
    if([dal updateItem:cus]==false)
    {
        NSLog(@"远程操作失败，请检查你的网络2!");
    }
    else
    {
        NSLog(@"修改成功");
    }
    
}


#pragma mark 回调

-(void)updateCustomCallBack:(BOOL)result
{
    NSLog(@"修改成功");
}


#pragma mark 显示控件
-(void)changeButtonofSet
{
    /**********请输入账号***********/
    // 设置文字的位置
    UserField=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    // 设置文本内容
    UserField.text=@"用户名";
    // 设置标题文字大小
    UserField.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:UserField];
    
    
    /**********请输入密码***********/
    // 设置文字的位置
    UserField=[[UILabel alloc] initWithFrame:CGRectMake(30, 160, 100, 30)];
    // 设置文本内容
    UserField.text=@"密  码";
    // 设置标题文字大小
    UserField.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:UserField];
    
    /**********请再次输入密码***********/
    // 设置文字的位置
    PwdFieldAgan=[[UILabel alloc] initWithFrame:CGRectMake(0, 220, 130, 30)];
    // 设置文本内容
    PwdFieldAgan.text=@"再次输入密码";
    // 设置标题文字大小
    PwdFieldAgan.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:PwdFieldAgan];
    
    
    
    /**********输入账号***********/
    // 初始化坐标位置
    UserTextField=[[UITextField alloc] initWithFrame:CGRectMake(110, 100, 190, 30)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    UserTextField.placeholder = @"请输入您的用户名";
    // 设置点一下清除内容

    // 设置textField的形状
    UserTextField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置为YES当用点触文本字段时，字段内容会被清除,这个属性一般用于密码设置，当输入有误时情况textField中的内容
    UserTextField.clearsOnBeginEditing = YES;
    // 设置键盘完成按钮
    UserTextField.returnKeyType=UIReturnKeyDone;
    // 委托类需要遵守UITextFieldDelegate协议
    UserTextField.delegate=self;
    //设置TextFiel输入框字体大小
    UserTextField.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:UserTextField];
    
    
    
    
    /**********输入密码***********/
    
    //初始化坐标位置
    PwdTextFieldold=[[UITextField alloc] initWithFrame:CGRectMake(110, 160, 190, 30)];
    // 设置文本文档的输入为密码
    PwdTextFieldold.secureTextEntry=YES;
    // 设置点一下清除内容
    PwdTextFieldold.clearButtonMode=PwdTextFieldold;
    //为空白文本字段绘制一个灰色字符串作为占位符
    PwdTextFieldold.placeholder = @"请输入您的密码";
    //设置textField的形状
    PwdTextFieldold.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    PwdTextFieldold.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    PwdTextFieldold.delegate=self;
    //设置TextFiel输入框字体大小
    PwdTextFieldold.font = [UIFont systemFontOfSize:18];
    //    把TextField添加到视图上
    [self.view addSubview:PwdTextFieldold];
    
    /**********再次输入密码***********/
    
    //初始化坐标位置
    PwdTextFieldnew=[[UITextField alloc] initWithFrame:CGRectMake(110, 220, 190, 30)];
    // 设置文本文档的输入为密码
    PwdTextFieldnew.secureTextEntry=YES;
    // 设置点一下清除内容
    PwdTextFieldnew.clearButtonMode=PwdTextFieldnew;
    //为空白文本字段绘制一个灰色字符串作为占位符
    PwdTextFieldnew.placeholder = @"再次输入密码";
    //设置textField的形状
    PwdTextFieldnew.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    PwdTextFieldnew.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    PwdTextFieldnew.delegate=self;
    //设置TextFiel输入框字体大小
    PwdTextFieldnew.font = [UIFont systemFontOfSize:18];
    //    把TextField添加到视图上
    [self.view addSubview:PwdTextFieldnew];
}

@end
