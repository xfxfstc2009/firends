
#import <UIKit/UIKit.h>
#import "GoodsInfoModel.h"
#import "CustomInfoDAL.h"
@interface HomeDetailed : UIViewController
<CustomInfoDelegate>
    {
    UILabel *customIDLabel;       // 类型编号
    UILabel *goodsNameLabel;      // 客户编号
    UILabel *oldLevellLabel;      // 商品名称
    UILabel *buyLabel;            // 采购价格
    UILabel *saleLabel;           // 销售报价
    UILabel *goodsSignLabel;      // 销售标记
    UILabel *goodsDescLabel;      // 创建时间
      
        CustomInfoDAL *cusDal;
}
// 传入的模型
@property (nonatomic,strong) GoodsInfoModel * goodsmodel;
// 标题的名字
@property(nonatomic,strong)NSString *titlename;


@property(nonatomic,assign)int customDjlsh;
@property(nonatomic,assign)int goodsDjLsh;      //单据流水号
@property (nonatomic,assign) int customID;      // 客户编号
@property (nonatomic,copy) NSString *goodsName; // 商品名称
@property (nonatomic,copy) NSString *oldLevel; // newlevel
@property (nonatomic,assign) float buyPrice;    // 采购价格
@property (nonatomic,assign) float salePrice;   // 销售报价
@property (nonatomic,copy) NSString *goodsSign; // 商品规格
@property(nonatomic,copy) NSString *ImageTitle; // 图片名字
@property (nonatomic,copy) NSString *imgPath;   // imgpath
@property (nonatomic,copy) NSString *imgName;   // imgname
@property (nonatomic,copy) NSString *goodsDesc; // goodsdesc
@property (nonatomic,copy) NSDate   *createTime;  // 创建时间


@property (nonatomic,copy) UIButton *addToCollectiom;
@property (nonatomic,copy) UIButton *callPhone;
@property(nonatomic,strong)NSString *mobilephone;
// 链接的方法
-(void)setValues:(GoodsInfoModel*)supply;




@end
