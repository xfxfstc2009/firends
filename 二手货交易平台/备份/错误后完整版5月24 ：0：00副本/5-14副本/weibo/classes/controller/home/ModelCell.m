
#import "ModelCell.h"
#import "GoodsInfoModel.h"


@implementation ModelCell

@synthesize goodsModel;
@synthesize goodsName1;
@synthesize goodsPrice1;
@synthesize goodsdesc1;
@synthesize goodsimage1;
@synthesize createTime;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

 
}

-(void)setValues:(GoodsInfoModel *)item
{
    goodsModel = item;
    goodsName1.text = goodsModel.goodsName;

    goodsPrice1.text=[NSString stringWithFormat:@"%.1f",goodsModel.salePrice];
    goodsPrice1.textAlignment=NSTextAlignmentRight;
    goodsdesc1.text=goodsModel.goodsDesc;
    NSString *a=[NSString stringWithFormat:@"%@",goodsModel.createTime];
    NSArray *array=[a componentsSeparatedByString:@" "];
    createTime.text=array[0];

}

@end
