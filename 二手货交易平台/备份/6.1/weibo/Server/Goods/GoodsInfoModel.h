﻿#import <Foundation/Foundation.h>

// 商品信息
@interface GoodsInfoModel : NSObject
<NSCoding,NSCopying>

@property(nonatomic,assign)int customDjlsh; //当前登录的单据流水号
@property (nonatomic,assign) int djLsh; // 单据流水号
@property (nonatomic,assign) int customId; // 客户编号
@property (nonatomic,copy) NSString *goodsName; // 商品名称
@property (nonatomic,copy) NSString *oldlevel; // oldlevel
@property (nonatomic,assign) float buyPrice; // 采购价格
@property (nonatomic,assign) float salePrice; // 销售报价
@property (nonatomic,assign) int saleSign; // salesign
@property (nonatomic,copy) NSString *imageTitle; // imagetitle
@property (nonatomic,copy) NSString *imgPath; // imgpath
@property (nonatomic,copy) NSString *imgName; // imgname
@property (nonatomic,copy) NSString *goodsDesc; // goodsdesc
@property (nonatomic,copy) NSDate *createTime; // 创建时间

+(GoodsInfoModel *)itemWithDict:(NSDictionary *)dict;

-(void)exchangeNil;               // 替换所有的nil为空字符串
-(NSMutableString *)getJsonValue; // 转换成Json字符串
-(NSMutableString *)getXmlValue;  // 转换成Xml字符串

@end

