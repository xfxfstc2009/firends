//
//  UIImage+image.m
//  weibo
//
//  Created by 裴烨烽 on 14-4-29.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import "UIImage+image.h"

@implementation UIImage (image)

#pragma mark 根据屏幕尺寸返回全屏的图片


+ (UIImage *)fullscreenImageWithName:(NSString *)name
{
    if (iPhone5) {
        //        // 1.获取没有拓展名的文件名
        //        NSString *filename = [name stringByDeletingPathExtension];
        //
        //        // 2.拼接-568h@2x
        //        filename = [filename stringByAppendingString:@"-568h@2x"];
        //
        //        // 3.拼接拓展名
        //        NSString *extension = [name pathExtension];
        name = [name filenameAppend:@"-568h@2x"];
    }
    return [UIImage imageNamed:name];
}




+(UIImage *)stretchImageWithName:(NSString *)name
{
    UIImage *image = [UIImage imageNamed:name];
    
    return [image stretchableImageWithLeftCapWidth:image.size.width * 0.5 topCapHeight:image.size.height * 0.5];

}




/*************与上面这句话相同的效果*************/

//+(UIImage *)fullscreenImageWithName:(NSString *)name
//{
//if(iPhone5)
//{// 如果是iPhone 5 就改造文件
//   NSString *filename = [name stringByDeletingPathExtension];// 删除拓展名，然后返回字符串
//    // 2.拼接-568h@2x
//   filename = [filename stringByAppendingString:@"-568h@2x"];
//  
//    // 3.拼接拓展名
//    NSString *extension=[name pathExtension]; // 获取文件拓展名
//    name=[filename stringByAppendingPathExtension:extension];
//}
//    
//    return [UIImage imageNamed:name];
//}
@end
