//
//  NSString+file.m
//  weibo
//
//  Created by 裴烨烽 on 14-4-29.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import "NSString+file.h"

@implementation NSString (file)

- (NSString *)filenameAppend:(NSString *)append
{
    // 1.获取没有拓展名的文件名
    NSString *filename = [self stringByDeletingPathExtension];
    
    // 2.拼接append
    filename = [filename stringByAppendingString:append];
    
    // 3.拼接拓展名
    NSString *extension = [self pathExtension];
    
    // 4.生成新的文件名
    return [filename stringByAppendingPathExtension:extension];
}

@end
