//
//  MenuViewController.h
//  weibo
//
//  Created by 裴烨烽 on 14-5-6.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
@interface MenuViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>
// 显示名字
@property(nonatomic,strong) NSString *name;
// 显示是否登录需要注销
@property(nonatomic,strong)NSString *islog;
// 显示拥有的点卡数
@property(nonatomic,strong)NSString *money;
// 所在地区
@property(nonatomic,strong)NSString *cityname;


// 读取服务器的imgPath
@property (nonatomic,strong)NSString *imgpath;
@property (nonatomic,strong)NSString *imgname;

@end
