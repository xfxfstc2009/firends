// 修改帐户密码

#import <UIKit/UIKit.h>
#import "CustomInfoDAL.h"
@interface ChangePwd : UIViewController<UITextFieldDelegate,CustomInfoDelegate>
{
    CustomInfoDAL *dal; // 用户登录的代理方法
    
    UILabel *UserField; // 用户名
    UILabel *PwdField;  // 密码
    UILabel *PwdFieldAgan; // 再次输入密码
    
    UITextField *UserTextField;//用户名输入框
    UITextField *PwdTextFieldold;//密码输入框
    UITextField *PwdTextFieldnew;//密码输入框
    
    UIButton *changeButton; // 修改按钮

}


// 创建属性用来获得账户信息
@property (nonatomic,strong)NSString *username;
@property (nonatomic,strong)NSString *mobilephone;
@property (nonatomic,strong)NSString *email;
@property (nonatomic,strong)NSString *cityname;
@property (nonatomic,strong)NSString *address;



@end
