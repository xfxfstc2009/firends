// 我的商店
#import "MyShop.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>
@interface MyShop ()

@end

@implementation MyShop
@synthesize myscroll;// 存放选择图片的scroll
@synthesize myShopBtn;// 存放商店图片的img
@synthesize shopDescField;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // 图片上传控件
    imgUploader = [[ImgUploader alloc] initWithDelegate:self];
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    // 设置右侧的发送按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确认修改" style:UIBarButtonItemStyleBordered target:self action:@selector(send)];
    // 添加背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    // 添加店铺
    [self ShopButton];
}


-(void)send
{
    
    if(imageListNeedSend != nil && imageListNeedSend.count > 0)
    {
        // 暂存图片
        
        currImage = [imageListNeedSend objectAtIndex:0];
        
        // ==== 传送照片 ====
        // png格式
        NSData *imagedata = UIImagePNGRepresentation(currImage);
        
        // JEPG格式
        // NSData *imagedata=UIImageJEPGRepresentation(m_imgFore,1.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        NSString *temp = [NSString stringWithFormat:@"savePhoto%i.png", arc4random()%100];
        NSString *savedImagePath = [libraryDirectory stringByAppendingPathComponent:temp];
        [imagedata writeToFile:savedImagePath atomically:YES];
        
        if([imagedata writeToFile:savedImagePath atomically:YES])
        {
            // 自己写的方法
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:currImage forKey:@"img"];
            if([imgUploader sendImgWithPage:[NSString stringWithFormat:@"%@",HeadImgUpload]
                                     params:params] == false)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                                message:@"图片发送失败"
                                                               delegate:nil
                                                      cancelButtonTitle:@"关闭"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        
    }

}

-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark 设置按钮
-(void)ShopButton
{
    
    /**********商店名字***********/
    shopNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 190, 100, 30)];
    shopNameLabel.text=@"用户名:";
    shopNameLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:shopNameLabel];
    
    
    ////////////修改商店名字//////////////
    //初始化坐标位置
    shopNameField=[[UITextField alloc] initWithFrame:CGRectMake(90, 190, 190, 30)];
    // 设置文本文档的输入为密码
    shopNameField.secureTextEntry=YES;
    // 设置点一下清除内容
    //为空白文本字段绘制一个灰色字符串作为占位符
    shopNameField.placeholder = @"请输入您的密码";
    //设置textField的形状
    shopNameField.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    shopNameField.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    shopNameField.delegate=self;
    //设置TextFiel输入框字体大小
    shopNameField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:shopNameField];

    
    
    
    /**********商店简介***********/
    shopDescLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 240, 100, 30)];
    shopDescLabel.text=@"用户名:";
    shopDescLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:shopDescLabel];
    
    
    ////////////修改商店简介//////////////

    
    shopDescField = [[UITextView alloc] initWithFrame:CGRectMake(90, 240, 190, 100)];
    shopDescField.textColor = [UIColor blackColor];//设置textview里面的字体颜色
    shopDescField.font = [UIFont fontWithName:@"Arial" size:12.0];//设置字体名字和字体大小
    // 设置textview圆角
    shopDescField.backgroundColor = [UIColor whiteColor];//设置它的背景颜色

    shopDescField.layer.borderWidth =1.0;
    shopDescField.layer.cornerRadius =9.0;
    shopDescField.text = @"Now is the time for all good developers to come to serve their country.\n\nNow is the time for all good developers to come to serve their country.";//设置它显示的内容
    shopDescField.returnKeyType = UIReturnKeyDefault;//返回键的类型
    shopDescField.keyboardType = UIKeyboardTypeDefault;//键盘类型
    shopDescField.scrollEnabled = YES;//是否可以拖动
    [self.view addSubview:shopDescField];//加入到整个页面中
    
    
    //////////////传图片////////////////
    // 创建一个scroll存放图片
    myscroll=[[UIScrollView alloc]initWithFrame:CGRectMake(10, 70, 300, 100)];
    [self.view addSubview:myscroll];
    // 创建一个图片放按钮上
    UIImage *imageshow=[[UIImage alloc]initWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.madeininfi.com/UploadFiles/Head/HUD_PHOTO@2x1.png"]]];
    UIImageView *imgview=[[UIImageView alloc]initWithImage:imageshow];
    [myscroll addSubview:imgview];
    // 创建一个按钮
    myShopBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, 70, 300, 100)];
    [myShopBtn addTarget:self action:@selector(searchImage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:myShopBtn];

    
}


-(void)searchImage
{
    // 选择照片来源
    UIActionSheet *imgSrcSheet = [[UIActionSheet alloc] initWithTitle:@"选择照片来源"
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"图片库",@"相机",@"相册",nil];
    [imgSrcSheet showInView:self.view];

}







#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex >= 3)
        return; // 取消
    
    // 选择照片来源
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES; // 简单的选择区域
    switch (buttonIndex)
    {
        case 0:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; // 图片库
            break;
        case 1:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera]; // 相机
            break;
        case 2:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum]; // 相册
            break;
    }
    // 改变处理方式,模式对话框=>添加子视图
    imagePicker.view.frame = self.view.frame;
    [self.view addSubview:imagePicker.view];
}



/***************************************************************/


#pragma mark UIImagePicker 委托模式
#pragma mark - UIImagePickerControllerDelegate
// 必须实现的委托方法
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    // 保存数组,在新增的提交后提交图片、、 选择新图片，存入数组
    if(imageList == nil)
        imageList = [[NSMutableArray alloc] init];
    [imageList removeLastObject];
    [imageList addObject:image];
    
    // 这里的是点传送按钮要传输的
    if(imageListNeedSend == nil)
        imageListNeedSend = [[NSMutableArray alloc] init];
    [imageListNeedSend removeLastObject];
    [imageListNeedSend addObject:image];
    
    
    
    // 创建新控件
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 70, 300, 100)];
    imageView.image = image;
    [myscroll addSubview:imageView];
    
    // 隐藏(移除模式视图=>移除子视图)
    [imagePicker.view removeFromSuperview];
}




#pragma mark - 循环发送图片
// 发送图片
-(void)sendImage
{
    if(imageListNeedSend != nil && imageListNeedSend.count > 0)
    {
        // 暂存图片
        
        currImage = [imageListNeedSend objectAtIndex:0];
        
        // ==== 传送照片 ====
        // png格式
        NSData *imagedata = UIImagePNGRepresentation(currImage);
        
        // JEPG格式
        // NSData *imagedata=UIImageJEPGRepresentation(m_imgFore,1.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        NSString *temp = [NSString stringWithFormat:@"savePhoto%i.png", arc4random()%100];
        NSString *savedImagePath = [libraryDirectory stringByAppendingPathComponent:temp];
        [imagedata writeToFile:savedImagePath atomically:YES];
        
        if([imagedata writeToFile:savedImagePath atomically:YES])
        {
            // 自己写的方法
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:currImage forKey:@"img"];
            if([imgUploader sendImgWithPage:[NSString stringWithFormat:@"%@",HeadImgUpload1]
                                     params:params] == false)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                                message:@"图片发送失败"
                                                               delegate:nil
                                                      cancelButtonTitle:@"关闭"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        
    }
}


#pragma mark ImgUpload委托




#pragma mark - ImgUploaderDelegate
// 成功返回
-(void)uploadCallBack:(NSMutableData *)webData
{
    if(IfDebugDAL)
        NSLog(@"%@",[[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding] );
    
    //  json反序列化
    CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
    NSError *error = nil;
    NSDictionary *jsonDict = [jsonDeserializer deserializeAsDictionary:webData error:&error];
    if (error)
    {
        
        NSLog(@"图片保存失败!");
    }
    else
    {
        int sign = [[jsonDict valueForKey:@"sign"] intValue];
        
        if(sign == -1)
        {
            
            NSLog(@"图片保存失败:无图片");
        }
        else if(sign == -2)
        {
            
            NSLog(@"图片保存失败!");
        }
        else
        {
            
            filePath = [[jsonDict valueForKey:@"imgPath"] copy];
            fileName = [[jsonDict valueForKey:@"imgName"] copy];
            
            
            NSLog(@"图片发送成功!");
            
            // 把 图片 从 待发送数组中移除
            [imageListNeedSend removeObject:currImage];
            
            
            // 发送 下一张图片
            [self sendImage];
        }
    }
}
// 失败返回
-(void)uploadErrorCallBack
{
    
    NSLog(@"图片发送失败!");
}



@end
