// 账户管理
#import "AccountView.h"
#import "CJSONDeserializer.h"
#import "ColorButton.h"
#import "ChangePwd.h"
#import <sqlite3.h>
#import "CustomInfoModel.h"
@interface AccountView ()

@end

@implementation AccountView
@synthesize scrollviewimg;
@synthesize mybutton;
// 账户信息
@synthesize userDjLsh;
@synthesize username;
@synthesize password;
@synthesize mobilephone;
@synthesize email;
@synthesize cityname;
@synthesize address;
@synthesize shop;
@synthesize shopNo;
@synthesize createtime;
@synthesize imgName;
@synthesize imgPath;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // 图片上传控件
    imgUploader = [[ImgUploader alloc] initWithDelegate:self];
    // 6. 数据交互代理方法
    cusdal = [[CustomInfoDAL alloc] initWithDelegate:self];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    // 设置右侧的发送按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确认修改" style:UIBarButtonItemStyleBordered target:self action:@selector(sendImage)];
    // 添加背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    // 添加头像
    [self getUserHead];
    
    // 添加按钮
    [self AccountButton];
    // 获取账户信息
    [self SearchFromDataBase];
    // 改变原有信息
    [self ConAccount];
    
}
// leftBarButtonItem
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}




// 查找图片
-(void)searchImage
{
    // 选择照片来源
    UIActionSheet *imgSrcSheet = [[UIActionSheet alloc] initWithTitle:@"选择照片来源"
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"图片库",@"相机",@"相册",nil];
    [imgSrcSheet showInView:self.view];

}
#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex >= 3)
        return; // 取消
    
    // 选择照片来源
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES; // 简单的选择区域
    switch (buttonIndex)
    {
        case 0:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; // 图片库
            break;
        case 1:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera]; // 相机
            break;
        case 2:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum]; // 相册
            break;
    }
    // 改变处理方式,模式对话框=>添加子视图
    imagePicker.view.frame = self.view.frame;
    [self.view addSubview:imagePicker.view];
}



/***************************************************************/


#pragma mark UIImagePicker 委托模式
#pragma mark - UIImagePickerControllerDelegate
// 必须实现的委托方法
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    // 保存数组,在新增的提交后提交图片、、 选择新图片，存入数组
    if(imageList == nil)
        imageList = [[NSMutableArray alloc] init];
    [imageList removeLastObject];
    [imageList addObject:image];
    
    // 这里的是点传送按钮要传输的
    if(imageListNeedSend == nil)
        imageListNeedSend = [[NSMutableArray alloc] init];
    [imageListNeedSend removeLastObject];
    [imageListNeedSend addObject:image];
    
    
    
    // 创建新控件
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 70, 100, 100)];
    imageView.image = image;
    [self.view addSubview:imageView];
    
    // 隐藏(移除模式视图=>移除子视图)
    [imagePicker.view removeFromSuperview];
}





#pragma mark - 循环发送图片
// 发送图片
-(void)sendImage
{
    if(imageListNeedSend != nil && imageListNeedSend.count > 0)
    {
        // 暂存图片
        
        currImage = [imageListNeedSend objectAtIndex:0];
        
        // ==== 传送照片 ====
        // png格式
        NSData *imagedata = UIImagePNGRepresentation(currImage);
        
        // JEPG格式
        // NSData *imagedata=UIImageJEPGRepresentation(m_imgFore,1.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        NSString *temp = [NSString stringWithFormat:@"savePhoto%i.png", arc4random()%100];
        NSString *savedImagePath = [libraryDirectory stringByAppendingPathComponent:temp];
        [imagedata writeToFile:savedImagePath atomically:YES];
        
        if([imagedata writeToFile:savedImagePath atomically:YES])
        {
            // 自己写的方法
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:currImage forKey:@"img"];
            if([imgUploader sendImgWithPage:[NSString stringWithFormat:@"%@",HeadImgUpload]
                                     params:params] == false)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                                message:@"图片发送失败"
                                                               delegate:nil
                                                      cancelButtonTitle:@"关闭"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        
    }
}


#pragma mark ImgUpload委托




#pragma mark - ImgUploaderDelegate
// 成功返回
-(void)uploadCallBack:(NSMutableData *)webData
{
    if(IfDebugDAL)
        NSLog(@"%@",[[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding] );
    
    //  json反序列化
    CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
    NSError *error = nil;
    NSDictionary *jsonDict = [jsonDeserializer deserializeAsDictionary:webData error:&error];
    if (error)
    {
        
        NSLog(@"图片保存失败!");
    }
    else
    {
        int sign = [[jsonDict valueForKey:@"sign"] intValue];
        
        if(sign == -1)
        {
            
            NSLog(@"图片保存失败:无图片");
        }
        else if(sign == -2)
        {
            
            NSLog(@"图片保存失败!");
        }
        else
        {
            
            filePath = [[jsonDict valueForKey:@"imgPath"] copy];
            fileName = [[jsonDict valueForKey:@"imgName"] copy];
            
            CustomInfoModel *cusmodel=[[CustomInfoModel alloc]init];
            cusmodel.djLsh=userDjLsh;
            cusmodel.userName=username;
            cusmodel.passWord=password;
            cusmodel.mobilePhone=mobilephone;
            cusmodel.email=email;
            cusmodel.cityName=cityname;
            cusmodel.address=address;
            cusmodel.shop=shop;
            cusmodel.shopNo=shopNo;
            cusmodel.createTime=[NSDate date];
            cusmodel.imgPath=filePath;
            cusmodel.imgName=fileName;
            [cusdal updateItem:cusmodel];

        }
    }
}
// 失败返回
-(void)uploadErrorCallBack
{
    
    NSLog(@"图片发送失败!");
}

-(void)updateCustomCallBack:(BOOL)result
{
if(result==1)
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:@"信息修改成功"
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];

}else
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:@"信息修改不成功"
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];
}
}




////////////////////////////////////////////////////////////////////////
// 添加按钮
-(void)AccountButton
{
    
    /**********用户名***********/
    UserLabel=[[UILabel alloc] initWithFrame:CGRectMake(145, 70, 100, 30)];
    UserLabel.text=@"用户名:";
    UserLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:UserLabel];
    
    /////////用户名内容///////////
    UserLabelCon=[[UILabel alloc] initWithFrame:CGRectMake(155, 90, 100, 30)];
    UserLabelCon.text=@"当前用户";
    UserLabelCon.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:UserLabelCon];
    
    
    
    /**********密码***********/
    PassWord=[[UILabel alloc] initWithFrame:CGRectMake(145, 110, 100, 30)];
    PassWord.text=@"密码管理";
    PassWord.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:PassWord];
    
    /////////修改密码内容///////////
    NSMutableArray *colorArray = [@[[UIColor colorWithRed:0.3 green:0.278 blue:0.957 alpha:1],[UIColor colorWithRed:0.133 green:1 blue:0.843 alpha:1]] mutableCopy];
	PassWordCon = [[ColorButton alloc]initWithFrame:CGRectMake(155, 140,120, 25) FromColorArray:colorArray ByGradientType:topToBottom];
    [PassWordCon setTitle:@"修改密码" forState:UIControlStateNormal];
    PassWordCon.titleLabel.font = [UIFont systemFontOfSize: 12.0];
    [PassWordCon addTarget:self action:@selector(UpdatePassword) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:PassWordCon];
    
    
    /**********手机号码***********/
    MobilePhoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 180, 100, 30)];
    MobilePhoneLabel.text=@"手机号码:";
    MobilePhoneLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:MobilePhoneLabel];
    
    /////////手机号码///////////
    // 初始化坐标位置
    MobilePhoneField=[[UITextField alloc] initWithFrame:CGRectMake(100, 180, 170, 25)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    MobilePhoneField.placeholder = @"请输入您的手机号码";
    // 设置点一下清除内容
    MobilePhoneField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置为YES当用点触文本字段时，字段内容会被清除,这个属性一般用于密码设置，当输入有误时情况textField中的内容

    // 设置键盘完成按钮
    MobilePhoneField.returnKeyType=UIReturnKeyDone;
    //设置TextFiel输入框字体大小
    MobilePhoneField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:MobilePhoneField];
    
    
    /**********Email***********/
    EmailLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 210, 100, 30)];
    EmailLabel.text=@"E-mail:";
    EmailLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:EmailLabel];
    
    /////////Email///////////
    // 初始化坐标位置
    EmailField=[[UITextField alloc] initWithFrame:CGRectMake(100, 210, 170, 25)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    EmailField.placeholder = @"请输入您的手机号码";
    // 设置点一下清除内容
    EmailField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置键盘完成按钮
    EmailField.returnKeyType=UIReturnKeyDone;
    //设置TextFiel输入框字体大小
    EmailField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:EmailField];
    
    
    /**********城市名字***********/
    CityLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 240, 100, 30)];
    CityLabel.text=@"城市名字:";
    CityLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:CityLabel];
    
    /////////城市名字///////////
    CityField=[[UILabel alloc] initWithFrame:CGRectMake(100, 240, 100, 30)];
    CityField.text=@"当前城市名字";
    CityField.font=[UIFont systemFontOfSize:12];
    
    [self.view addSubview:CityField];
    
    
    /**********地址名字***********/
    AddressLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 270, 100, 30)];
    AddressLabel.text=@"城市名字:";
    AddressLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:AddressLabel];
    
    /////////地址名字///////////
    AddressField=[[UILabel alloc] initWithFrame:CGRectMake(100, 269, 150, 50)];
    AddressField.text=@"当前地址名字";
    AddressField.font=[UIFont systemFontOfSize:12];
    AddressField.numberOfLines = 10;
    [self.view addSubview:AddressField];
    
    
    //////////////传图片////////////////
    // 创建一个scroll存放图片
    scrollviewimg=[[UIScrollView alloc]initWithFrame:CGRectMake(30, 70, 100, 100)];
    [self.view addSubview:scrollviewimg];
    
    
    NSMutableString *imgUrl = [NSMutableString stringWithString:@""];
    [imgUrl appendString:[NSString stringWithFormat:@"%@/head/",ImageUrl]];
    [imgUrl appendString:[NSString stringWithFormat:@"%@/%@",imgPath,imgName]];
    
    
    
    // 创建一个图片放按钮上
    UIImage *imageshow=[[UIImage alloc]initWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]]];
    UIImageView *imgview=[[UIImageView alloc]initWithImage:imageshow];
    [scrollviewimg addSubview:imgview];
    // 创建一个按钮
    mybutton=[[UIButton alloc]initWithFrame:CGRectMake(30, 70, 100, 100)];
    [mybutton addTarget:self action:@selector(searchImage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mybutton];
    

}



// 修改密码
-(void)UpdatePassword
{
    ChangePwd *send = [[ChangePwd alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:send];
    [self presentViewController:nav animated:YES completion:nil];
}


-(void)SearchFromDataBase
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh,username,password,mobilephone,email,cityname,address,shop,shopno from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    userDjLsh=[[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];
                    username = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    password = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    mobilephone = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    email = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                    cityname = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 5)];
                    address = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 6)];
                    shop = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 7)]intValue];
                    shopNo = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 8)]intValue];
                    
                }
                
                
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
}

-(void)ConAccount
{

    UserLabelCon.text=username;              // 账户信息内容
    MobilePhoneField.text=mobilephone;    // 电话号码
    EmailField.text=email;          // Email
    CityField.text=cityname;           // 城市
    AddressField.text=address;        // 地址
}

// 获得用户头像
-(void)getUserHead
{
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select imgpath,imgname from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    imgPath=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    imgName = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    
                }
                
                
            }
            else
            {
                
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
    
}



@end
