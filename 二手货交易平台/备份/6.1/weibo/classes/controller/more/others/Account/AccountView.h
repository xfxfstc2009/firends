// 账户管理
#import <UIKit/UIKit.h>
#import "ImgUploader.h"// 图片上传
#import "CustomInfoDAL.h"
@interface AccountView : UIViewController
<UIActionSheetDelegate,UIImagePickerControllerDelegate,ImgUploaderDelegate,CustomInfoDelegate>
{
    
      CustomInfoDAL *cusdal;
    
    
    UILabel *UserLabel;         // 账户信息
    UILabel *PassWord;          // 密码
    UILabel *MobilePhoneLabel;  // 电话号码
    UILabel *EmailLabel;        // Email
    UILabel *CityLabel;         // 城市
    UILabel *AddressLabel;      // 地址

    
    UILabel *UserLabelCon;              // 账户信息内容
    UIButton *PassWordCon;              // 修改密码
    UITextField *MobilePhoneField;    // 电话号码
    UITextField *EmailField;          // Email
    UILabel *CityField;           // 城市
    UILabel *AddressField;        // 地址
    
    
    
    
    
    
    
    
////////// 照片管理//////////////
    // 选择的照片
    UIImage *currImage;                // 当前正在传送的图片
    NSMutableArray *imageList;         // 图片数组
    NSMutableArray *imageListNeedSend; // 图片数组,需要上传的
    UIImagePickerController *imagePicker;
    // 图片上传控件
    ImgUploader *imgUploader;
    // 最后上传的图片在服务器上存放的文件夹和名称
    NSString *filePath;
    NSString *fileName;

}
// 图片上传scrollview
///////////////传照片/////////////////
@property (nonatomic,strong) UIScrollView *scrollviewimg;
@property (nonatomic,strong) UIButton *mybutton;




// 创建属性用来获得账户信息
@property (nonatomic,assign)int userDjLsh;
@property (nonatomic,strong)NSString *username;
@property (nonatomic,strong)NSString  *password;
@property (nonatomic,strong)NSString *mobilephone;
@property (nonatomic,strong)NSString *email;
@property (nonatomic,strong)NSString *cityname;
@property (nonatomic,strong)NSString *address;
@property (nonatomic,assign)int shop;
@property (nonatomic,assign)int shopNo;
@property (nonatomic,strong)NSString *createtime;
@property (nonatomic,strong)NSString *imgPath;
@property (nonatomic,strong)NSString *imgName;



@end
