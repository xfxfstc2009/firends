#import "CustomInfoModel.h"
#import "Md5Coder.h"

#import "UserInfoSqlite.h"

// 获取地址
#import "MMLocationManager.h"

#import "RegisterViewController.h"


@interface RegisterViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic,assign) BOOL          isRead;

@end

@implementation RegisterViewController
// 获取到的城市名称
@synthesize cityname;
// 获取到的地址
@synthesize address;



- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}
- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
}

/**
 *	@brief	键盘出现
 *
 *	@param 	aNotification 	参数
 */
- (void)keyboardWillShow:(NSNotification *)aNotification

{
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.view.frame = CGRectMake(0.f, -35.f, self.view.frame.size.width, self.view.frame.size.height);
        
    }completion:nil] ;

}

/**
 *	@brief	键盘消失
 *
 *	@param 	aNotification 	参数
 */
- (void)keyboardWillHide:(NSNotification *)aNotification

{
    [UIView animateWithDuration:0.25 animations:^{
        
        self.view.frame = CGRectMake(0.f, 0.f, self.view.frame.size.width, self.view.frame.size.height);
        
    }completion:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // 设置标题
    self.title=@"用户注册";
    

    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    
    // 4.设置获取地址方法
    [self getAddress];
    [self getCity];
    
    // 6. 数据交互代理方法
    dal = [[CustomInfoDAL alloc] initWithDelegate:self];
    
    //创建tableView
    [self createTableView];
}

#pragma mark -获取 地址
-(void)getAddress
{
    __block __weak RegisterViewController *wself = self;
    [[MMLocationManager shareLocation] getAddress:^(NSString *addressString) {
        [wself setLabelText:addressString];
    }];
}

#pragma mark 获取地址显示在上面
-(void)setLabelText:(NSString *)text
{
    address=text;
}
#pragma mark 获取城市
-(void)getCity
{
    __block __weak RegisterViewController *wself = self;
    [[MMLocationManager shareLocation] getCity:^(NSString *cityString) {
        [wself setLabelText1:cityString];
    }];
}

#pragma mark 获取地址显示在上面
-(void)setLabelText1:(NSString *)text
{
    cityname=text;
}

#pragma mark 左边的取消按钮的监听事件
-(void)cancel
{
    // 返回上层
    [self dismissViewControllerAnimated:YES completion:nil];
}
/**
 *	@brief	创建TableView
 */
- (void)createTableView{

    _registerTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.f, 20, 320.f,
                                                                       (FSystenVersion >=7.0)?(ISIPHONE5?(588.f - 64.f):(490.f - 64.f)):(ISIPHONE5?(548.f - 44.f):(480.f - 44.f))) style:UITableViewStyleGrouped];
    _registerTableView.allowsSelection = NO;
    _registerTableView.delegate = self;
    _registerTableView.dataSource = self;
    [self.view addSubview:_registerTableView];
    
}

/**
 *	@brief	创建自定义导航条
 */

#pragma mark - TopNavBarDelegate Method
/**
 *	@brief	TopNavBarDelegate Method
 *
 *	@param 	index 	barItemButton 的索引值
 */

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 2) {
        return 2;
    }else{
        
        return 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cellIdentifier";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] ;

    if (indexPath.section == 0){
        
        cell.imageView.image = PNGIMAGE(@"register_user@2x");
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(50.f, 12.f, 220.f, 21.f)];
        textField.tag = Tag_AccountTextField;
        textField.returnKeyType = UIReturnKeyDone;
        textField.delegate = self;
        textField.placeholder = @"用户名,必填";
        [cell addSubview:textField];
        

        
    }else if (indexPath.section == 1){
        

        cell.imageView.image = PNGIMAGE(@"register_recommand_people@2x");
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(50.f, 12.f, 220.f, 21.f)];
        textField.tag = Tag_RecommadTextField;
        textField.returnKeyType = UIReturnKeyDone;
        textField.delegate = self;
        textField.placeholder = @"电话,必填";
        [cell addSubview:textField];
        
        
    }else if (indexPath.section == 2){
        
        if (indexPath.row == 0) {
            
            cell.imageView.image = PNGIMAGE(@"register_password@2x");
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(50.f, 12.f, 220.f, 21.f)];
            textField.tag = Tag_TempPasswordTextField;
            textField.returnKeyType = UIReturnKeyDone;
            textField.secureTextEntry = YES;
            textField.delegate = self;
            textField.placeholder = @"密码,必填";
            [cell addSubview:textField];
    
            
        }else if (indexPath.row == 1){
            
            cell.imageView.image = PNGIMAGE(@"register_password@2x");
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(50.f, 12.f, 220.f, 21.f)];
            textField.tag = Tag_ConfirmPasswordTextField;
            textField.returnKeyType = UIReturnKeyDone;
            textField.secureTextEntry = YES;
            textField.delegate = self;
            textField.placeholder = @"确认密码,必填";
            [cell addSubview:textField];
      
        }
        
    }else if (indexPath.section ==3){
        cell.imageView.image = PNGIMAGE(@"register_email@2x");
        UITextField *textField= [[UITextField alloc] initWithFrame:CGRectMake(50.f, 12.f, 220.f, 21.f)];
        textField.tag = Tag_EmailTextField;
        textField.returnKeyType = UIReturnKeyDone;
        textField.delegate = self;
        textField.placeholder = @"邮箱,必填";
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        [cell addSubview:textField];
        


    }else if (indexPath.section == 4){
        
        cell.imageView.image = PNGIMAGE(@"register_lbs@2x");
        NSString *citynamelabel=[NSString stringWithFormat:@"你所在的位置为:%@",cityname];
        UILabel *label = [Utils labelWithFrame:CGRectMake(50.f, 14.f, 200.f, 21.f) withTitle:citynamelabel titleFontSize:[UIFont systemFontOfSize:14.f] textColor:[UIColor lightGrayColor] backgroundColor:[UIColor clearColor] alignment:NSTextAlignmentLeft];
        label.tag = Tag_SourceLabel;
        [cell addSubview:label];
        
        UIButton *sourceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sourceBtn.frame = CGRectMake(260.f, 0.f, 50.f, 44.f);
        [sourceBtn addTarget:self action:@selector(sourceBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [sourceBtn setImage:[UIImage imageNamed:@"mypoisition@2x"] forState:UIControlStateNormal];
        [cell addSubview:sourceBtn];
        
    }else if (indexPath.section == 5){
        
        UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        registerBtn.frame = CGRectMake((FSystenVersion >= 7.0)?0.f:10.f, 0.f, (FSystenVersion>=7.0)?320.f:300.f, 44.f);
        [registerBtn addTarget:self action:@selector(registerBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [registerBtn setBackgroundImage:[UIImage imageNamed:@"register_btn@2x"] forState:UIControlStateNormal];
        [registerBtn setTitle:@"提交" forState:UIControlStateNormal];
        [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        registerBtn.titleLabel.font = [UIFont systemFontOfSize:18.f];
        [registerBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        
        [cell addSubview:registerBtn];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        
        return nil;
    }else if (section == 1){
        
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 21.f)];
        footerView.backgroundColor = [UIColor clearColor];
        UILabel *label = [Utils labelWithFrame:CGRectMake(10.f, 0.f, 300.f, 21.f) withTitle:@"注册后不可更改，3~20位字符，可包含英文、数字和“_”" titleFontSize:[UIFont systemFontOfSize:10.f] textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] alignment:NSTextAlignmentLeft];
        [footerView addSubview:label];
        
        return footerView ;
    }else if (section == 2){
        
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 21.f)];
        footerView.backgroundColor = [UIColor clearColor];
        UILabel *label = [Utils labelWithFrame:CGRectMake(10.f, 0.f, 300.f, 21.f) withTitle:@"6位字符以上，可包含数字、字母（区分大小写）" titleFontSize:[UIFont systemFontOfSize:10.f] textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] alignment:NSTextAlignmentLeft];
        [footerView addSubview:label];
        return footerView ;
    }else if (section == 3){
        
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 21.f)];
        footerView.backgroundColor = [UIColor clearColor];
        UILabel *label = [Utils labelWithFrame:CGRectMake(10.f, 0.f, 300.f, 21.f) withTitle:@"请填写您的邮箱" titleFontSize:[UIFont systemFontOfSize:10.f] textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] alignment:NSTextAlignmentLeft];
        [footerView addSubview:label];
        return footerView ;
    }else if (section == 4){
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 21.f)];
        footerView.backgroundColor = [UIColor clearColor];
        
        UIButton *isReadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        isReadBtn.frame = CGRectMake(10.f, 0.f, 21.f, 21.f);
        [isReadBtn setImage:[UIImage imageNamed:@"isRead_waiting_selectButton@2x"] forState:UIControlStateNormal];
        [isReadBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        isReadBtn.tag = Tag_isReadButton;
        [footerView addSubview:isReadBtn];
        
        UILabel *label1 = [Utils labelWithFrame:CGRectMake(35.f, 0.f, 70.f, 21.f) withTitle:@"我已阅读并同意" titleFontSize:[UIFont systemFontOfSize:10.f] textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] alignment:NSTextAlignmentLeft];
        [footerView addSubview:label1];
        
        UIButton *servicesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        servicesBtn.frame = CGRectMake(110.f, 0.f, 40.f, 21.f);
        [servicesBtn setTitle:@"服务协议" forState:UIControlStateNormal];
        [servicesBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        servicesBtn.titleLabel.font = [UIFont systemFontOfSize:10.f];
        [servicesBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [servicesBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        servicesBtn.tag = Tag_servicesButton;
        [footerView addSubview:servicesBtn];
        
        UILabel *label2 = [Utils labelWithFrame:CGRectMake(155.f, 0.f, 10.f, 21.f) withTitle:@"和" titleFontSize:[UIFont systemFontOfSize:10.f] textColor:[UIColor blackColor] backgroundColor:[UIColor clearColor] alignment:NSTextAlignmentLeft];
        [footerView addSubview:label2];
        
        UIButton *privacyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        privacyBtn.frame = CGRectMake(170.f, 0.f, 40.f, 21.f);
        [privacyBtn setTitle:@"隐私协议" forState:UIControlStateNormal];
        [privacyBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        privacyBtn.titleLabel.font = [UIFont systemFontOfSize:10.f];
        [privacyBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [privacyBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        privacyBtn.tag = Tag_privacyButton;
        [footerView addSubview:privacyBtn];
        
        return footerView ;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5.0;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 5.f;
    }else if(section == 4){
        
        return 30.f;
        
    }else{
        return 21.f;
    }
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    return [[UIView alloc] initWithFrame:CGRectZero];
}


#pragma mark - UIButtonClicked Method
- (void)buttonClicked:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case Tag_isReadButton:
        {
            //是否阅读协议
            if (_isRead) {
                
                [btn setImage:[UIImage imageNamed:@"isRead_waiting_selectButton@2x"] forState:UIControlStateNormal];
                _isRead = NO;
            }else{
                
                [btn setImage:[UIImage imageNamed:@"isRead_selectedButton@2x"] forState:UIControlStateNormal];
                
                _isRead = YES;
            }
        }
            break;
        case Tag_servicesButton:
        {
            //服务协议
            [Utils alertTitle:@"提示" message:@"您点击了服务协议" delegate:nil cancelBtn:@"取消" otherBtnName:nil];
        }
            break;
        case Tag_privacyButton:
        {
            //隐私协议
            [Utils alertTitle:@"提示" message:@"您点击了隐私协议" delegate:nil cancelBtn:@"取消" otherBtnName:nil];
        }
            break;
            
        default:
            break;
    }
    
}
#pragma mark - sourceBtnClicked Method
- (void)sourceBtnClicked:(id)sender{
    NSString *addresslabel=[NSString stringWithFormat:@"当前位置:%@",address];
    
    [Utils alertTitle:@"提示" message:addresslabel delegate:nil cancelBtn:@"确定" otherBtnName:nil];
}

#pragma mark - RegisterBtnClicked Method
- (void)registerBtnClicked:(id)sender{
    
    
    if (!_isRead) {
        [Utils alertTitle:@"提示" message:@"请勾选阅读协议选项框" delegate:nil cancelBtn:@"确定" otherBtnName:nil];
    }else{
        
        if ([self checkValidityTextField]) {
            
            NSString * uid= [(UITextField *)[self.view viewWithTag:Tag_AccountTextField] text];// 账户
            NSString * pwd=[(UITextField *)[self.view viewWithTag:Tag_TempPasswordTextField] text];// 密码
            NSString * phone=[(UITextField *)[self.view viewWithTag:Tag_RecommadTextField] text];// 电话
            NSString * email=[(UITextField *)[self.view viewWithTag:Tag_EmailTextField] text];// 电话
              pwd = [[Md5Coder md5Encode:pwd] lowercaseString];
            CustomInfoModel *cus=[[CustomInfoModel alloc]init];
            cus.passWord=pwd;
            cus.userName=uid;
            cus.cityName=cityname;
            cus.address=address;
            cus.mobilePhone=phone;
            cus.email=email;
            
            if([dal addItem:cus]== false)
            {
                 [Utils alertTitle:@"提示" message:@"请检查网络" delegate:nil cancelBtn:@"确定" otherBtnName:nil];
                
            }

 
        }
    }
}
-(void)addCustomCallBack:(BOOL)item
{
     [Utils alertTitle:@"提示" message:@"服务器注册成功" delegate:nil cancelBtn:@"确定" otherBtnName:nil];
}

/**
 *	@brief	验证文本框是否为空
 */
#pragma mark checkValidityTextField Null
- (BOOL)checkValidityTextField
{
    
    if ([(UITextField *)[self.view viewWithTag:Tag_EmailTextField] text] == nil || [[(UITextField *)[self.view viewWithTag:Tag_EmailTextField] text] isEqualToString:@""]) {
        
        [Utils alertTitle:@"提示" message:@"邮箱不能为空" delegate:self cancelBtn:@"取消" otherBtnName:nil];
        
        return NO;
    }
    if ([(UITextField *)[self.view viewWithTag:Tag_AccountTextField] text] == nil || [[(UITextField *)[self.view viewWithTag:Tag_AccountTextField] text] isEqualToString:@""]) {
        
        [Utils alertTitle:@"提示" message:@"用户名不能为空" delegate:self cancelBtn:@"取消" otherBtnName:nil];
        
        return NO;
    }
    if ([(UITextField *)[self.view viewWithTag:Tag_TempPasswordTextField] text] == nil || [[(UITextField *)[self.view viewWithTag:Tag_TempPasswordTextField] text] isEqualToString:@""]) {
        
        [Utils alertTitle:@"提示" message:@"用户密码不能为空" delegate:self cancelBtn:@"取消" otherBtnName:nil];
        
        return NO;
    }
    if ([(UITextField *)[self.view viewWithTag:Tag_ConfirmPasswordTextField] text] == nil || [[(UITextField *)[self.view viewWithTag:Tag_ConfirmPasswordTextField] text] isEqualToString:@""]) {
        
        [Utils alertTitle:@"提示" message:@"用户确认密码不能为空" delegate:self cancelBtn:@"取消" otherBtnName:nil];
        
        return NO;
    }
    
    return YES;
    
}

#pragma mark - UITextFieldDelegate Method

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField.tag == Tag_RecommadTextField) {
        
        [UIView animateWithDuration:0.25 animations:^{
            
            self.view.frame = CGRectMake(0.f, -35.f, self.view.frame.size.width, self.view.frame.size.height);
            
        }completion:nil] ;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
            
        case Tag_EmailTextField:
        {
            if ([textField text] != nil && [[textField text] length]!= 0) {
                
                if (![Utils isValidateEmail:textField.text]) {
                    
                    [Utils alertTitle:@"提示" message:@"邮箱格式不正确" delegate:nil cancelBtn:@"取消" otherBtnName:nil];
                }
            }
        }
            break;
        case Tag_TempPasswordTextField:
        {
            if ([textField text] != nil && [[textField text] length]!= 0) {
                
                if ([[textField text] length] < 6) {
                    
                    [Utils alertTitle:@"提示" message:@"用户密码小于6位！" delegate:nil cancelBtn:@"取消" otherBtnName:nil];
                }
            }
        }
            break;
        case Tag_ConfirmPasswordTextField:
        {
            if ([[(UITextField *)[self.view viewWithTag:Tag_TempPasswordTextField] text] length] !=0 && ([textField text]!= nil && [[textField text] length]!= 0)) {
                
                if (![[(UITextField *)[self.view viewWithTag:Tag_TempPasswordTextField] text] isEqualToString:[textField text]]) {
                    [Utils alertTitle:@"提示" message:@"两次输入的密码不一致" delegate:nil cancelBtn:@"取消" otherBtnName:nil];
                }
            }
        }
            break;
        case Tag_RecommadTextField:
        {
            [UIView animateWithDuration:0.25 animations:^{
                
                self.view.frame = CGRectMake(0.f, (FSystenVersion >= 7.0)?0.f:20.f, self.view.frame.size.width, self.view.frame.size.height);
                
            }completion:nil];
        }
            break;
            
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - touchMethod
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    [self allEditActionsResignFirstResponder];
}

#pragma mark - PrivateMethod
- (void)allEditActionsResignFirstResponder{
    
    //邮箱
    [[self.view viewWithTag:Tag_EmailTextField] resignFirstResponder];
    //用户名
    [[self.view viewWithTag:Tag_AccountTextField] resignFirstResponder];
    //temp密码
    [[self.view viewWithTag:Tag_TempPasswordTextField] resignFirstResponder];
    //确认密码
    [[self.view viewWithTag:Tag_ConfirmPasswordTextField] resignFirstResponder];
    //推荐人
    [[self.view viewWithTag:Tag_RecommadTextField] resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
