// 商店是否注册
#import <UIKit/UIKit.h>
#import "ShopInfoDAL.h"
#import "CustomInfoDAL.h"
@interface UpdateShop : UIViewController<ShopInfoDelegate,CustomInfoDelegate>
{
    ShopInfoDAL *shopdal;
    CustomInfoDAL *customdal;
    UILabel *shopName; // 商店名字
    UILabel *shopDesc;  // 商店简介
    
    UITextField *shopNameField;//输入商店名字
    UITextField *shopDescField;//输入商店简介
    
    UIButton *loadButton; // 登录按钮
}

// 当前登录的单据流水号
@property(nonatomic,assign)int userdjlsh;
@property(nonatomic,strong)NSString *userNamefromSql;
@property(nonatomic,strong)NSString *passwordfromSql;

// 修改用户信息
@property(nonatomic,strong)NSString *username;
@property(nonatomic,strong)NSString *password;
@property(nonatomic,strong)NSString *mobilephone;
@property(nonatomic,strong)NSString *email;
@property(nonatomic,strong)NSString *cityname;
@property(nonatomic,strong)NSString *address;
@property(nonatomic,assign)int shop;
@property(nonatomic,assign)int shopno;
@property(nonatomic,strong)NSString *createtime;



// 取列表获得当前账户
@property(nonatomic,strong)NSMutableArray *shoplist;

// 获取当前商店的单据流水号
@property(nonatomic,assign)int shopdjlsh;
@end
