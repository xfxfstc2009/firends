
#import "DiscoverViewController.h"
#import "PlaceAddressCell.h"
#import "KKContactCell.h"
#import "HomeDetailed.h"
#import "GoodsInfoModel.h"
#import <sqlite3.h>
#import "ShopView.h"
#import "DiscoverController.h"
#import "UpdateShop.h"
@interface DiscoverViewController ()
{
    DiscoverController *discoverController;
}
@end

@implementation DiscoverViewController
// 取列表
@synthesize goodsList;
@synthesize arraycopy;
// 商店列表
@synthesize shopList;

/******/
@synthesize username;
@synthesize password;
@synthesize shopOpen;
@synthesize shopNo;
// 商店总数
@synthesize shopCount;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // 4.文件代理方法
    dal = [[GoodsInfoDAL alloc] initWithDelegate:self];
    dalforcus = [[CustomInfoDAL alloc] initWithDelegate:self];
    dalforshop=[[ShopInfoDAL alloc]initWithDelegate:self];
    
    // 运行此段代码，去执行查询有多少条商店
    [self SearchShopCountFromSql];
    
    // 设置标题
    self.title=@"广场";
    // 添加左上角店铺
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"navigationbar_compose.png" target:self action:@selector(sendStatus)];
    
    mainTable.delegate = self;
    mainTable.dataSource = self;
    // 取出列表，得到列表的count
    [self getListCount];
    // 取出商店列表，得到商店的count
    [self getShopList];

    _array = [[NSMutableArray alloc]init];
    for (int i=1;i<shopCount+1;i++)
    {
        NSString *a=[NSString stringWithFormat:@"%i",i];
        [_array addObject:a];
    }
    didSection = _array.count+1;
    [self performSelector:@selector(firstOneClicked) withObject:self afterDelay:0.2f];
}



#pragma mark - Table view data source
- (void)firstOneClicked{
    didSection = 0;
    endSection = 0;
    [self didSelectCellRowFirstDo:YES nextDo:NO];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == didSection) {
        return 2;
    }
    return 0;
}



#pragma mark 点击进入某行
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0) {
        static NSString *CellIdentifier = @"PlaceAddressCell";
        
        PlaceAddressCell *cell=(PlaceAddressCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell==nil)
        {
            NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"PlaceAddressCell" owner:self options:nil];
            for(id oneObject in nibs)
            {
                if([oneObject isKindOfClass:[PlaceAddressCell class]])
                {
                    cell = (PlaceAddressCell *)oneObject;
                    
                }
            }
            cell.shopInfoModel = [shopList objectAtIndex:indexPath.section];
            [cell setValues];
           
        }
        return cell;
    }
    else{
        static NSString *CellIdentifier = @"KKContactCell";
        
        KKContactCell *cell=(KKContactCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell==nil)
        {
            NSArray *nibs=[[NSBundle mainBundle] loadNibNamed:@"KKContactCell" owner:self options:nil];
            for(id oneObject in nibs)
            {
                if([oneObject isKindOfClass:[KKContactCell class]])
                {
                    cell = (KKContactCell *)oneObject;
                }
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.shopModel = [shopList objectAtIndex:indexPath.section];
            [cell setValues];
        }
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 获得到一个组和一个行号
    if (discoverController == nil) {
        discoverController = [[DiscoverController alloc] init];
    }
    
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:discoverController];
    
    discoverController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:nav animated:YES completion:^{
        
        [discoverController setValues:self.shopList[indexPath.section]];
    }];
    
    
    
}




#pragma mark 返回一共多少行
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _array.count;
}
#pragma mark 返回多少高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == didSection) {
        return 45;
    }
    return 0;
}
#pragma mark 店铺图片的高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 110;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *mView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 110)];
    [mView setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *logoView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 300, 90)];
    [logoView setImage:[UIImage imageNamed:[_array objectAtIndex:section]]];
    [mView addSubview:logoView];
    
    
    if (section<_array.count-1) {
        UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 109, 320, 1)];
        [lineView setImage:[UIImage imageNamed:@"XX0022"]];
        [mView addSubview:lineView];
        
    }
    UIButton *bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt setFrame:CGRectMake(0, 0, 320, 110)];
    [bt setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [bt setTag:section];
    [bt.titleLabel setFont:[UIFont systemFontOfSize:20]];
    [bt.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [bt.titleLabel setTextColor:[UIColor blueColor]];
    [bt addTarget:self action:@selector(addCell:) forControlEvents:UIControlEventTouchUpInside];
    [mView addSubview:bt];
    return mView;
}
- (void)addCell:(UIButton *)bt{
    endSection = bt.tag;
    if (didSection==_array.count+1) {
        ifOpen = NO;
        didSection = endSection;
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    else{
        if (didSection==endSection) {
            [self didSelectCellRowFirstDo:NO nextDo:NO];
        }
        else{
            [self didSelectCellRowFirstDo:NO nextDo:YES];
        }
    }
}


- (void)didSelectCellRowFirstDo:(BOOL)firstDoInsert nextDo:(BOOL)nextDoInsert{
    [mainTable beginUpdates];
    ifOpen = firstDoInsert;
    NSMutableArray* rowToInsert = [[NSMutableArray alloc] init];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:didSection];
    [rowToInsert addObject:indexPath];
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:didSection];
    [rowToInsert addObject:indexPath1];
    if (!ifOpen) {
        didSection = _array.count+1;
        [mainTable deleteRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }else{
        [mainTable insertRowsAtIndexPaths:rowToInsert withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [mainTable endUpdates];
    if (nextDoInsert) {
        didSection = endSection;
        [self didSelectCellRowFirstDo:YES nextDo:NO];
    }
    [mainTable scrollToNearestSelectedRowAtScrollPosition:UITableViewScrollPositionTop animated:YES];
}









////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#pragma mark 左上角的添加商店按钮
-(void)sendStatus
{
    // 1.首先查一下现在登录的账号的djlsh
    // 2.通过djlsh去服务器去找对应的shop==1；
    // 3.if(shop==1)
    //{
    // 添加店铺内容
    // }
    // else
    // {
    //  开通
    // }
    //****************************//
    //1.查询一下本地数据库的登录账户,得到账户和密码
    [self SearchIsLogFromDataBase0];
    if(username.length==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                        message:@"您还没有登录，请登录后开通店铺"
                                                       delegate:nil
                                              cancelButtonTitle:@"关闭"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
    //2.查询服务器
    [dalforcus login:username and:password];
        //3. 接回调方法
    }
}


#pragma mark 取出列表，然后得到有多少条数据
-(void)getListCount
{
    // 取列表
    [dal getList];
}

#pragma mark callback 方法
-(void)getGoodsInfoListCallBack:(NSMutableArray *)list
{
    self.goodsList=list;
    
}

-(void)getShopList
{
    [dalforshop getList];
}

-(void)getShopInfoListCallBack:(NSMutableArray *)list
{
    NSLog(@"%i",list.count);
    
    shopList = [[NSMutableArray alloc]init];
    shopList=list;
}

-(void)SearchShopCountFromSql
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select shopcount from shopcount where djlsh=1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    shopCount = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}






//1.查询一下本地数据库的登录账户,得到账户和密码
-(void)SearchIsLogFromDataBase0
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select username,password from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    username = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    password = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}
//2.查询服务器获得当前的item
-(void)loginCallBack:(CustomInfoModel *)item
{
    // 商店是否打开
    shopOpen =item.shop;
    // 商店编号
    shopNo=item.shopNo;
    // 3.判断店铺是否打开
    if(shopOpen==1)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                        message:@"您已经开通一家店铺，无法再次开通"
                                                       delegate:nil
                                              cancelButtonTitle:@"关闭"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        ShopView *shopview = [[ShopView alloc] initWithNibName:nil bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:shopview];
        [self presentViewController:nav animated:YES completion:nil];
    }
}



@end