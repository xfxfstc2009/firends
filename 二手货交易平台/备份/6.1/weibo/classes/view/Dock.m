// 主控制器底部的选项卡
#import "Dock.h"
#import "DockItem.h"

@interface Dock()
{
// 当前选中了那个item
    DockItem *_currentItem;
}
@end


@implementation Dock

// init 方法内部会调用initWithFrame 方法
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // 设置背景(拿到image进行平铺)
        self.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tabbar_background.png"]];
    }
    return self;
}


#pragma mark 添加item
// 添加一个选项卡（图标、文字标题）
-(void)addDockItemWithIcon:(NSString *)icon title:(NSString *)title
{
    // 创建item
    DockItem *item=[DockItem buttonWithType:UIButtonTypeCustom];
    [self addSubview:item];
    // 设置文字
    [item setTitle:title forState:UIControlStateNormal];
    // 设置图片
    [item setImage:[UIImage imageNamed:icon] forState:UIControlStateNormal];
    [item setImage:[UIImage imageNamed:[icon filenameAppend:@"_selected"]] forState:UIControlStateSelected];

    // 监听点击
    [item addTarget:self action:@selector(itemClick:) forControlEvents:UIControlEventTouchDown];// 当用户按下去就进行监听调方法
    
    // 调整item的边框-遍历所有的item然后进行宽度的计算
    [self adjustDockItemsFrame];
    
}

#pragma mark 点击了某个item
-(void)itemClick:(DockItem *)item
{
    // 让新的item取消选中
    _currentItem.selected=NO;
    
    // 让新的item选中
    item.selected=YES;
    
    // 让新的item变为当前选中
    _currentItem=item;
    
    
    // 4.调用block, 进行监听
    if (_itemClickBlock) {
        _itemClickBlock(item.tag);
    }
}







#pragma mark 调整item的边框
-(void)adjustDockItemsFrame
{
    int count=self.subviews.count;
    
    //计算出item的尺寸
    CGFloat itemWidth=self.frame.size.width/count;
    CGFloat itemHright=self.frame.size.height;
    for(int i=0;i<count;i++)
    {
        // 1.取出子控件
        DockItem *item=self.subviews[i];
    
        // 2.计算出边框
        item.frame=CGRectMake(i*itemWidth, 0, itemWidth, itemHright);
    if(i==0)
    {
        // 默认第0个item选中
        item.selected=YES;
        _currentItem=item;
    }
        // 4.设置item的tag
        item.tag = i;
    
    }
}




#pragma mark 重写设置选中索引的方法
- (void)setSelectedIndex:(int)selectedIndex
{
    // 1.条件过滤
    if (selectedIndex < 0 || selectedIndex >= self.subviews.count) return;
    
    // 2.赋值给成员变量
    _selectedIndex = selectedIndex;
    
    // 3.对应的item
    DockItem *item = self.subviews[selectedIndex];
    
    // 4.相当于点击了这个item
    [self itemClick:item];
}



@end
