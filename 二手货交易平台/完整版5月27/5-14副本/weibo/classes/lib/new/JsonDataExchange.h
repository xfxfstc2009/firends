#import <Foundation/Foundation.h>

// json的时间 <==> NSDate
// eg: Date(1388725950000+0800) = 2014-1-3 13:12:30

@interface JsonDataExchange : NSObject

+(NSDate *)JsonTimeToNSDate:(NSString *)json;
+(NSDate *)NSDateMin;
+(NSDate *)NSDateNow;
+(NSString *)JsonTimeFromNSDate:(NSDate *)date;
+(NSString *)JsonTimeNow;
+(NSString *)JsonTimeMin;

+(NSString *)NSStringNowWithFormat:(NSString *)formatStr;
+(NSString *)NSStringFromNSDate:(NSDate *)date
                         format:(NSString *)formatStr;
+(NSDate *)NSDateFromNSString:(NSString *)string
                       format:(NSString *)formatStr;

@end
