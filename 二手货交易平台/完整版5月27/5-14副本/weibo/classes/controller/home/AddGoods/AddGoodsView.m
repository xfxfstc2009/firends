// 添加商品页面
#import "AddGoodsView.h"
#import "GoodsInfoModel.h"
#import <sqlite3.h>
#import "GoodsInfoSqlite.h"
#import "UserModel.h"
// 可以滚动的scrollview
#import "TPKeyboardAvoidingScrollView.h"


#import "CJSONDeserializer.h"
@implementation AddGoodsView

// 查本地数据库，然后得到用户名
@synthesize userDjlsh;

// 存放数组内容
@synthesize pickerData;
@synthesize picker;

@synthesize oldlevelfield;


// 传图片
@synthesize mybutton;
@synthesize scrollviewimg;

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}


- (void)viewDidLoad
{
    // 图片上传控件
    imgUploader = [[ImgUploader alloc] initWithDelegate:self];
    
    // 1.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];

    // 4. 数据交互代理方法
    dal = [[GoodsInfoDAL alloc] initWithDelegate:self];
    // 5.设置按钮
    [self addButtonSet];

    // 读取当前登陆的islog=1然后去设置发布的内容
    [self SearchFromDataBase];
    // 接上面的已知登录账户密码得到单据流水号
//    [self searchFromseverWithDjLsh];
    
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    
    
    // 3.设置右边的添加item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"添加" size:CGSizeMake(50, 30) target:self action:@selector(sendImage)];
    
    
    ///////////////////////////////设置picker///////////////////////////
    NSArray *dataArray = [[NSArray alloc]initWithObjects:@"九五成新",@"九成新",@"八成新",@"七成新",@"六成新", nil];
    pickerData=dataArray;
    self.oldlevelfield.inputView = picker;
}

#pragma mark 左边的取消按钮的监听事件
-(void)cancel
{
    // 返回上层
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 右边的传输内容事件
-(void)sendadd
{
    // 创建存放到服务器数据库的对象
    GoodsInfoModel *addgoods=[[GoodsInfoModel alloc]init];
    addgoods.customId=[userDjlsh intValue];
    addgoods.goodsName=GoodsNameField.text;
    addgoods.oldlevel=oldlevelfield.text;
    addgoods.buyPrice=[BuyPriceField.text floatValue];
    addgoods.salePrice=[SalePriceField.text floatValue];
    addgoods.goodsDesc=GoodsDescField.text;
    addgoods.imgPath=filePath;
    addgoods.imgName=fileName;
    addgoods.saleSign=0;
    addgoods.createTime=[NSDate date];
    // 进行存储
    if([dal addItem:addgoods]==false)
    {
        [self setAlert:@"添加失败"];
    }
    else
    {
        
    }
}

-(void)addGoodsInfoCallBack:(BOOL)result
{
    if(result==true)
    {
        [self setAlert:@"添加成功"];
    }
    else
    {
        [self setAlert:@"添加失败"];
    }
}


#pragma mark -得到当前用户
#pragma mark 读取本地数据库获得当前登陆帐号
-(void)SearchFromDataBase
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select Djlsh from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    userDjlsh = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                }
                
                
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
}


#pragma mark 设置Alert弹出消息
-(void)setAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];
    
}

#pragma mark 添加按钮等控件
-(void)addButtonSet
{
    
    
    scroll = [[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    scroll.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scroll];
    
    /**********物品名字***********/
    GoodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 130, 100, 30)];
    GoodsNameLabel.text=@"商品名称:";
    GoodsNameLabel.font=[UIFont systemFontOfSize:12];
    [scroll addSubview:GoodsNameLabel];
    
    /**********新旧程度***********/
    OldlevelLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 170, 100, 30)];
    OldlevelLabel.text=@"新旧程度:";
    OldlevelLabel.font=[UIFont systemFontOfSize:12];
    [scroll addSubview:OldlevelLabel];
    
    /**********购买时的价格***********/
    BuyPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 210, 100, 30)];
    BuyPriceLabel.text=@"购买时价格";
    BuyPriceLabel.font=[UIFont systemFontOfSize:12];
    [scroll addSubview:BuyPriceLabel];
    
    /**********出售的价格***********/
    SalePriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 250, 100, 30)];
    SalePriceLabel.text=@"商品出售价格";
    SalePriceLabel.font=[UIFont systemFontOfSize:12];
    [scroll addSubview:SalePriceLabel];
    
    /**********商品简介***********/
    GoodsDescLabel=[[UILabel alloc] initWithFrame:CGRectMake(35, 290, 100, 30)];
    GoodsDescLabel.text=@"商品简介";
    GoodsDescLabel.font=[UIFont systemFontOfSize:12];
    [scroll addSubview:GoodsDescLabel];
    

    
    /**********商品名称***********/
    GoodsNameField=[[UITextField alloc] initWithFrame:CGRectMake(130, 130, 150, 30)];
    GoodsNameField.placeholder = @"请输入商品名称";
    GoodsNameField.borderStyle=UITextBorderStyleRoundedRect;
    GoodsNameField.clearsOnBeginEditing = YES;
    GoodsNameField.returnKeyType=UIReturnKeyDone;
    GoodsNameField.font = [UIFont systemFontOfSize:12];
    [scroll addSubview:GoodsNameField];
    
    
    /**********采购价格***********/
    oldlevelfield=[[UITextField alloc] initWithFrame:CGRectMake(130, 170, 150, 30)];
    oldlevelfield.placeholder = @"请输入新旧程度";
    oldlevelfield.borderStyle=UITextBorderStyleRoundedRect;
    oldlevelfield.clearsOnBeginEditing = YES;
    oldlevelfield.returnKeyType=UIReturnKeyDone;
    oldlevelfield.font = [UIFont systemFontOfSize:12];
    [scroll addSubview:oldlevelfield];
    
    /**********销售报价***********/
    BuyPriceField=[[UITextField alloc] initWithFrame:CGRectMake(130, 210, 150, 30)];
    BuyPriceField.placeholder = @"请输入您的出售价格";
    BuyPriceField.borderStyle=UITextBorderStyleRoundedRect;
    BuyPriceField.clearsOnBeginEditing = YES;
    BuyPriceField.returnKeyType=UIReturnKeyDone;
    BuyPriceField.font = [UIFont systemFontOfSize:12];
    [scroll addSubview:BuyPriceField];
    
    /**********销售报价***********/
    SalePriceField=[[UITextField alloc] initWithFrame:CGRectMake(130, 250, 150, 30)];
    SalePriceField.placeholder = @"请输入您的出售价格";
    SalePriceField.borderStyle=UITextBorderStyleRoundedRect;
    SalePriceField.clearsOnBeginEditing = YES;
    SalePriceField.returnKeyType=UIReturnKeyDone;
    SalePriceField.font = [UIFont systemFontOfSize:12];
    [scroll addSubview:SalePriceField];
    
    /**********销售报价***********/
    GoodsDescField=[[UITextField alloc] initWithFrame:CGRectMake(130, 290, 150, 30)];
    GoodsDescField.placeholder = @"请输入您的物品简介";
    GoodsDescField.borderStyle=UITextBorderStyleRoundedRect;
    GoodsDescField.clearsOnBeginEditing = YES;
    GoodsDescField.returnKeyType=UIReturnKeyDone;
    GoodsDescField.font = [UIFont systemFontOfSize:12];
    [scroll addSubview:GoodsDescField];
    
    picker=[[UIPickerView alloc] initWithFrame:CGRectMake(110, 100, 150, 30)];
    picker.delegate=self;
    picker.showsSelectionIndicator=YES;
    
    
    //////////////传图片////////////////
    
    scrollviewimg=[[UIScrollView alloc]initWithFrame:CGRectMake(30, 10, 100, 100)];
    [scroll addSubview:scrollviewimg];
    
    
    UIImage *imageshow=[[UIImage alloc]initWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.madeininfi.com/UploadFiles/Head/HUD_PHOTO@2x1.png"]]];
    UIImageView *imgview=[[UIImageView alloc]initWithImage:imageshow];
 
    [scrollviewimg addSubview:imgview];
    
    
    mybutton=[[UIButton alloc]initWithFrame:CGRectMake(30, 10, 100, 100)];
    [mybutton addTarget:self action:@selector(searchImage) forControlEvents:UIControlEventTouchUpInside];
    
    [scroll addSubview:mybutton];

    
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [scroll adjustOffsetToIdealIfNeeded];
}



////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
//picker

// 返回当前的列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
//返回当前列显示的行数
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [pickerData count];
}

#pragma mark Picker Delegate Methods

//返回当前行的内容,此处是将数组中数值添加到滚动的那个显示栏上
-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerData objectAtIndex:row];
}


#pragma mark 选中了某一行就会调用
#pragma mark 选择某一行某一列的内容
-(void)pickerView:(UIPickerView *)pickerView
     didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    NSString *left=self.pickerData[row];
    NSLog(@"%@",left);
    self.oldlevelfield.text =left;
    
    
}


#pragma mark - 隐藏软键盘





///////////////////////////////////////////
// 查找图片
-(void)searchImage
{
    // 选择照片来源
    UIActionSheet *imgSrcSheet = [[UIActionSheet alloc] initWithTitle:@"选择照片来源"
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"图片库",@"相机",@"相册",nil];
    [imgSrcSheet showInView:self.view];
}





#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex >= 3)
        return; // 取消
    
    // 选择照片来源
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES; // 简单的选择区域
    switch (buttonIndex)
    {
        case 0:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; // 图片库
            break;
        case 1:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera]; // 相机
            break;
        case 2:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum]; // 相册
            break;
    }
    // 改变处理方式,模式对话框=>添加子视图
    imagePicker.view.frame = self.view.frame;
    [self.view addSubview:imagePicker.view];
}



/***************************************************************/


#pragma mark UIImagePicker 委托模式
#pragma mark - UIImagePickerControllerDelegate
// 必须实现的委托方法
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    // 保存数组,在新增的提交后提交图片、、 选择新图片，存入数组
    if(imageList == nil)
        imageList = [[NSMutableArray alloc] init];
    [imageList removeLastObject];
    [imageList addObject:image];
    
    // 这里的是点传送按钮要传输的
    if(imageListNeedSend == nil)
        imageListNeedSend = [[NSMutableArray alloc] init];
    [imageListNeedSend removeLastObject];
    [imageListNeedSend addObject:image];
    
    
    
    // 创建新控件
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.image = image;
    [scrollviewimg addSubview:imageView];
    
    // 隐藏(移除模式视图=>移除子视图)
    [imagePicker.view removeFromSuperview];
}





#pragma mark - 循环发送图片
// 发送图片
-(void)sendImage
{
    if(imageListNeedSend != nil && imageListNeedSend.count > 0)
    {
        // 暂存图片
        
        currImage = [imageListNeedSend objectAtIndex:0];
        
        // ==== 传送照片 ====
        // png格式
        NSData *imagedata = UIImagePNGRepresentation(currImage);
        
        // JEPG格式
        // NSData *imagedata=UIImageJEPGRepresentation(m_imgFore,1.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        NSString *temp = [NSString stringWithFormat:@"savePhoto%i.png", arc4random()%100];
        NSString *savedImagePath = [libraryDirectory stringByAppendingPathComponent:temp];
        [imagedata writeToFile:savedImagePath atomically:YES];
        
        if([imagedata writeToFile:savedImagePath atomically:YES])
        {
            // 自己写的方法
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:currImage forKey:@"img"];
            if([imgUploader sendImgWithPage:[NSString stringWithFormat:@"%@",HeadImgUpload]
                                     params:params] == false)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                                message:@"图片发送失败"
                                                               delegate:nil
                                                      cancelButtonTitle:@"关闭"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        
    }
}


#pragma mark ImgUpload委托




#pragma mark - ImgUploaderDelegate
// 成功返回
-(void)uploadCallBack:(NSMutableData *)webData
{
    if(IfDebugDAL)
        NSLog(@"%@",[[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding] );
    
    //  json反序列化
    CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
    NSError *error = nil;
    NSDictionary *jsonDict = [jsonDeserializer deserializeAsDictionary:webData error:&error];
    if (error)
    {
        
        NSLog(@"图片保存失败!");
    }
    else
    {
        int sign = [[jsonDict valueForKey:@"sign"] intValue];
        
        if(sign == -1)
        {
            
            NSLog(@"图片保存失败:无图片");
        }
        else if(sign == -2)
        {
            
            NSLog(@"图片保存失败!");
        }
        else
        {
            
            filePath = [[jsonDict valueForKey:@"imgPath"] copy];
            fileName = [[jsonDict valueForKey:@"imgName"] copy];
            
            [self sendadd];
        }
    }
}
-(void)updateHeadCallBack:(BOOL)result
{
    NSLog(@"123");
}

// 失败返回
-(void)uploadErrorCallBack
{
    
    NSLog(@"图片发送失败!");
}



@end
