// 添加商品页面
#import <UIKit/UIKit.h>
#import "GoodsInfoDAL.h"
#import "CustomInfoDAL.h"
#import "HeadInfoDAL.h"
#import "ImgUploader.h"

// 可以滚动的 scrollview
@class TPKeyboardAvoidingScrollView;
@interface AddGoodsView : UIViewController
<GoodsInfoDelegate,CustomInfoDelegate,UIPickerViewDelegate,UIPickerViewDataSource,ImgUploaderDelegate>
{
    // 设置代理
    GoodsInfoDAL *dal;
    // 用户代理
    CustomInfoDAL *dal1;
    
    UILabel *GoodsNameLabel;  // 物品名字
    UILabel *OldlevelLabel;   // 新旧程度
    UILabel *BuyPriceLabel;   // 买来的价格
    UILabel *SalePriceLabel;  // 出售的价格
    UILabel *GoodsDescLabel;  // 商品简介绍
    
    
    UITextField *GoodsNameField;    // 输入物品名字
//  UITextField *OldlevelField;     // 输入新旧程度
    UITextField *BuyPriceField;     // 购买时的价格
    UITextField *SalePriceField;    // 出售的价格
    UITextField *GoodsDescField;    // 物品简介

    UIPickerView *pick;             // 创建一个picker
    
    // 在view上面添加一个scrollview
    TPKeyboardAvoidingScrollView *scroll;
    
    
    //////////////传照片//////////////////
    // 选择的照片
    UIImage *currImage;                // 当前正在传送的图片
    NSMutableArray *imageList;         // 图片数组
    NSMutableArray *imageListNeedSend; // 图片数组,需要上传的
    UIImagePickerController *imagePicker;
    // 图片上传控件
    ImgUploader *imgUploader;
    
    // 最后上传的图片在服务器上存放的文件夹和名称
    NSString *filePath;
    NSString *fileName;
   
}

// 得到当前登录状态的账户和密码
@property(nonatomic,strong)NSString * userDjlsh;

// 创建一个pick数组来存放内容信息
@property (nonatomic,strong)NSArray *pickerData;
// 创建一个picker
@property (nonatomic,strong)UIPickerView *picker;

// 上面的新旧程度必须开属性
@property(nonatomic,strong)UITextField *oldlevelfield;
// 使view可以滚动
@property (nonatomic, strong) TPKeyboardAvoidingScrollView *scrollView;


///////////////传照片/////////////////
@property (nonatomic,strong) UIScrollView *scrollviewimg;
@property (nonatomic,strong) UIButton *mybutton;

@end
