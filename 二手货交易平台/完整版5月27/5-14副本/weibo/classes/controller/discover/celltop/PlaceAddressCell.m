#import "DiscoverController.h"
#import "PlaceAddressCell.h"

@implementation PlaceAddressCell
@synthesize goodsmodel;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setValues
{
    self.detailLabel.text = self.shopInfoModel.shopName;
}

@end
