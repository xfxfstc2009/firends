
#import <UIKit/UIKit.h>
#import "GoodsInfoModel.h"
#import "ShopInfoModel.h"
@interface PlaceAddressCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *detailLabel;

@property(nonatomic,strong)GoodsInfoModel *goodsmodel;
@property(nonatomic,strong)ShopInfoModel *shopInfoModel;

-(void)setValues;

@end
