// 商店是否打开
#import "UpdateShop.h"
#import <sqlite3.h>
#import "ShopModel.h"
#import "ShopInfoSqlite.h"
@interface UpdateShop ()

@end

@implementation UpdateShop
@synthesize userdjlsh;
@synthesize userNamefromSql;
@synthesize passwordfromSql;


// 得到整个userModel
@synthesize username;
@synthesize password;
@synthesize mobilephone;
@synthesize email;
@synthesize cityname;
@synthesize address;
@synthesize shop;
@synthesize shopno;
@synthesize createtime;


// 取列表获得当前账户
@synthesize shoplist;

// 获取当前商店单据流水号
@synthesize shopdjlsh;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    
    // 右边的开通
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"修改" style:UIBarButtonItemStyleBordered target:self action:@selector(send)];
    // 设置背景颜色
    [self.view setBackgroundColor:[UIColor whiteColor]];
   
    [self TryToLogin];
    [self SearchIsLogFromDataBase0];
    [self SearchShopDjLshFromDataBase];
    // 设置按钮
    [self addButtonofSet];
    // 设置代理
    shopdal = [[ShopInfoDAL alloc] initWithDelegate:self];
    customdal = [[CustomInfoDAL alloc] initWithDelegate:self];
    // 尝试去登录一下得到当前的item
    
}

// 返回
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 发送开通
-(void)send
{
    
    
    /**********将数据存入服务器**********/
    
    
    ShopInfoModel *shopmodel=[[ShopInfoModel alloc]init];
    shopmodel.djLsh=shopdjlsh;
    shopmodel.shopName=shopNameField.text;
    shopmodel.shopDesc=shopDescField.text;
    shopmodel.customID=userdjlsh ;
    if([shopdal updateItem:shopmodel]== false)
    {
        NSLog(@"修改失败");
    }
    
    
    
}



-(void)updateShopInfoCallBack:(BOOL)result
{
    NSLog(@"修改成功");
}

-(void)updateCustomShop
{
    /**********将服务器中用户表中的商店名字修改为1**********/
    CustomInfoModel *custommodel=[CustomInfoModel alloc];
    custommodel.djLsh=userdjlsh;
    custommodel.userName=username;
    custommodel.passWord=password;
    custommodel.mobilePhone=mobilephone;
    custommodel.email=email;
    custommodel.cityName=cityname;
    custommodel.address=address;
    custommodel.shop=1;
    custommodel.shopNo=shopdjlsh;
    [customdal updateItem:custommodel];
}

#pragma mark 回调函数
-(void)addShopInfoCallBack:(BOOL)result
{
    // 1. 取列获得当前的列表
    [shopdal getList];
}

-(void)getShopInfoListCallBack:(NSMutableArray *)list
{
    self.shoplist=list;
    // 将取得的列表数据存入本地数据库
    [self loadIntoSqlite];
    // 存完了以后去本地数据库取得当前的商店单据
    [self SearchShopDjLshFromDataBase];
    //再次运行一遍
    [self updateCustomShop];
    
}

// 将取得的列表放到本地数据库中
-(void)loadIntoSqlite
{
    for(int i=0;i<shoplist.count;i++)
    {
        ShopInfoModel *shopmodel=shoplist[i];
        // 将goodsmodel存入数据库
        ShopModel *sqlshopmodel=[[ShopModel alloc]init];
        sqlshopmodel.djLsh=shopmodel.djLsh;
        sqlshopmodel.shopName=shopmodel.shopName;
        sqlshopmodel.shopDesc=shopmodel.shopDesc;
        sqlshopmodel.customID=shopmodel.customID;
        // 添加到数据库
        [ShopInfoSqlite addShop:sqlshopmodel];
    }
    
}

#pragma mark 修改的回调函数
-(void)updateCustomCallBack:(BOOL)result
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                    message:@"添加成功"
                                                   delegate:nil
                                          cancelButtonTitle:@"关闭"
                                          otherButtonTitles:nil];
    [alert show];
}

#pragma mark 去登录一下，返回一个整体的CustomModel
-(void)TryToLogin
{
    [customdal login:userNamefromSql and:passwordfromSql];
}
-(void)loginCallBack:(CustomInfoModel *)item
{
    username=item.userName;
    password=item.passWord;
    mobilephone=item.mobilePhone;
    email=item.email;
    cityname=item.cityName;
    address=item.address;
    shop=item.shop;
    shopno=item.shopNo;
    createtime=[NSString stringWithFormat:@"%@",item.createTime];
}

#pragma mark 查询当前登录账户的djlsh
-(void)SearchIsLogFromDataBase0
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh,username,password from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    userdjlsh = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];
                    userNamefromSql = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    passwordfromSql = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}


#pragma mark 查询当前通过当前登录的djlsh去取得商店的djlsh
-(void)SearchShopDjLshFromDataBase
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh from shopinfo where customid = %i",userdjlsh];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    shopdjlsh = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}



/////////******************添加按钮********************//////////
-(void)addButtonofSet
{
    /**********请输入账号***********/
    // 设置文字的位置
    shopName=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    // 设置文本内容
    shopName.text=@"商店名字";
    // 设置标题文字大小
    shopName.font=[UIFont systemFontOfSize:15];
    [self.view addSubview:shopName];
    
    
    /**********请输入密码***********/
    // 设置文字的位置
    shopDesc=[[UILabel alloc] initWithFrame:CGRectMake(30, 160, 100, 30)];
    // 设置文本内容
    shopDesc.text=@"商店简介";
    
    // 设置标题文字大小
    shopDesc.font=[UIFont systemFontOfSize:15];
    [self.view addSubview:shopDesc];
    
    
    
    
    /**********输入账号***********/
    // 初始化坐标位置
    shopNameField=[[UITextField alloc] initWithFrame:CGRectMake(110, 100, 180, 30)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    shopNameField.placeholder =@"请输入要修改的店名";
    // 设置点一下清除内容
    
    // 设置textField的形状
    shopNameField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置为YES当用点触文本字段时，字段内容会被清除,这个属性一般用于密码设置，当输入有误时情况textField中的内容
    shopNameField.clearsOnBeginEditing = YES;
    // 设置键盘完成按钮
    shopNameField.returnKeyType=UIReturnKeyDone;
    // 委托类需要遵守UITextFieldDelegate协议
    shopNameField.delegate=self;
    //设置TextFiel输入框字体大小
    shopNameField.font = [UIFont systemFontOfSize:18];
    
    
    
    
    
    /**********输入密码***********/
    
    //初始化坐标位置
    shopDescField=[[UITextField alloc] initWithFrame:CGRectMake(110, 160, 180, 30)];
    // 设置文本文档的输入为密码
    shopDescField.secureTextEntry=YES;
    // 设置点一下清除内容
    
    //为空白文本字段绘制一个灰色字符串作为占位符
    shopDescField.placeholder = @"请输入要修改的店的简介";
    //设置textField的形状
    shopDescField.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    shopDescField.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    shopDescField.delegate=self;
    //设置TextFiel输入框字体大小
    shopDescField.font = [UIFont systemFontOfSize:18];
    
    
    
    //    把TextField添加到视图上
    [self.view addSubview:shopNameField];
    [self.view addSubview:shopDescField];
    
}
@end