
#import <UIKit/UIKit.h>
#import "GoodsInfoModel.h"
#import "ShopInfoModel.h"
#import "GoodsInfoDAL.h"

@interface DiscoverController : UITableViewController
<GoodsInfoDelegate>
{
    GoodsInfoDAL *dalgoods; // 用户登录的代理方法
}
// 传入的模型1
@property (nonatomic,strong) GoodsInfoModel * goodsmodel;

// 传入的模型2
@property (nonatomic,strong) ShopInfoModel * shopInfoModel;

// 标题的名字
@property(nonatomic,strong)NSString *titlename;
// 获得店用户所拥有的goods的客户编号
@property(nonatomic,assign)int customID;

// 链接的方法
-(void)setValues:(ShopInfoModel *)item;

// 创建一个可变数组来接收
@property(nonatomic,strong)NSMutableArray *goodsArray;

@end
