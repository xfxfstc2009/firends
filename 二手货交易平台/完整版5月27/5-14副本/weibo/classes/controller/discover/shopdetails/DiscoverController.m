
#import "DiscoverController.h"
#import "ModelCell.h"
#import "ShopDetailed.h"
@interface DiscoverController ()
{
    ShopDetailed *shopDetailed;
}
@end

@implementation DiscoverController
@synthesize titlename;
@synthesize customID;

// 接收有多少商品的可变数组
@synthesize goodsArray;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    

    // 设置商品代理
    dalgoods = [[GoodsInfoDAL alloc] initWithDelegate:self];
    
}

-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}






- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
	return goodsArray.count;
}

#pragma mark cell 复用
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *ID = @"Cell";
    ModelCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    // 2.如果缓存池中没有，才需要传入一个标识创建新的Cell
    if (cell == nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ModelCell" owner:self options:nil]lastObject];
    }
  
    GoodsInfoModel* goodsmodel = self.goodsArray[indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%i",goodsmodel.customId];
    // 设置CELL背景透明
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor] ;
    GoodsInfoModel *model=self.goodsArray[indexPath.row];
    cell.goodsModel=model;
    [cell setValues:model];
    return cell;
}

// 返回每一行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77;
}

#pragma mark tableview delegate 选中某一行 进行跳转
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 获得到一个组和一个行号
    if (shopDetailed == nil) {
        shopDetailed = [[ShopDetailed alloc] init];
    }
    
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:shopDetailed];
    
    shopDetailed.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:nav animated:YES completion:^{
        
        [shopDetailed setValues:self.goodsArray[indexPath.section]];
    }];
    
    
    
}










// 根据用户的djlsh进行查询发布的商品列表
-(void)sendadd
{
    [dalgoods GetGoodsList:customID];
    NSLog(@"%i",goodsArray.count);
}
// 返回一个列表
-(void)getgoodsListWithDjLshCallBack:(NSMutableArray *)list
{
    goodsArray=list;
    [self.tableView reloadData];
}



-(void)setValues:(ShopInfoModel*)item
{
    titlename =[NSString stringWithFormat:@"%@",item.shopName];
    self.title=titlename;
    customID=item.customID;
    // 根据customid去服务器端获得列表
 [dalgoods GetGoodsList:customID];
}



@end
