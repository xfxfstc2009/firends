//
//  MoreViewController.h
//  weibo
//
//  Created by 裴烨烽 on 14-4-29.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  Account;
@class Suggest;
@class AboutUs;

@interface MoreViewController : UITableViewController<UIAlertViewDelegate,UIActionSheetDelegate>

@property (nonatomic,strong) NSArray *data;
@property (nonatomic,strong) UIImage *bgImage;
@property (nonatomic,strong) UIButton *btnB;
@property (nonatomic,strong) AboutUs *aboutUs;
@property (nonatomic,strong) UIViewController *selectedController;
@property (nonatomic,strong) Suggest *suggest;
@property (nonatomic,strong)UIImageView *img;

@end