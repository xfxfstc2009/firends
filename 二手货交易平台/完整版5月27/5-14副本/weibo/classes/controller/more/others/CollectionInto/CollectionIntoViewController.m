//  我的收藏内部页面
#import "CollectionIntoViewController.h"

@interface CollectionIntoViewController ()

@end

@implementation CollectionIntoViewController
@synthesize tablename;
@synthesize collModel;


// 属性
@synthesize goodsName;      // 商品名称
@synthesize oldLevell;      // 新旧程度
@synthesize buyPrice;       // 采购价格
@synthesize salePrice;      // 销售报价
@synthesize saleSign;       // 出售标记
@synthesize imgTitle;       // 图片标题
@synthesize imgPath;        // 图片路径
@synthesize imgName;        // 图片名字
@synthesize goodsDesc;      // 物品简介
@synthesize createTime;     // 创建时间
@synthesize mobilephone;    // 电话号码

@synthesize callPhone;      // 电话按钮
- (void)viewDidLoad
{
    [super viewDidLoad];

    // 1.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    [self addButtonSet];
}

// 取消
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


// 链接方法
-(void)setValue:(CollectionModel *)item
{
    goodsNameLabel.text=item.goodsName;       // 商品名称
    oldlevellLabel.text=item.oldLevell;       // 新旧程度
    buyPriceLabel.text=[NSString stringWithFormat:@"%.2f",item.buyPrice];         // 采购价格
    salePriceLabel.text=[NSString stringWithFormat:@"%.2f",item.salePrice];       // 销售报价
    saleSignLabel.text=[NSString stringWithFormat:@"%i",item.saleSign];         // 出售标记
    /////*****图片****/////
    imgTitle=item.imgTitle;         // 图片标题
    imgPath=item.imgPath;           // 图片路径
    imgName=item.imgName;           // 图片名字
    
    goodsDescLabel.text=item.goodsDesc;       // 物品简介
    createTimeLabel.text=item.createTime;     // 创建时间
    mobilephone=item.mobilephone;   // 电话号码

}



UILabel * createTimeLabel;
UILabel * mobilephoneLable;
#pragma mark 添加按钮
-(void)addButtonSet
{
    /**********商品名称***********/
    goodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    goodsNameLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsNameLabel];
    
    /**********新旧程度***********/
    oldlevellLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 140, 100, 30)];
    oldlevellLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:oldlevellLabel];
    
    /**********买来价格***********/
    buyPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 180, 100, 30)];
    buyPriceLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:buyPriceLabel];
    
    /**********销售报价***********/
    salePriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 220, 100, 30)];
    salePriceLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:salePriceLabel];
    
    /**********销售报价***********/
    saleSignLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 260, 100, 30)];
    saleSignLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:saleSignLabel];
    
    /**********销售报价***********/
    goodsDescLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 300, 100, 30)];
    goodsDescLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsDescLabel];
    
    /**********销售报价***********/
    goodsDescLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 340, 100, 30)];
    goodsDescLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsDescLabel];
    

    
    //加入电话//
    callPhone=[[UIButton alloc]initWithFrame:CGRectMake(90, 420, 100, 30)];
    [callPhone setTitle:@"拨打电话" forState:UIControlStateNormal];
    [callPhone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [callPhone addTarget:self action:@selector(call) forControlEvents:UIControlEventTouchUpInside];
    [callPhone setBackgroundImage:[UIImage imageNamed:@"chatfrom_bg_normal.png"] forState:UIControlStateNormal];
    [self.view addSubview:callPhone];
    
}

-(void)call
{
    NSMutableString *phoneNumber=[NSMutableString string];
    [phoneNumber appendFormat:@"tel://"];
    [phoneNumber appendFormat:@"%@",mobilephone];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}
@end
