
#import "Account.h"
#import "CJSONDeserializer.h"
@implementation Account
@synthesize scrollview;
@synthesize imageView;

@synthesize mybutton;

- (void)viewDidLoad
{
    [super viewDidLoad];

    // 图片上传控件
    imgUploader = [[ImgUploader alloc] initWithDelegate:self];
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    
    
    // 设置右侧的发送按钮
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确认修改" style:UIBarButtonItemStyleBordered target:self action:@selector(send)];
   
    UIImage *imageshow=[[UIImage alloc]initWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://www.kutx.cn/xiaotupian/icons/png/200803/20080327095245737.png"]]];
  
    [mybutton setBackgroundImage:imageshow forState:UIControlStateNormal];

    // 设置代理
    headdal=[[HeadInfoDAL alloc]initWithDelegate:self];
}


-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)send
{
    // 开始传送图片
    [self sendImage];
}

#pragma mark 选择不同的呈现方式

- (IBAction)imageshow:(id)sender
{
    // 选择照片来源
    UIActionSheet *imgSrcSheet = [[UIActionSheet alloc] initWithTitle:@"选择照片来源"
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"图片库",@"相机",@"相册",nil];
    [imgSrcSheet showInView:self.view];

}




#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex >= 3)
        return; // 取消
    
    // 选择照片来源
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES; // 简单的选择区域
    switch (buttonIndex)
    {
        case 0:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary]; // 图片库
            break;
        case 1:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera]; // 相机
            break;
        case 2:
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum]; // 相册
            break;
    }
    // 改变处理方式,模式对话框=>添加子视图
    imagePicker.view.frame = self.view.frame;
    [self.view addSubview:imagePicker.view];
}



/***************************************************************/ 


#pragma mark UIImagePicker 委托模式
#pragma mark - UIImagePickerControllerDelegate
// 必须实现的委托方法
-(void)imagePickerController:(UIImagePickerController *)picker
       didFinishPickingImage:(UIImage *)image
                 editingInfo:(NSDictionary *)editingInfo
{
    // 保存数组,在新增的提交后提交图片、、 选择新图片，存入数组
    if(imageList == nil)
        imageList = [[NSMutableArray alloc] init];
    [imageList removeLastObject];
    [imageList addObject:image];
    
    // 这里的是点传送按钮要传输的
    if(imageListNeedSend == nil)
        imageListNeedSend = [[NSMutableArray alloc] init];
    [imageListNeedSend removeLastObject];
    [imageListNeedSend addObject:image];
    


    // 创建新控件
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    imageView.image = image;
    [scrollview addSubview:imageView];

    // 隐藏(移除模式视图=>移除子视图)
    [imagePicker.view removeFromSuperview];
}





#pragma mark - 循环发送图片
// 发送图片
-(void)sendImage
{
    if(imageListNeedSend != nil && imageListNeedSend.count > 0)
    {
        // 暂存图片

        currImage = [imageListNeedSend objectAtIndex:0];
        
        // ==== 传送照片 ====
        // png格式
        NSData *imagedata = UIImagePNGRepresentation(currImage);
        
        // JEPG格式
        // NSData *imagedata=UIImageJEPGRepresentation(m_imgFore,1.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        NSString *temp = [NSString stringWithFormat:@"savePhoto%i.png", arc4random()%100];
        NSString *savedImagePath = [libraryDirectory stringByAppendingPathComponent:temp];
        [imagedata writeToFile:savedImagePath atomically:YES];
        
        if([imagedata writeToFile:savedImagePath atomically:YES])
        {
            // 自己写的方法
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setValue:currImage forKey:@"img"];
            if([imgUploader sendImgWithPage:[NSString stringWithFormat:@"%@",HeadImgUpload]
                                     params:params] == false)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                                message:@"图片发送失败"
                                                               delegate:nil
                                                      cancelButtonTitle:@"关闭"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
     
    }
}


#pragma mark ImgUpload委托




#pragma mark - ImgUploaderDelegate
// 成功返回
-(void)uploadCallBack:(NSMutableData *)webData
{
    if(IfDebugDAL)
        NSLog(@"%@",[[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding] );
    
    //  json反序列化
    CJSONDeserializer *jsonDeserializer = [CJSONDeserializer deserializer];
    NSError *error = nil;
    NSDictionary *jsonDict = [jsonDeserializer deserializeAsDictionary:webData error:&error];
    if (error)
    {
     
        NSLog(@"图片保存失败!");
    }
    else
    {
        int sign = [[jsonDict valueForKey:@"sign"] intValue];
        
        if(sign == -1)
        {

            NSLog(@"图片保存失败:无图片");
        }
        else if(sign == -2)
        {
          
            NSLog(@"图片保存失败!");
        }
        else
        {

            filePath = [[jsonDict valueForKey:@"imgPath"] copy];
            fileName = [[jsonDict valueForKey:@"imgName"] copy];
            
            HeadInfoModel *headmodel=[[HeadInfoModel alloc]init];
            headmodel.imgPath=filePath;
            headmodel.imgName=fileName;
            headmodel.customID=23;
            [headdal updateItem:headmodel];
            NSLog(@"图片发送成功!");
        }
    }
}
-(void)updateHeadCallBack:(BOOL)result
{
    NSLog(@"123");
}

// 失败返回
-(void)uploadErrorCallBack
{

    NSLog(@"图片发送失败!");
}


@end
