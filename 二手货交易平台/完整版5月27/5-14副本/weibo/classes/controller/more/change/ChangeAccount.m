// 修改账户
#import "changeAccount.h"
#import "CustomInfoModel.h"
#import "Md5Coder.h"
#import <sqlite3.h>
@implementation ChangeAccount
@synthesize customDjLsh;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 1.修改账户信息
    [self changeButtonofSet];
    
    // 2.设置背景颜色
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    // 3.设置右边的取消item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"修改" size:CGSizeMake(50, 30) target:self action:@selector(change)];
    
    // 4.设置代理
    dal = [[CustomInfoDAL alloc] initWithDelegate:self];
    
    // 5.查询当前登录的账户
    [self SearchDjLshFromDataBase];
}

#pragma mark 取消
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark 修改
-(void)change
{

    NSString *uid=UserTextField.text;
    NSString *pwdnew=PwdTextFieldnew.text;
    pwdnew = [[Md5Coder md5Encode:pwdnew] lowercaseString];
    CustomInfoModel *cus=[[CustomInfoModel alloc]init];
    cus.djLsh=customDjLsh;
    cus.userName=uid;
    cus.passWord=pwdnew;
    if([dal updateItem:cus]==false)
    {
        NSLog(@"远程操作失败，请检查你的网络2!");
    }
    else
    {
       
    }
    
}


#pragma mark 回调

-(void)updateCustomCallBack:(BOOL)result
{
    NSLog(@"修改成功");
}


#pragma mark 查询一下当前登录的账号的DjLsh
-(void)SearchDjLshFromDataBase
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    customDjLsh = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}



#pragma mark 显示控件
-(void)changeButtonofSet
{
    /**********请输入账号***********/
    // 设置文字的位置
    UserField=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    // 设置文本内容
    UserField.text=@"用户名";
    // 设置标题文字大小
    UserField.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:UserField];
    
    
    /**********请输入密码***********/
    // 设置文字的位置
    UserField=[[UILabel alloc] initWithFrame:CGRectMake(30, 160, 100, 30)];
    // 设置文本内容
    UserField.text=@"密  码";
    // 设置标题文字大小
    UserField.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:UserField];
    
    /**********请再次输入密码***********/
    // 设置文字的位置
    PwdFieldAgan=[[UILabel alloc] initWithFrame:CGRectMake(0, 220, 130, 30)];
    // 设置文本内容
    PwdFieldAgan.text=@"再次输入密码";
    // 设置标题文字大小
    PwdFieldAgan.font=[UIFont systemFontOfSize:18];
    [self.view addSubview:PwdFieldAgan];
    
    
    
    /**********输入账号***********/
    // 初始化坐标位置
    UserTextField=[[UITextField alloc] initWithFrame:CGRectMake(110, 100, 190, 30)];
    // 为空白文本字段绘制一个灰色字符串作为占位符
    UserTextField.placeholder = @"请输入您的用户名";
    // 设置点一下清除内容
    UserTextField.clearButtonMode=UserTextField;
    // 默认就是左对齐，这个是UITextField扩展属性
    UserTextField.textAlignment = UITextAlignmentLeft;
    // 设置textField的形状
    UserTextField.borderStyle=UITextBorderStyleRoundedRect;
    // 设置为YES当用点触文本字段时，字段内容会被清除,这个属性一般用于密码设置，当输入有误时情况textField中的内容
    UserTextField.clearsOnBeginEditing = YES;
    // 设置键盘完成按钮
    UserTextField.returnKeyType=UIReturnKeyDone;
    // 委托类需要遵守UITextFieldDelegate协议
    UserTextField.delegate=self;
    //设置TextFiel输入框字体大小
    UserTextField.font = [UIFont systemFontOfSize:18];
    [self.view addSubview:UserTextField];
    
    
    
    
    /**********输入密码***********/
    
    //初始化坐标位置
    PwdTextFieldold=[[UITextField alloc] initWithFrame:CGRectMake(110, 160, 190, 30)];
    // 设置文本文档的输入为密码
    PwdTextFieldold.secureTextEntry=YES;
    // 设置点一下清除内容
    PwdTextFieldold.clearButtonMode=PwdTextFieldold;
    //为空白文本字段绘制一个灰色字符串作为占位符
    PwdTextFieldold.placeholder = @"请输入您的密码";
    //设置textField的形状
    PwdTextFieldold.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    PwdTextFieldold.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    PwdTextFieldold.delegate=self;
    //设置TextFiel输入框字体大小
    PwdTextFieldold.font = [UIFont systemFontOfSize:18];
    //    把TextField添加到视图上
    [self.view addSubview:PwdTextFieldold];
    
    /**********再次输入密码***********/
    
    //初始化坐标位置
    PwdTextFieldnew=[[UITextField alloc] initWithFrame:CGRectMake(110, 220, 190, 30)];
    // 设置文本文档的输入为密码
    PwdTextFieldnew.secureTextEntry=YES;
    // 设置点一下清除内容
    PwdTextFieldnew.clearButtonMode=PwdTextFieldnew;
    //为空白文本字段绘制一个灰色字符串作为占位符
    PwdTextFieldnew.placeholder = @"再次输入密码";
    //设置textField的形状
    PwdTextFieldnew.borderStyle=UITextBorderStyleRoundedRect;
    //设置键盘完成按钮
    PwdTextFieldnew.returnKeyType=UIReturnKeyDone;
    //委托类需要遵守UITextFieldDelegate协议
    PwdTextFieldnew.delegate=self;
    //设置TextFiel输入框字体大小
    PwdTextFieldnew.font = [UIFont systemFontOfSize:18];
    //    把TextField添加到视图上
    [self.view addSubview:PwdTextFieldnew];
}

@end
