//
//  NSString+file.h
//  weibo
//
//  Created by 裴烨烽 on 14-4-29.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (file)
- (NSString *)filenameAppend:(NSString *)append;
@end
