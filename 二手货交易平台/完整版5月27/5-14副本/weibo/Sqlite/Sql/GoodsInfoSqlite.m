// SQL执行语句
#import "GoodsInfoSqlite.h"
#import <sqlite3.h>
// 本地数据库的表名
#define TableName @"GoodsInfo"
@implementation GoodsInfoSqlite

 //新增
+(BOOL)addGoodsItem:(GoodsModel *)item
{
    if(item==nil)
        return false;
    BOOL result=false;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {

            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"insert into %@",TableName];

            [sql appendFormat:@"(djlsh,customid,goodsname,oldlevel,buyprice,saleprice,salesign,imagetitle,imgpath,imgname,goodsdesc,createtime)values('%i','%i','%@','%@','%.2f','%.2f','%i','%@','%@','%@','%@','%@')",item.djLsh,item.customID,item.goodsName,item.oldLevell,item.buyPrice,item.salePrice,item.saleSign,item.imgTitle,item.imgPath,item.imgName,item.goodsDesc,item.createTime];
            
            
            
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                return true;
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    return  result;

}



@end
