﻿#import <Foundation/Foundation.h>
#import "WcfRequest.h"
#import "CustomInfoDelegate.h"

@class CustomInfoModel;

// 客户信息 跟WCF服务器交互
@interface CustomInfoDAL : NSObject
<WcfRequestDelegate, NSXMLParserDelegate>
{
    WcfRequest *request;

    // xml 解析相关
    NSString *currentElement;     // 当前节点名称
    NSMutableString *currentData; // 当前节点的数据
    NSMutableDictionary *xmlData; // 解析后的字典数据
}

@property (nonatomic,assign) id<CustomInfoDelegate> delegate;

-(CustomInfoDAL *)initWithDelegate:(id)de;

// 根据djlsh取单条
-(BOOL)login:(NSString *)passWord and:(NSString *)passWord;

// 新增
-(BOOL)addItem:(CustomInfoModel *)item;

// 更新
-(BOOL)updateItem:(CustomInfoModel *)item;

// 删除
-(BOOL)deleteItem:(int)djLsh;

// 取单条
-(BOOL)getFullItem:(int)djLsh;

@end

