﻿#import <Foundation/Foundation.h>
#import "WcfRequest.h"
#import "HeadInfoDelegate.h"

@class HeadInfoModel;

// headinfo 跟WCF服务器交互
@interface HeadInfoDAL : NSObject
<WcfRequestDelegate, NSXMLParserDelegate>
{
    WcfRequest *request;

    // xml 解析相关
    NSString *currentElement;     // 当前节点名称
    NSMutableString *currentData; // 当前节点的数据
    NSMutableDictionary *xmlData; // 解析后的字典数据
}

@property (nonatomic,assign) id<HeadInfoDelegate> delegate;

-(HeadInfoDAL *)initWithDelegate:(id)de;

// 新增
-(BOOL)addItem:(HeadInfoModel *)item;

// 更新
-(BOOL)updateItem:(HeadInfoModel *)item;

// 删除
-(BOOL)deleteItem:(int)djLsh;

// 取单条
-(BOOL)getItem:(int)djLsh;

// 取列表
-(BOOL)getList;

// 分页取列表
-(BOOL)getListWithPageOutCount:(int)nPageSize :(int)nPageIndex;

@end

