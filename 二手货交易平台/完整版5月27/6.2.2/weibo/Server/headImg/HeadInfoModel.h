﻿#import <Foundation/Foundation.h>

// headinfo
@interface HeadInfoModel : NSObject
<NSCoding,NSCopying>

@property (nonatomic,assign) int djLsh; // 单据流水号
@property (nonatomic,copy) NSString *title; // 标题
@property (nonatomic,copy) NSString *imgPath; // imgpath
@property (nonatomic,copy) NSString *imgName; // imgname
@property (nonatomic,assign) int customID; // 客户编号

+(HeadInfoModel *)itemWithDict:(NSDictionary *)dict;

-(void)exchangeNil;               // 替换所有的nil为空字符串
-(NSMutableString *)getJsonValue; // 转换成Json字符串
-(NSMutableString *)getXmlValue;  // 转换成Xml字符串

@end

