﻿#import <Foundation/Foundation.h>
#import "HeadInfoModel.h"

// headinfo 委托协议
@protocol HeadInfoDelegate<NSObject>

@optional
-(void)addHeadInfoCallBack:(BOOL)result;
-(void)updateHeadCallBack:(BOOL)result;
-(void)deleteHeadInfoCallBack:(BOOL)result;
-(void)getHeadInfoItemCallBack:(HeadInfoModel *)item;
-(void)getHeadInfoListCallBack:(NSMutableArray *)list;
-(void)getHeadInfoPageListCallBack:(NSMutableArray *)list
                             andRecordCount:(int)recordCount
                               andPageCount:(int)pageCount
                                andPageSize:(int)pageSize
                               andPageIndex:(int)pageIndex;

@end

