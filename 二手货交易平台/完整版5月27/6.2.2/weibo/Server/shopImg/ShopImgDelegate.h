﻿#import <Foundation/Foundation.h>
#import "ShopImgModel.h"

// shopimg 委托协议
@protocol ShopImgDelegate<NSObject>

@optional
-(void)addShopImgCallBack:(BOOL)result;
-(void)updateShopImgCallBack:(BOOL)result;
-(void)deleteShopImgCallBack:(BOOL)result;
-(void)getShopImgItemCallBack:(ShopImgModel *)item;
-(void)getShopImgListCallBack:(NSMutableArray *)list;
-(void)getShopImgPageListCallBack:(NSMutableArray *)list
                             andRecordCount:(int)recordCount
                               andPageCount:(int)pageCount
                                andPageSize:(int)pageSize
                               andPageIndex:(int)pageIndex;

@end

