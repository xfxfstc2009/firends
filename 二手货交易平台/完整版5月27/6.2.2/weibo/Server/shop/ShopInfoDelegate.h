﻿#import <Foundation/Foundation.h>
#import "ShopInfoModel.h"

// shopinfo 委托协议
@protocol ShopInfoDelegate<NSObject>

@optional
-(void)addShopInfoCallBack:(BOOL)result;
-(void)updateShopInfoCallBack:(BOOL)result;
-(void)deleteShopInfoCallBack:(BOOL)result;
-(void)getShopInfoItemCallBack:(ShopInfoModel *)item;
-(void)getShopInfoListCallBack:(NSMutableArray *)list;
-(void)getShopInfoPageListCallBack:(NSMutableArray *)list
                             andRecordCount:(int)recordCount
                               andPageCount:(int)pageCount
                                andPageSize:(int)pageSize
                               andPageIndex:(int)pageIndex;

@end

