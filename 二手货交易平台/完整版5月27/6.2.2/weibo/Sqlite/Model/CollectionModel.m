
#import "CollectionModel.h"
@implementation CollectionModel

@synthesize djLsh;          // 单据流水号
@synthesize customID;       // 客户编号
@synthesize goodsName;      // 商品名称
@synthesize oldLevell;      // 新旧程度
@synthesize buyPrice;       // 采购价格
@synthesize salePrice;      // 销售报价
@synthesize saleSign;       // 出售标记
@synthesize imgTitle;       // 图片标题
@synthesize imgPath;        // 图片路径
@synthesize imgName;        // 图片名字
@synthesize goodsDesc;      // 物品简介
@synthesize createTime;     // 创建时间
@synthesize mobilephone;    // 电话号码
@end
