// 用户模型
#import <Foundation/Foundation.h>

@interface UserModel : NSObject
@property (nonatomic,assign) int DjLsh;         // 单据流水号
@property (nonatomic,copy)NSString *userName;   // 标题
@property (nonatomic,copy)NSString *pwdWord;    // 密码
@property (nonatomic,copy)NSString *MobilePhone;// 电话
@property (nonatomic,copy)NSString *Email;      // Emial
@property (nonatomic,copy)NSString *cityName;   // 城市名称
@property (nonatomic,copy)NSString *Address;    // 地址
@property (nonatomic,assign)int Shop;           // 是否开店
@property (nonatomic,assign)int ShopNo;         // 店的编号
@property (nonatomic,assign)BOOL isLog;         // 是否登录
@property (nonatomic,copy)NSDate* CreateTime;    // 创建时间
@property (nonatomic,copy)NSString *imgPath;    // 图片
@property (nonatomic,copy)NSString *imgName;    // 图片名字


@end
