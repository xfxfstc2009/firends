//
//  MainViewController.h
//  weibo
//
//  Created by apple on 13-8-28.
//  Copyright (c) 2013年 itcast. All rights reserved.
//  主框架

#import <UIKit/UIKit.h>

@interface MainViewController : UINavigationController


// 设置  弹出Menu
- (void)showMenu;
// 判断是否登录
@property(nonatomic,strong)NSString *islogname;
@end
