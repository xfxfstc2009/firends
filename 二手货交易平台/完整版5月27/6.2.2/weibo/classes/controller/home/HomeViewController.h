
#import <UIKit/UIKit.h>
#import "GoodsInfoDAL.h"
#import "ShopInfoDAL.h"
// 这个是跳转的页面
@class HomeDetailed;


@interface HomeViewController : UITableViewController
<GoodsInfoDelegate,ShopInfoDelegate>
{
    GoodsInfoDAL *dal;
    ShopInfoDAL *dalforshop;
}


//跳转的页面
@property (nonatomic,retain) HomeDetailed * homedetailed;
// 获得到取得列表的总数
@property(nonatomic,assign)int getListCount;
// 取列表的内容
@property (nonatomic,strong) NSMutableArray * goodsList;


// 创建一个shopList用来存放取得的数据
@property (nonatomic,strong)NSMutableArray *shopList;

// 查询一下获得数据，存放是否查到shopcount表中有数据
@property(nonatomic,assign)int ishave;

// 判断是否有登录
@property(nonatomic,strong)NSString *username;
@end
