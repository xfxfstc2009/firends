#import "HomeViewController.h"
#import "HomeDetailed.h"
#import "MainViewController.h"
#import "AddGoodsView.h"
#import "GoodsInfoModel.h"
#import "EditGoodsViewController.h"

// 模型Cell
#import "ModelCell.h"

#import "GoodsModel.h"
#import "GoodsInfoSqlite.h"

// 本地数据库
#import <sqlite3.h>
#import "ShopInfoSqlite.h"

@interface HomeViewController ()

@property (nonatomic, strong) NSTimer *workTimer;
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation HomeViewController
@synthesize workTimer = _workTimer;
@synthesize items = _items;

// 一共有多少个数组
@synthesize getListCount;
// 取列表
@synthesize goodsList;

// 创建一个ShopList用来存放本地数据库中的shop表
@synthesize shopList;
// 存放shopcount表是否查到数据
@synthesize ishave;
// 判断是否登录
@synthesize username;


- (void)viewDidLoad
{
    [super viewDidLoad];
    //1.首先先去数据库中插入一条数据shopcount=0
    [self insertIntoShopCount];
    
    // 1.标题
    self.title=@"首页";
    
    // 2.左边的按钮
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"navigationbar_compose.png" target:self action:@selector(sendStatus)];

    // 4.文件代理方法
    dal = [[GoodsInfoDAL alloc] initWithDelegate:self];
    dalforshop=[[ShopInfoDAL alloc]initWithDelegate:self];
    
    //判断是否登录
    [self SearchIsLogFromDataBase0];
    
    
    // 取出列表，得到列表的count
    [self getListCount];
    [self getShopList];
    
    
}


#pragma mark 添加物品
- (void)sendStatus
{
    // 判断，如果当前没有登录就不能弹出
    if(username.length==0)// 表示这个没有登录
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"系统消息"
                                                        message:@"您还没有登录，请登录后发布商品!"
                                                       delegate:nil
                                              cancelButtonTitle:@"关闭"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
    AddGoodsView *addgoods = [[AddGoodsView alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:addgoods];
    [self presentViewController:nav animated:YES completion:nil];
    }
}
#pragma mark 弹出菜单
-(void)popMenu
{
    EditGoodsViewController *addgoods = [[EditGoodsViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:addgoods];
    [self presentViewController:nav animated:YES completion:nil];
}





#pragma mark - Table view data source

#pragma mark Cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return goodsList.count;
}

#pragma mark cell 复用
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *ID = @"Cell";
    ModelCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    // 2.如果缓存池中没有，才需要传入一个标识创建新的Cell
    if (cell == nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ModelCell" owner:self options:nil]lastObject];
    }
    GoodsInfoModel* goodsmodel = self.goodsList[indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%i",goodsmodel.customId];
    // 设置CELL背景透明
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor] ;
    GoodsInfoModel *model=self.goodsList[indexPath.row];
    cell.goodsModel=model;
    [cell setValues:model];
    
    
    [ cell.goodsimage1 setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/head/%@/%@",ImageUrl,cell.goodsModel.imgPath,cell.goodsModel.imgName]] placeholderImage:[UIImage imageNamed:@"hud_wifi.png"] options:SDWebImageLowPriority | SDWebImageRefreshCached | SDWebImageRetryFailed ];
    
    return cell;
}

// 返回每一行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77;
}

#pragma mark tableview delegate 选中某一行 进行跳转
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.homedetailed == nil) {
        self.homedetailed = [[HomeDetailed alloc] init];
    }
    [self.homedetailed setValues: self.goodsList[indexPath.row]];
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:self.homedetailed];
    
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    self.homedetailed.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:nav animated:YES completion:^{
        
        [self.homedetailed setValues:self.goodsList[indexPath.row]];
    }];
    
}


#pragma mark 取出列表，然后得到有多少条数据
-(void)getListCount
{
    // 取列表
    [dal getList];
}

#pragma mark callback 方法
-(void)getGoodsInfoListCallBack:(NSMutableArray *)list
{
    self.goodsList=list;
    [self.tableView reloadData];
    [self loadIntoSqlite];
    
}


// 将取得的列表放到本地数据库中
-(void)loadIntoSqlite
{
    for(int i=0;i<goodsList.count;i++)
    {
        GoodsInfoModel *goodsmodel=goodsList[i];
        // 将goodsmodel存入数据库
        GoodsModel *sqlgoodsmodel=[[GoodsModel alloc]init];
        sqlgoodsmodel.djLsh=goodsmodel.djLsh;
        sqlgoodsmodel.customID=goodsmodel.customId;
        sqlgoodsmodel.goodsName=goodsmodel.goodsName;
        sqlgoodsmodel.oldLevell=goodsmodel.oldlevel;
        sqlgoodsmodel.buyPrice=goodsmodel.buyPrice;
        sqlgoodsmodel.salePrice=goodsmodel.salePrice;
        sqlgoodsmodel.imgPath=goodsmodel.imgPath;
        sqlgoodsmodel.imgName=goodsmodel.imgName;
        sqlgoodsmodel.goodsDesc=goodsmodel.goodsDesc;
        sqlgoodsmodel.createTime=goodsmodel.createTime;
        sqlgoodsmodel.mobilephone=@"123123";
        [GoodsInfoSqlite addGoodsItem:sqlgoodsmodel];
    }
    
}

#pragma mark 取得列表
-(void)getShopList
{
    [dalforshop getList];
}
#pragma mark 返回
-(void)getShopInfoListCallBack:(NSMutableArray *)list
{
    shopList = [[NSMutableArray alloc]init];
    shopList=list;
    // 将取得的数据.count直接存入到shopcount表中
    [self UpdateShopCount];
}









#pragma mark 将取得的数据转换称为.count存入shopcount 表
// 1.首先查一下改表中是否有数据
// if(有)
//{
//修改
//}
//else
//{
//增加
//}




// 增加商店
-(void)insertIntoShopCount
{
    
    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"insert into shopcount"];
            [sql appendFormat:@"(djlsh,shopcount) values (1,0)"];
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    
}

// 修改
-(void)UpdateShopCount
{
    
    BOOL result=false;
    //NSDocumentDirectory 找到Document 文件夹，是个宏定义
    // 寻找 当前程序所在 沙盒下面的Document文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"update shopcount set "];
            [sql appendFormat:@"shopcount =%i where djlsh=1",shopList.count];
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK)
            {
                result=false;
                NSLog(@"error to exec %@",sql);
            }
            else
            {
                
            }
            // 关闭数据库
            sqlite3_close(database);
        }
    }
    
}


/////////////////用来判断有否登录不登录就不能添加新商品/////////////////////

//1.查询一下本地数据库的登录账户,得到账户和密码
-(void)SearchIsLogFromDataBase0
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select username,password from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    username = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
          
                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}




@end
