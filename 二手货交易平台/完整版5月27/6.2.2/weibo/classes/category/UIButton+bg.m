// 设置按钮的所有状态的图片

#import "UIButton+bg.h"

@implementation UIButton (bg)


-(CGSize)setAllstateBg:(NSString *)icon
{
 
        UIImage *normal=[UIImage stretchImageWithName:icon];
        UIImage *highlighted=[UIImage stretchImageWithName:[icon filenameAppend:@"_highlighted"]];
        
        [self setBackgroundImage:normal forState:UIControlStateNormal];
        [self setBackgroundImage:highlighted forState:UIControlStateHighlighted];
        return normal.size;
    
}
@end
