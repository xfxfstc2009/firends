#import "JsonDataExchange.h"

@implementation JsonDataExchange

+(NSDate *)JsonTimeToNSDate:(NSString *)json
{
    if(json == nil
       || [json isEqualToString:@"nil"]
       || [json isEqualToString:@"null"]
       || json.length <= 0)
        return nil;
    else
    {
        char c1 = [json characterAtIndex:0];
        if(c1 == '/')
            json = [json substringFromIndex:1];
        char c2 = [json characterAtIndex:json.length-1];
        if(c2 == '/')
            json = [json substringToIndex:json.length-1];
        
        // NSString *json = @"Date(1388725950000+0800)";
        // 特殊情况: 1970-1-1 == Date(0+0800)
        json = [json substringFromIndex:5]; // Date(1388725950000+0800) ==> 1388725950000+0800)
        json = [json substringToIndex:json.length-6]; // 1388725950000+0800) ==> 1388725950000
        if(json.length > 3)
            json = [json substringToIndex:json.length-3]; // 1388725950000 ==> 1388725950
        
        // 时间间隔(带时区)
        NSTimeInterval timeInterval = [json doubleValue];
        // timeInterval += 28800; 不能加,加了错误
        
        // 时间
        [NSDate dateWithTimeIntervalSince1970:timeInterval];
        // 转成NSDate
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        return date;
    }
}
+(NSDate *)NSDateMin
{
    return [self JsonTimeToNSDate:@"Date(0000+0800)"];
}
+(NSDate *)NSDateNow
{
    // 当前时间(是带时区的)
    NSDate *now = [NSDate date]; // 获取的是GMT时间
    return now;
}

+(NSString *)JsonTimeFromNSDate:(NSDate *)date
{
//    // 时区转换
//    NSTimeZone *zone = [NSTimeZone systemTimeZone];
//    NSInteger interval = [zone secondsFromGMTForDate:date];
//    // 追加时区
//    NSDate *localTime = [date dateByAddingTimeInterval:interval];
    
    // 时间间隔,0时区
    double timeInterval = [date timeIntervalSince1970];
    // 转换成json时间
    NSNumber *number = [[NSNumber alloc] initWithDouble:timeInterval];
    // 最后时间
    NSString *jsonTime = [NSString stringWithFormat:@"Date(%0.lf000+0800)",[number doubleValue]];
    
    return jsonTime;
}

+(NSString *)JsonTimeNow
{
    // 当前时间(是带时区的)
    NSDate *now = [NSDate date]; // 获取的是GMT时间
    
    // 这个答应是错误的,不会计算时区
    // NSLog(@"%@",now);
    
    // 这个打印的带时区的
//    NSDateFormatter *dateFromatter = [[NSDateFormatter alloc] init];
//    [dateFromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSLog(@"JsonTimeNow = %@",[dateFromatter stringFromDate:now]);
//    [dateFromatter release];
    
    return [self JsonTimeFromNSDate:now];
}
+(NSString *)JsonTimeMin
{
//    NSDateFormatter *dateFromatter = [[NSDateFormatter alloc] init];
//    [dateFromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *now = [dateFromatter dateFromString:@"1970-1-1 00:00:00"];
//    [dateFromatter release];
//    NSTimeZone *zone = [NSTimeZone systemTimeZone];
//    NSInteger interval = [zone secondsFromGMTForDate:now];
//    NSDate *localTime = [now dateByAddingTimeInterval:interval];
//    return [self JsonTimeFromNSDate:localTime];
    
    return @"Date(0000+0800)";
}

+(NSString *)NSStringNowWithFormat:(NSString *)formatStr
{
    NSDate *now = [NSDate date]; // 获取的是GMT时间
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formatStr];
    NSString *string = [formatter stringFromDate:now];
    return string;
}
+(NSString *)NSStringFromNSDate:(NSDate *)date format:(NSString *)formatStr
{
    if(date == nil)
        return nil;
    else
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:formatStr];
        NSString *string = [formatter stringFromDate:date];
              return string;
    }
}
+(NSDate *)NSDateFromNSString:(NSString *)string format:(NSString *)formatStr
{
    if(string == nil)
        return nil;
    else
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:formatStr];
        NSDate *date = [formatter dateFromString:string];
            return date;
    }
}

@end
