//
//  UIImage+image.h
//  weibo
//
//  Created by 裴烨烽 on 14-4-29.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (image)

// 别人传一个文件名给我，然后返回一张图片
+(UIImage *)fullscreenImageWithName:(NSString *)name;

// 传一个文件名给我，然后返回一张拉伸好的图片
+(UIImage *)stretchImageWithName:(NSString *)name;
@end
