
#import <UIKit/UIKit.h>
#import "CustomInfoDAL.h"
@class  Account;
@class Suggest;
@class AboutUs;

@interface MoreViewController : UITableViewController<UIAlertViewDelegate,UIActionSheetDelegate,CustomInfoDelegate>
{
    
    CustomInfoDAL *cusdal; // 用户登录的代理方法
}
@property (nonatomic,strong) NSArray *data;
@property (nonatomic,strong) UIImage *bgImage;
@property (nonatomic,strong) UIButton *btnB;
@property (nonatomic,strong) AboutUs *aboutUs;
@property (nonatomic,strong) UIViewController *selectedController;
@property (nonatomic,strong) Suggest *suggest;
@property (nonatomic,strong)UIImageView *img;

// 读取服务器的imgPath
@property(nonatomic,strong)NSString *username;
@property(nonatomic,strong)NSString *password;
@property (nonatomic,strong)NSString *imgpath;
@property (nonatomic,strong)NSString *imgname;

@end