
#import "MoreViewController.h"
#import "Suggest.h"
#import "Login.h"
#import "MyGoods.h"
#import "MyCollection.h"
#import "ChangeAccount.h"
#import "UserInfoSqlite.h"
#import "AccountView.h"
#import <sqlite3.h>
#import "RegisterViewController.h"



#import "homViewController.h"
@interface MoreViewController ()

@end

@implementation MoreViewController

@synthesize data,bgImage,btnB,selectedController,img;
@synthesize imgname;
@synthesize imgpath;
@synthesize username;
@synthesize password;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
        // 加载PersonalCenter的数据到data
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"PersonalCenter" withExtension:@"plist"];
        self.data = [NSDictionary dictionaryWithContentsOfURL:url][@"CH"];
        
        // 删除垂直滚动条
        self.tableView.showsVerticalScrollIndicator = NO;
        // 设置tableview没有分割线
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        // 扩展scrollerview的顶部空间
        self.tableView.contentInset = UIEdgeInsetsMake(210, 0, 0, 0);
        
        // 设置背景
        // 当tableview的样式为group时，如果想更换背景，必须清除条纹状的backgroundView
        self.tableView.backgroundView = nil;
        self.tableView.backgroundColor = kGlobalBg;
        
        // 5.缩小每一组之间的间距
        self.tableView.sectionHeaderHeight = 10;
        self.tableView.sectionFooterHeight = 0;
        
        // 6.添加最下面的退出按钮
        [self exitButton];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self viewDidAppear:YES];

    // 设置登录代理
    cusdal = [[CustomInfoDAL alloc] initWithDelegate:self];
    
    // 1.设置顶部的默认图片背景
    bgImage = [UIImage imageNamed:@"eiffel.png"];
    
    UIImageView *bgView = [[UIImageView alloc] initWithImage:bgImage];
    bgView.frame = CGRectMake(0, -210, 320, 200);
    [self.view addSubview:bgView];
    
    // 2.添加按钮更换图片
    [self changeBgButton];
    
    // 3.添加背景切换动画效果
    [self changeWithAnimation];
    
    // 7取得当前用户的头像
    [self getUserHead];
    
    // 4.添加头像
    [self setHeadImage];
    
    // 5.添加个人资料按钮
    [self setSelfBtn];

    
}


#pragma mark 添加背景切换动画效果
-(void)changeWithAnimation
{
    // 动画效果
    CATransition *animation = [CATransition animation];
    animation.delegate = btnB;
    animation.duration = 1;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"rippleEffect";
    animation.subtype = kCATransitionFromRight;
    // 动画执行者是新视图
    [[btnB.imageView layer] addAnimation:animation forKey:@"animation"];
    
}

#pragma mark 添加按钮切换背景图片
-(void)changeBgButton
{
    // 添加更换背景图片btn
    UIButton *btnBg = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBg.titleLabel.font = [UIFont systemFontOfSize:13];
    [btnBg addTarget:self action:@selector(changeBG) forControlEvents:UIControlEventTouchUpInside];
    NSString *btnBgText = @"";
    [btnBg setTitle:btnBgText forState:UIControlStateNormal];
    [btnBg setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnBg setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnBg.center = CGPointMake(160, -125);
    btnBg.bounds = CGRectMake(0, 0, 320, 200);
    
    [self.view addSubview:btnBg];
    
}

#pragma mark -添加头像
-(void)setHeadImage
{
    // 设置个人头像
    UIButton *headbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    // 设置tag进行按钮点击事件
    [headbutton addTarget:self action:@selector(headImage) forControlEvents:UIControlEventTouchUpInside];
    // 设置按钮的图片
    // 准备参数
    NSMutableString *imgUrl = [NSMutableString stringWithString:@""];
    [imgUrl appendString:[NSString stringWithFormat:@"%@/head/",ImageUrl]];
  [imgUrl appendString:[NSString stringWithFormat:@"%@/%@",imgpath,imgname]];

    UIImage *myImage5 =[ [ UIImage alloc]initWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]] ];
    [headbutton setBackgroundImage:myImage5 forState:UIControlStateNormal];
    
    // 设置按钮的尺寸
    headbutton.frame = CGRectMake(20, -65, 95, 95);
    // 显示为圆角
    [headbutton.layer setMasksToBounds:YES];
    // 圆角弧度
    [headbutton.layer setCornerRadius:15.0];
    
    // 设置按钮的颜色
    [headbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [headbutton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
    // 设置上面的文字
    headbutton.titleLabel.font = [UIFont systemFontOfSize:13];
    
    [self.view addSubview:headbutton];
}

#pragma mark  设置头像方法
-(void)headImage
{
    
}

#pragma mark 添加个人资料按钮
-(void)setSelfBtn
{
    // 添加个人资料btn
    UIButton *btnSelf = [UIButton buttonWithType:UIButtonTypeCustom];
    // 设置按钮位置
    btnSelf.frame = CGRectMake(125, 0, 120, 30);
    // 设置按钮的文字大小
    btnSelf.titleLabel.font = [UIFont systemFontOfSize:17];
    // 设置按钮点击事件
    [btnSelf addTarget:self action:@selector(selfData) forControlEvents:UIControlEventTouchUpInside];
    // 设置个人资料btn背景
    UIImage *btnImage1 = [UIImage stretchImageWithName:@"common_card_background_highlighted.png"];
    UIImage *btnImage2 = [UIImage stretchImageWithName:@"common_card_background.png"];
    [btnSelf setBackgroundImage:btnImage1 forState:UIControlStateNormal];
    [btnSelf setBackgroundImage:btnImage2 forState:UIControlStateHighlighted];
    // 设置按钮显示文字
    NSString *selftext = @"个人资料";
    [btnSelf setTitle:selftext forState:UIControlStateNormal];
    [btnSelf setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
    // 加载
    [self.view addSubview:btnSelf];
}
-(void)selfData
{
    AccountView *account = [[AccountView alloc] initWithNibName:nil bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:account];
    [self presentViewController:nav animated:YES completion:nil];
}
#pragma mark - 数据源方法
#pragma mark 返回的section组数
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // 根据数据返回有几组
    return data.count - 1;
}

#pragma mark 设置对应的section返回多少个cell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // 返回每组有几行
    NSArray *sectionArray = data[section];
    return sectionArray.count;
}

#pragma mark 设置cell的数据源方法
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 获得cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // 清除标签的背景
        cell.textLabel.backgroundColor = [UIColor clearColor];
        
        // 设置标签的高亮时的文字颜色为默认的文字颜色
        cell.textLabel.highlightedTextColor = cell.textLabel.textColor;
        
        // 设置cell的背景view
        UIImageView *bg = [[UIImageView alloc] init];
        cell.backgroundView = bg;
        
        UIImageView *selectedBg = [[UIImageView alloc]  init];
        cell.selectedBackgroundView = selectedBg;
    }
    
    /************设置Cell背景*************/
    // 设置显示的标题文字
    cell.textLabel.text = data[indexPath.section][indexPath.row][@"name"];
    
    // 设置背景
    // 取出背景view
    UIImageView *bg = (UIImageView *)cell.backgroundView;
    UIImageView *selectedbg = (UIImageView *)cell.selectedBackgroundView;
    
    // 算出文件名
    NSArray *sectionArray = data[indexPath.section];
    int count =sectionArray.count;
    NSString *name = nil;
    
    if (count == 1)
    {   // 只有1个
        name = @"common_card_background.png";
    }
    else if (indexPath.row == 0)
    {   // 顶部
        name = @"common_card_top_background.png";
    }
    else if (indexPath.row == count - 1)
    {   // 底部
        name = @"common_card_bottom_background.png";
    }
    else
    {   // 中间
        name = @"common_card_middle_background.png";
    }
    
    // 设置背景图
    bg.image = [UIImage stretchImageWithName:name];
    selectedbg.image = [UIImage stretchImageWithName:[name filenameAppend:@"_highlighted"]];
    cell.backgroundColor = kGlobalBg;
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"common_icon_arrow.png"]];
    return cell;
}




#pragma mark 切换背景的Alert内容
-(void)changeBG
{
    UIAlertView *changeBG = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"默认背景1", @"默认背景2",@"默认背景3",@"更多背景", nil];
    [changeBG show];
}

#pragma mark 背景切换点击事件
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"背景切换:%i",buttonIndex);
    switch (buttonIndex) {
        case 0:
        {NSLog(@"取消");
        }
            break;
        case 1:
        {
            bgImage = [UIImage imageNamed:@"eiffel.png"];
            [self viewDidLoad];
        }
            break;
        case 2:
        {
            bgImage = [UIImage imageNamed:@"ghost.png"];
            [self viewDidLoad];
        }
            break;
        case 3:
        {
            bgImage = [UIImage imageNamed:@"bg2.png"];
            [self viewDidLoad];
        }
            break;
        default:
            break;
    }
}

#pragma mark Cell按钮点击事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%i,%i",indexPath.section,indexPath.row);
    NSInteger i = indexPath.section;
    NSInteger n = indexPath.row;
    if (i == 0 && n == 0) // 账户管理
    {
        AccountView *account = [[AccountView alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:account];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else if(i==1&&n==0)// 我的宝贝
    {
        MyGoods *mygoods=[[MyGoods alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mygoods];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else if (i==1&&n==1)// 购物清单
    {
        MyCollection *mycollection=[[MyCollection alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mycollection];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
    else if(i==2&&n==0)// 给我评分
    {
        NSString *str = [NSString stringWithFormat:
                         @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d",
                         436957167 ];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        
    }
    
    else if (i == 3 && n == 0) // 意见反馈
    {
        homViewController *suggest = [[homViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:suggest];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else if (i == 3 && n == 1) // 关于我们
    {
        RegisterViewController *suggest = [[RegisterViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:suggest];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

#pragma mark 添加最下面的退出按钮
-(void)exitButton
{
    // 添加退出按钮到tableView的最底部
    UIButton *btnExit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnExit.frame = CGRectMake(10, 5, 300, 40);
    btnExit.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnExit addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    NSString *text = [data lastObject][0][@"name"];
    // 设置按钮背景
    [btnExit setAllstateBg:@"common_button_red.png"];
    [btnExit setTitle:text forState:UIControlStateNormal];
    
    UIView *footer = [[UIView alloc] init];
    footer.frame = CGRectMake(0, 0, 0, 70);
    [footer addSubview:btnExit];
    
    
    // footerView的宽度固定是320
    self.tableView.tableFooterView = footer;
}

#pragma mark 添加最下面的退出按钮点击事件
-(void)exit
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"确定退出此账号？" delegate:self cancelButtonTitle:@"关闭" destructiveButtonTitle:@"退出" otherButtonTitles: @"登录",@"注册",@"修改账户",nil];
    
    // UIActionSheet最好显示到Window上面
    [sheet showInView:self.view.window];
    
}

#pragma mark UIActionSheet弹出的点击事件
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        // 清空所有账户的登录状态
        [UserInfoSqlite updateItemisLog0];
    }
    else if(buttonIndex==1)
    {   // 登录
        Login *send = [[Login alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:send];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else if(buttonIndex==2)
    {
        RegisterViewController *send = [[RegisterViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:send];
        [self presentViewController:nav animated:YES completion:nil];
    }
    else if(buttonIndex==3)
    {
        ChangeAccount *changeaccount = [[ChangeAccount alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:changeaccount];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
    
    NSLog(@"actionSheet buttonIndex = %i", buttonIndex);
}




#pragma mark 登录一下取出当前登录的账户

-(void)getUserHead
{
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select username,password,imgpath,imgname from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    username=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    password = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                       imgpath = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                       imgname = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                }
                
                
            }
            else
            {
                
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }

}



#pragma mark 再次登录一下获得当前账户的item
-(void)tryLogingetItem
{
    [cusdal login:username and:password];
}
-(void)loginCallBack:(CustomInfoModel *)item
{
    [self.tableView reloadData];
}





@end