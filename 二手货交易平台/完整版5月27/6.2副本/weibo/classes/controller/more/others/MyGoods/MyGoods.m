// 我发布的宝贝
#import "MyGoods.h"
#import "ModelCell.h"
#import "MyGoodsDetailed.h"
#import <sqlite3.h>
@interface MyGoods ()
{
    MyGoodsDetailed *mygoodsDetailed;
}
@end

@implementation MyGoods
@synthesize titlename;
@synthesize userdjlsh;

// 接收有多少商品的可变数组
@synthesize goodsArray;
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"我的宝贝";
    // 2.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barButtonItemWithIcon:@"btn_back_disabled.png" target:self action:@selector(cancel)];
    //访问本地数据库得到当前的用户单据
    [self SearchIsLogFromDataBase0];
    
    // 设置商品代理
    dalgoods = [[GoodsInfoDAL alloc] initWithDelegate:self];
    // 根据customid去服务器端获得列表
    [dalgoods GetGoodsList:userdjlsh];
}
#pragma mark 左侧返回按钮点击事件
-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
	return goodsArray.count;
}

#pragma mark cell 复用
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *ID = @"Cell";
    ModelCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    // 2.如果缓存池中没有，才需要传入一个标识创建新的Cell
    if (cell == nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ModelCell" owner:self options:nil]lastObject];
    }
    
    GoodsInfoModel* goodsmodel = self.goodsArray[indexPath.row];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%i",goodsmodel.customId];
    // 设置CELL背景透明
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor] ;
    GoodsInfoModel *model=self.goodsArray[indexPath.row];
    cell.goodsModel=model;
    [cell setValues:model];
    return cell;
}

// 返回每一行的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 77;
}

#pragma mark tableview delegate 选中某一行 进行跳转
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 获得到一个组和一个行号
    if (mygoodsDetailed == nil) {
        mygoodsDetailed = [[MyGoodsDetailed alloc] init];
    }
    
    
    UINavigationController  *nav = [[UINavigationController alloc] initWithRootViewController:mygoodsDetailed];
    
    mygoodsDetailed.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:nav animated:YES completion:^{
        
        [mygoodsDetailed setValues:self.goodsArray[indexPath.section]];
    }];
    
    
    
}

// 根据用户的djlsh进行查询发布的商品列表

// 返回一个列表
-(void)getgoodsListWithDjLshCallBack:(NSMutableArray *)list
{
    goodsArray=list;
    [self.tableView reloadData];
}


#pragma mark 查询当前登录账户的djlsh
-(void)SearchIsLogFromDataBase0
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select djlsh,username,password from userinfo where islog = 1"];
            
            // 执行sql语句
            //存储结果，可能错误
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                    userdjlsh = [[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)]intValue];

                }
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
    }
}



@end
