// 我的宝贝
// 我发布的东西
//
#import <UIKit/UIKit.h>
#import "GoodsInfoModel.h"
#import "ShopInfoModel.h"
#import "GoodsInfoDAL.h"

@interface MyGoods : UITableViewController
<GoodsInfoDelegate>
{
    GoodsInfoDAL *dalgoods; // 用户登录的代理方法
}
// 传入的模型1
@property (nonatomic,strong) GoodsInfoModel * goodsmodel;

// 传入的模型2
@property (nonatomic,strong) ShopInfoModel * shopInfoModel;

// 标题的名字
@property(nonatomic,strong)NSString *titlename;


// 创建一个可变数组来接收
@property(nonatomic,strong)NSMutableArray *goodsArray;


// 获得当前登录用户的djlsh
@property(nonatomic,assign)int userdjlsh;
@end