// 我的商店
#import <UIKit/UIKit.h>
#import "ImgUploader.h"
@interface MyShop : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,ImgUploaderDelegate,UITextFieldDelegate>
{
    // 选择的照片
    UIImage *currImage;                // 当前正在传送的图片
    NSMutableArray *imageList;         // 图片数组
    NSMutableArray *imageListNeedSend; // 图片数组,需要上传的
    UIImagePickerController *imagePicker;
    // 图片上传控件
    ImgUploader *imgUploader;
    
    // 最后上传的图片在服务器上存放的文件夹和名称
    NSString *filePath;
    NSString *fileName;
    
    
    
    /////////////////添加按钮/////////////////
    UILabel *shopNameLabel;             // 商店名字
    UILabel *shopDescLabel;             // 商店简介
    
    UITextField *shopNameField;              // 账户信息内容
    UITextView  *shopDescField;              // 商店简介
}
@property (nonatomic, retain) UITextView *shopDescField;

@property(nonatomic,strong)UIScrollView *myscroll;
@property(nonatomic,strong)UIButton *myShopBtn;


@end
