
#import <UIKit/UIKit.h>
#import "GoodsInfoDAL.h"
#import "CustomInfoDAL.h"
#import "ShopInfoDAL.h"
// 这个是跳转的页面
@class HomeDetailed;


@interface DiscoverViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,GoodsInfoDelegate,CustomInfoDelegate,ShopInfoDelegate>{
    
    IBOutlet UITableView *mainTable;
    NSInteger endSection;
    NSInteger didSection;
    BOOL ifOpen;
    
    
    // 声明代理
     GoodsInfoDAL *dal;
    CustomInfoDAL *dalforcus;
    ShopInfoDAL *dalforshop;
}



@property (nonatomic,retain)NSMutableArray *array;
@property(nonatomic,retain)NSMutableArray *arraycopy;
//跳转的页面
@property (nonatomic,retain) HomeDetailed * homedetailed;
// 取列表的内容
@property (nonatomic,strong) NSMutableArray * goodsList;
// 取得商店列表的内容
@property(nonatomic,strong) NSMutableArray *shopList;

// 登录账户
@property(nonatomic,strong) NSString *username;
// 登录密码
@property(nonatomic,strong)NSString *password;
// shop是否打开
@property(nonatomic,assign)int shopOpen;
// shop 名字
@property(nonatomic,assign)int shopNo;

// 商店的总数
@property (nonatomic,assign)int shopCount;

@end
