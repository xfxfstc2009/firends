//
//  homViewController.m
//  weibo
//
//  Created by 裴烨烽 on 14-6-1.
//  Copyright (c) 2014年 _______Smart_______. All rights reserved.
//

#import "homViewController.h"

@interface homViewController ()

@end

@implementation homViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //设置图片url数组
    NSMutableArray *arr=[[NSMutableArray alloc]initWithObjects:@"http://ww1.sinaimg.cn/large/53e0c4edjw1dy3qf6n17xj.jpg",@"http://www.ynwssn.com/file/upload/201106/30/15-54-54-98-45.jpg.middle.jpg",@"http://fs0.139js.com/file/s_jpg_6c26a24fjw1dmaubfgms1j.jpg", nil];
    //设置标题数组
    NSMutableArray *strArr = [[NSMutableArray alloc]initWithObjects:@"11",@"22", nil];
    
    // 初始化自定义ScrollView类对象
    AOScrollerView *aSV = [[AOScrollerView alloc]initWithNameArr:arr titleArr:strArr height:200];
    //设置委托
    aSV.vDelegate=self;
    //添加进view
    [self.view addSubview:aSV];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma AOScrollViewDelegate
-(void)buttonClick:(int)vid{
    NSLog(@"%d",vid);
}
@end
