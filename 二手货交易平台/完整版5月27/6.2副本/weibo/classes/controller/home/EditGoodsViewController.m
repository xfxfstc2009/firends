// 修改对应的商品信息
#import "EditGoodsViewController.h"
#import "GoodsInfoModel.h"
#import <sqlite3.h>
@interface EditGoodsViewController ()

@end

@implementation EditGoodsViewController

// 类型编号
@synthesize typeid1;
// 商品名称
@synthesize goodsname;
// 采购价格
@synthesize buyprice;
// 出售价格
@synthesize salePrice;
// 出售状态
@synthesize salesign;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // 1.设置左边的取消item
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel)];
    
    // 2.设置右边的取消item
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithBg:@"compose_emotion_table_send.png" title:@"登录" size:CGSizeMake(50, 30) target:self action:@selector(chaneGoods)];
   
    // 2.查找本地数据库然后将数据显示到控件上去
    [self SearchFromDataBase:1];
   
    // 1.添加按钮等控件
    [self addButtonSet];
    
    // background
    self.view.backgroundColor = [UIColor whiteColor];
    // 2.去服务器查找对应行的信息进行修改
    [self chaneGoods];
    
    
}

#pragma mark 读取本地数据得到对应物品的信息
-(void)SearchFromDataBase:(int)DjLsh
{
    BOOL result=false;
    sqlite3_stmt *statement;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:SqliteName];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind)
    {// 定义一个数据库操作对象
        sqlite3 *database;
        // 这一句话是使用c的写法，所以[path UTF8String]要转换字符串
        // &database 这个要使用取地址符号
        //整一句话的意思是如果打开这个数据库失败。
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK)
        {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
            NSLog(@"打开数据库文件失败!");
        }
        else
        {
            // 创建SQL语句
            NSMutableString *sql=[NSMutableString string];
            [sql appendFormat:@"select typeId,GoodsName,BuyPrice,salePrice,saleSign from Goodsinfo where djlsh='%i'",DjLsh];
            
            // 执行sql语句
            //存储结果，可能错误
            char *errorMsg;
            // 如果读取不成功
            if(sqlite3_prepare_v2(database, [sql UTF8String], -1, &statement, NULL)==SQLITE_OK)
            {
                if (sqlite3_step(statement) == SQLITE_ROW)
                {
                 
                  
                    // 类型编号
                     typeid1=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
                    // 商品名称
                     goodsname=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                    // 采购价格
                     buyprice=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                    // 出售价格
                     salePrice=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                    // 出售状态
                     salesign=[[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(statement, 4)];
                }
                
                
            }
            else
            {
                result=false;
                NSLog(@"error to exec %@",sql);
                
            }
            sqlite3_finalize(statement);
        }
        // 关闭数据库
        sqlite3_close(database);
        
    }
}





-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark 修改服务器端数据
-(void)chaneGoods
{
//    GoodsInfoModel *goodsmodel=[[GoodsInfoModel alloc]init];
//    goodsmodel.djLsh=1;
//    goodsmodel.typeID=[typeIDField.text intValue];
//    goodsmodel.customID=[customIDField.text intValue];
//    goodsmodel.goodsName= goodsNameField.text;
//    goodsmodel.buyPrice=[buyPriceField.text floatValue];
//    goodsmodel.salePrice=[salePriceField.text floatValue];
//    goodsmodel.saleSign=[saleSignField.text intValue];
//   
//    if([dal updateItem:goodsmodel]==false)
//    {
//        NSLog(@"远程操作失败，请检查你的网络2!");
//    }
//    else
//    {
//        NSLog(@"修改成功");
//    }
    
}



#pragma mark 添加按钮等控件
-(void)addButtonSet
{
    /**********类型编号***********/
    typeIDLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 100, 100, 30)];
    typeIDLabel.text=@"类型编号";
    typeIDLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:typeIDLabel];
    
    /**********客户编号***********/
    customIDLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 140, 100, 30)];
    customIDLabel.text=@"客户编号";
    customIDLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:customIDLabel];
    
    /**********商品名称***********/
    goodsNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 180, 100, 30)];
    goodsNameLabel.text=@"商品名称";
    goodsNameLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:goodsNameLabel];
    
    /**********采购价格***********/
    buyPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 220, 100, 30)];
    buyPriceLabel.text=@"采购价格";
    buyPriceLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:buyPriceLabel];
    
    /**********销售报价***********/
    salePriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 260, 100, 30)];
    salePriceLabel.text=@"销售报价";
    salePriceLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:salePriceLabel];
    
    /**********销售标记***********/
    saleSignLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 300, 100, 30)];
    saleSignLabel.text=@"销售标记";
    saleSignLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:saleSignLabel];
    
    /**********创建时间***********/
    createTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(30, 340, 100, 30)];
    createTimeLabel.text=@"创建时间";
    createTimeLabel.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:createTimeLabel];
    
    
    
    
    
    
    
    /**********类型编号***********/
    typeIDField=[[UITextField alloc] initWithFrame:CGRectMake(110, 100, 150, 30)];
    typeIDField.placeholder = @"请输入类型";
    typeIDField.text=typeid1;
    typeIDField.borderStyle=UITextBorderStyleRoundedRect;
    typeIDField.returnKeyType=UIReturnKeyDone;
    typeIDField.delegate=self;
    typeIDField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:typeIDField];
    
    /**********客户编号***********/
    customIDField=[[UITextField alloc] initWithFrame:CGRectMake(110, 140, 150, 30)];
    customIDField.placeholder = @"请输入客户编号";
    customIDField.borderStyle=UITextBorderStyleRoundedRect;
    customIDField.returnKeyType=UIReturnKeyDone;
    customIDField.delegate=self;
    customIDField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:customIDField];
    
    /**********商品名称***********/
    goodsNameField=[[UITextField alloc] initWithFrame:CGRectMake(110, 180, 150, 30)];
    goodsNameField.placeholder = @"请输入商品名称";
    goodsNameField.text=goodsname;
    goodsNameField.borderStyle=UITextBorderStyleRoundedRect;
    goodsNameField.returnKeyType=UIReturnKeyDone;
    goodsNameField.delegate=self;
    goodsNameField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:goodsNameField];
    
    /**********采购价格***********/
    buyPriceField=[[UITextField alloc] initWithFrame:CGRectMake(110, 220, 150, 30)];
    buyPriceField.placeholder = @"请输入您的采购价格";
    buyPriceField.text=buyprice;
    buyPriceField.borderStyle=UITextBorderStyleRoundedRect;
    buyPriceField.returnKeyType=UIReturnKeyDone;
    buyPriceField.delegate=self;
    buyPriceField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:buyPriceField];
    
    /**********销售报价***********/
    salePriceField=[[UITextField alloc] initWithFrame:CGRectMake(110, 260, 150, 30)];
    salePriceField.placeholder = @"请输入您的销售报价";
    salePriceField.text=salePrice;
    salePriceField.borderStyle=UITextBorderStyleRoundedRect;
    salePriceField.returnKeyType=UIReturnKeyDone;
    salePriceField.delegate=self;
    salePriceField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:salePriceField];
    
    /**********销售标记***********/
    saleSignField=[[UITextField alloc] initWithFrame:CGRectMake(110, 300, 150, 30)];
    saleSignField.placeholder = @"请输入您的销售标记";
    saleSignField.text=salesign;
    saleSignField.borderStyle=UITextBorderStyleRoundedRect;
    saleSignField.returnKeyType=UIReturnKeyDone;
    saleSignField.delegate=self;
    saleSignField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:saleSignField];
    
    /**********创建时间***********/
    createTimeField=[[UITextField alloc] initWithFrame:CGRectMake(110, 340, 150, 30)];
    createTimeField.placeholder = @"请输入您的创建时间";
    createTimeField.borderStyle=UITextBorderStyleRoundedRect;
    createTimeField.returnKeyType=UIReturnKeyDone;
    createTimeField.delegate=self;
    createTimeField.font = [UIFont systemFontOfSize:12];
    [self.view addSubview:createTimeField];
    
}

@end
