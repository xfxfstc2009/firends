/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

@class SWebImageManager;

@protocol SWebImageManagerDelegate <NSObject>

@optional

- (void)webImageManager:(SWebImageManager *)imageManager didFinishWithImage:(UIImage *)image;
- (void)webImageManager:(SWebImageManager *)imageManager didFailWithError:(NSError *)error;

@end
