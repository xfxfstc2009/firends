/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SWebImageCompat.h"

@class SWebImageDownloader;

@protocol SWebImageDownloaderDelegate <NSObject>

@optional

- (void)imageDownloaderDidFinish:(SWebImageDownloader *)downloader;
- (void)imageDownloader:(SWebImageDownloader *)downloader didFinishWithImage:(UIImage *)image;
- (void)imageDownloader:(SWebImageDownloader *)downloader didFailWithError:(NSError *)error;

@end
