//
//  NewFeatureViewController.h
//  weibo
//
//  Created by apple on 13-8-28.
//  Copyright (c) 2013年 itcast. All rights reserved.
//  版本新特性

#import <UIKit/UIKit.h>

@interface NewFeatureViewController : UIViewController <UIScrollViewDelegate>
@property (nonatomic, copy) void (^startBlock)(BOOL shared);

// 判断是否需要首次登录
@property (nonatomic,assign) int istologin;


@end
