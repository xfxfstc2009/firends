﻿#import "ShopInfoModel.h"
#import "JsonDataExchange.h"

@implementation ShopInfoModel

@synthesize djLsh;
@synthesize shopName;
@synthesize shopDesc;
@synthesize customID;


+(ShopInfoModel *)itemWithDict:(NSDictionary *)dict
{
    ShopInfoModel *item = [[ShopInfoModel alloc] init] ;
    item.djLsh = [[dict valueForKey:@"DjLsh"] intValue];
    item.shopName = [dict valueForKey:@"ShopName"];
    item.shopDesc = [dict valueForKey:@"ShopDesc"];
    item.customID = [[dict valueForKey:@"CustomID"] intValue];
    return item;
}

-(void)exchangeNil
{
    if(shopName == nil) shopName = @"";
    if(shopDesc == nil) shopDesc = @"";
}
-(NSMutableString *)getJsonValue
{
    [self exchangeNil];

    NSMutableString * jsonItem = [NSMutableString string];
    [jsonItem appendFormat:@"{"];
    [jsonItem appendFormat:@"\"DjLsh\":%i,", djLsh];
    [jsonItem appendFormat:@"\"ShopName\":\"%@\",", shopName];
    [jsonItem appendFormat:@"\"ShopDesc\":\"%@\",", shopDesc];
    [jsonItem appendFormat:@"\"CustomID\":%i", customID];
    [jsonItem appendFormat:@"}"];
    return jsonItem;
}
-(NSMutableString *)getXmlValue
{
    [self exchangeNil];

    NSMutableString *xmlItem = [NSMutableString string];
    [xmlItem appendFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"];
    [xmlItem appendFormat:@"<item>"];
    [xmlItem appendFormat:@"<DjLsh>%i</DjLsh>",djLsh];
    [xmlItem appendFormat:@"<ShopName>%@</ShopName>",shopName];
    [xmlItem appendFormat:@"<ShopDesc>%@</ShopDesc>",shopDesc];
    [xmlItem appendFormat:@"<CustomID>%i</CustomID>",customID];
    [xmlItem appendFormat:@"</item>"];
    return xmlItem;
}

#pragma mark - NSCoding
-(void) encodeWithCoder: (NSCoder *) encoder
{
    [encoder encodeInt:djLsh forKey: @"djLsh"];
    [encoder encodeObject:shopName forKey: @"shopName"];
    [encoder encodeObject:shopDesc forKey: @"shopDesc"];
    [encoder encodeInt:customID forKey: @"customID"];
}
-(id) initWithCoder: (NSCoder *) decoder
{
    djLsh = [decoder decodeIntForKey:@"djLsh"];
    shopName = [decoder decodeObjectForKey:@"shopName"] ;
    shopDesc = [decoder decodeObjectForKey:@"shopDesc"];
    customID = [decoder decodeIntForKey:@"customID"];

    return self;
}

#pragma mark - NSCopying
// 复制
-(id)copyWithZone:(NSZone *)zone
{
    ShopInfoModel *newItem = [[ShopInfoModel allocWithZone: zone] init];

    newItem.djLsh = self.djLsh;
    newItem.shopName = self.shopName;
    newItem.shopDesc = self.shopDesc;
    newItem.customID = self.customID;

	return newItem;
}

@end

