﻿#import "GoodsInfoModel.h"
#import "JsonDataExchange.h"

@implementation GoodsInfoModel

@synthesize customDjlsh;
@synthesize djLsh;
@synthesize customId;
@synthesize goodsName;
@synthesize oldlevel;
@synthesize buyPrice;
@synthesize salePrice;
@synthesize saleSign;
@synthesize imageTitle;
@synthesize imgPath;
@synthesize imgName;
@synthesize goodsDesc;
@synthesize createTime;


+(GoodsInfoModel *)itemWithDict:(NSDictionary *)dict
{
    GoodsInfoModel *item = [[GoodsInfoModel alloc] init] ;
    item.djLsh = [[dict valueForKey:@"DjLsh"] intValue];
    item.customId = [[dict valueForKey:@"CustomId"] intValue];
    item.goodsName = [dict valueForKey:@"GoodsName"];
    item.oldlevel = [dict valueForKey:@"Oldlevel"];
    item.buyPrice = [[dict valueForKey:@"BuyPrice"] floatValue];
    item.salePrice = [[dict valueForKey:@"SalePrice"] floatValue];
    item.saleSign = [[dict valueForKey:@"SaleSign"] intValue];
    item.imageTitle = [dict valueForKey:@"ImageTitle"];
    item.imgPath = [dict valueForKey:@"ImgPath"];
    item.imgName = [dict valueForKey:@"ImgName"];
    item.goodsDesc = [dict valueForKey:@"GoodsDesc"];
    item.createTime = [JsonDataExchange JsonTimeToNSDate:[dict valueForKey:@"CreateTime"]];
    return item;
}

-(void)exchangeNil
{
    if(goodsName == nil) goodsName = @"";
    if(oldlevel == nil) oldlevel = @"";
    if(imageTitle == nil) imageTitle = @"";
    if(imgPath == nil) imgPath = @"";
    if(imgName == nil) imgName = @"";
    if(goodsDesc == nil) goodsDesc = @"";
    if(createTime == nil)
        createTime = [JsonDataExchange NSDateMin];
}
-(NSMutableString *)getJsonValue
{
    [self exchangeNil];

    NSMutableString * jsonItem = [NSMutableString string];
    [jsonItem appendFormat:@"{"];
    [jsonItem appendFormat:@"\"DjLsh\":%i,", djLsh];
    [jsonItem appendFormat:@"\"CustomId\":%i,", customId];
    [jsonItem appendFormat:@"\"GoodsName\":\"%@\",", goodsName];
    [jsonItem appendFormat:@"\"Oldlevel\":\"%@\",", oldlevel];
    [jsonItem appendFormat:@"\"BuyPrice\":%f,", buyPrice];
    [jsonItem appendFormat:@"\"SalePrice\":%f,", salePrice];
    [jsonItem appendFormat:@"\"SaleSign\":%i,", saleSign];
    [jsonItem appendFormat:@"\"ImageTitle\":\"%@\",", imageTitle];
    [jsonItem appendFormat:@"\"ImgPath\":\"%@\",", imgPath];
    [jsonItem appendFormat:@"\"ImgName\":\"%@\",", imgName];
    [jsonItem appendFormat:@"\"GoodsDesc\":\"%@\",", goodsDesc];
    [jsonItem appendFormat:@"\"CreateTime\":\"\\/%@\\/\"", [JsonDataExchange JsonTimeFromNSDate:createTime]];
    [jsonItem appendFormat:@"}"];
    return jsonItem;
}
-(NSMutableString *)getXmlValue
{
    [self exchangeNil];

    NSMutableString *xmlItem = [NSMutableString string];
    [xmlItem appendFormat:@"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"];
    [xmlItem appendFormat:@"<item>"];
    [xmlItem appendFormat:@"<DjLsh>%i</DjLsh>",djLsh];
    [xmlItem appendFormat:@"<CustomId>%i</CustomId>",customId];
    [xmlItem appendFormat:@"<GoodsName>%@</GoodsName>",goodsName];
    [xmlItem appendFormat:@"<Oldlevel>%@</Oldlevel>",oldlevel];
    [xmlItem appendFormat:@"<BuyPrice>%f</BuyPrice>",buyPrice];
    [xmlItem appendFormat:@"<SalePrice>%f</SalePrice>",salePrice];
    [xmlItem appendFormat:@"<SaleSign>%i</SaleSign>",saleSign];
    [xmlItem appendFormat:@"<ImageTitle>%@</ImageTitle>",imageTitle];
    [xmlItem appendFormat:@"<ImgPath>%@</ImgPath>",imgPath];
    [xmlItem appendFormat:@"<ImgName>%@</ImgName>",imgName];
    [xmlItem appendFormat:@"<GoodsDesc>%@</GoodsDesc>",goodsDesc];
    [xmlItem appendFormat:@"<CreateTime>%@</CreateTime>",[JsonDataExchange JsonTimeFromNSDate:createTime]];
    [xmlItem appendFormat:@"</item>"];
    return xmlItem;
}

#pragma mark - NSCoding
-(void) encodeWithCoder: (NSCoder *) encoder
{
    [encoder encodeInt:djLsh forKey: @"djLsh"];
    [encoder encodeInt:customId forKey: @"customId"];
    [encoder encodeObject:goodsName forKey: @"goodsName"];
    [encoder encodeObject:oldlevel forKey: @"oldlevel"];
    [encoder encodeFloat:buyPrice forKey: @"buyPrice"];
    [encoder encodeFloat:salePrice forKey: @"salePrice"];
    [encoder encodeInt:saleSign forKey: @"saleSign"];
    [encoder encodeObject:imageTitle forKey: @"imageTitle"];
    [encoder encodeObject:imgPath forKey: @"imgPath"];
    [encoder encodeObject:imgName forKey: @"imgName"];
    [encoder encodeObject:goodsDesc forKey: @"goodsDesc"];
    [encoder encodeObject:createTime forKey: @"createTime"];
}
-(id) initWithCoder: (NSCoder *) decoder
{
    djLsh = [decoder decodeIntForKey:@"djLsh"];
    customId = [decoder decodeIntForKey:@"customId"];
    goodsName = [decoder decodeObjectForKey:@"goodsName"];
    oldlevel = [decoder decodeObjectForKey:@"oldlevel"] ;
    buyPrice = [decoder decodeFloatForKey:@"buyPrice"];
    salePrice = [decoder decodeFloatForKey:@"salePrice"];
    saleSign = [decoder decodeIntForKey:@"saleSign"];
    imageTitle = [decoder decodeObjectForKey:@"imageTitle"];
    imgPath = [decoder decodeObjectForKey:@"imgPath"] ;
    imgName = [decoder decodeObjectForKey:@"imgName"];
    goodsDesc = [decoder decodeObjectForKey:@"goodsDesc"] ;
    createTime = [decoder decodeObjectForKey:@"createTime"];

    return self;
}

#pragma mark - NSCopying
// 复制
-(id)copyWithZone:(NSZone *)zone
{
    GoodsInfoModel *newItem = [[GoodsInfoModel allocWithZone: zone] init];

    newItem.djLsh = self.djLsh;
    newItem.customId = self.customId;
    newItem.goodsName = self.goodsName;
    newItem.oldlevel = self.oldlevel;
    newItem.buyPrice = self.buyPrice;
    newItem.salePrice = self.salePrice;
    newItem.saleSign = self.saleSign;
    newItem.imageTitle = self.imageTitle;
    newItem.imgPath = self.imgPath;
    newItem.imgName = self.imgName;
    newItem.goodsDesc = self.goodsDesc;
    newItem.createTime = self.createTime;

	return newItem;
}

@end

