// SQL执行语句
#import <Foundation/Foundation.h>
#import "UserModel.h"
@interface UserInfoSqlite : NSObject
{



}

@property (nonatomic,strong)NSString *imgPath;
@property (nonatomic,strong)NSString *imgName;


// 新增用户
+(BOOL)addItem:(UserModel *)item;

// 查找用户
+(BOOL)selectItem:(UserModel *)item;

// 修改登录状态
+(BOOL)updateItemisLog0;

// 修改登录状态=1
+(BOOL)updateItemisLog1:(UserModel *)item;

// 查找当前登录的用户
+(BOOL)selectItemWithUserIsLogin:(UserModel *)item;

// 根据用户的单据流水号取得用户的头像
-(NSString *)getUserHead:(int)userDjLsh;

// 登录后将所有信息都重新加载一遍
+(BOOL)updateUserItem:(UserModel *)item;
@end
